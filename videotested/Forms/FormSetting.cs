﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ThinkGearNET;
using System.IO.Ports;


namespace screencapture
{
    public partial class FormSetting : Form
    {
        Pen gazeMarkerpPen = new Pen(Color.FromArgb(0, 124, 173), 5);
        int gazeMarkerSize = Convert.ToInt32(ConfigurationManager.AppSettings["Size_Gaze_Marker"]);

        VideoRecorder recoder;
        int size;

        public static bool srch;
        public static string activeport;

        public FormSetting(VideoRecorder recoder)
        {
            this.recoder = recoder;
            InitializeComponent();
        }

        #region Настройка директории для проектов
        private void btnSetDefaultDirectory_Click(object sender, EventArgs e)
        {
            tbPathToDefaultDir.Text = Path.Combine(Directory.GetCurrentDirectory(), "project");
        }

        private void btnSelectDirectory_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog FBDdir = new FolderBrowserDialog();
            FBDdir.SelectedPath = ConfigurationManager.AppSettings["Default_Project_Directory"];
            if (FBDdir.ShowDialog() == DialogResult.OK)
            {
                tbPathToDefaultDir.Text = FBDdir.SelectedPath;
            }
        }
        #endregion


        #region Настройка маркера взгляда на обработаном видео           
        private void trackBarSizeMarkerGaze_Scroll(object sender, EventArgs e)
        {
            size = trackBarSizeMarkerGaze.Value;
            using (Graphics g = this.CreateGraphics())
            {
                g.Clear(Color.FromName("Control"));
                g.DrawEllipse(gazeMarkerpPen, 370 - (size / 2), 200 - (size / 2), size, size);
            }
            labelGMdiametr.Text = trackBarSizeMarkerGaze.Value.ToString() + " px";
        }
        #endregion

        #region Стартовые параметры, принять\отменить
        private void FormSetting_Load(object sender, EventArgs e)
        {
            //Инициализация имеющихся настроек из app.config
            tbPathToDefaultDir.Text = ConfigurationManager.AppSettings["Default_Project_Directory"];
            trackBarSizeMarkerGaze.Value = Int32.Parse(ConfigurationManager.AppSettings["Size_Gaze_Marker"]);
            tbMaxTimeGaze.Text = ConfigurationManager.AppSettings["Max_Time_Fixation"];
            labelGMdiametr.Text = trackBarSizeMarkerGaze.Value.ToString() + " px";

            foreach (string port in SerialPort.GetPortNames())
                cboPort.Items.Add(port);
            cboPort.SelectedIndex = 0;

            string defaultPort = ConfigurationManager.AppSettings["COM_Port"];

            for (int i = 0; i < cboPort.Items.Count; i++)
            {
                if (cboPort.Items[i].ToString() == defaultPort)
                    cboPort.SelectedItem = cboPort.Items[i];
            }

            infolabel.Visible = true;
            if (defaultPort != cboPort.SelectedItem.ToString())
                infolabel.Text = "Сохраненный порт не найден";
            else
                infolabel.Text = "Сохраненный порт найден";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            //Изменение насроек в app.config
            Configuration currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            currentConfig.AppSettings.Settings["Size_Gaze_Marker"].Value = trackBarSizeMarkerGaze.Value.ToString();
            currentConfig.AppSettings.Settings["Default_Project_Directory"].Value = tbPathToDefaultDir.Text;
            currentConfig.AppSettings.Settings["Max_Time_Fixation"].Value = tbMaxTimeGaze.Text;
            if(currentConfig.AppSettings.Settings["COM_Port"] == null)
                currentConfig.AppSettings.Settings.Add("COM_Port", cboPort.SelectedItem.ToString());
            else
                currentConfig.AppSettings.Settings["COM_Port"].Value = cboPort.SelectedItem.ToString();
            currentConfig.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            this.Close();
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button1_MouseDown(object sender, MouseEventArgs e)
        {
            infolabel.Visible = true;
            infolabel.Text = "Пожалуйста, подождите идет поиск...";
            activeport = "НЕТ";
        }

        private void button1_MouseUp(object sender, MouseEventArgs e)
        {
            ThinkGearWrapper thinkGear = new ThinkGearWrapper();
            srch = false;

            var allports = SerialPort.GetPortNames();

            foreach (string port in allports)
            {
                thinkGear.Connect(port, ThinkGear.BAUD_57600, true);
                int idx = 0;
                while (idx < 100 & !srch)
                {
                    thinkGear.UpdateState();
                    if (thinkGear.ThinkGearState.PoorSignal > 0)
                    {
                        srch = true;
                        activeport = port;
                    }

                    Thread.Sleep(100);
                    idx++;
                }

            }

            if (activeport == "НЕТ")
                infolabel.Text = "Устройство не обнаружено";
            else
            {
                infolabel.Text = "Активный порт: " + activeport;
                for (int i = 0; i < cboPort.Items.Count; i++)
                {
                    if (cboPort.Items[i].ToString() == activeport)
                        cboPort.SelectedItem = cboPort.Items[i];
                }
            }
             
        }
    }
}
