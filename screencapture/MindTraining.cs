﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace screencapture
{
    public struct MindTraining
    {
        public int id { get; set; }
        public double timeStamp { get; set; }

        public int emo_id { get; set; }
        public int anger { get; set; }
        public int fear { get; set; }
        public int sadness { get; set; }
        public int happiness { get; set; }

        public int mouseX { get; set; }
        public int mouseY { get; set; }
        public int gazeX { get; set; }
        public int gazeY { get; set; }

        public int meditation { get; set; }
        public int attention { get; set; }
        public int alpha1 { get; set; }
        public int alpha2 { get; set; }
        public int beta1 { get; set; }
        public int beta2 { get; set; }
        public int gamma1 { get; set; }
        public int gamma2 { get; set; }
        public int poor_signal { get; set; }
        public int blink { get; set; }
        public int raw { get; set; }
        public string path { get; set; }

        //public int cluster { get; set; }
        //public double timeGazeFixed { get; set; }

        public MindTraining(int id = 0, double timeStamp = 0, int emo_id = 0, int anger = 0, int fear = 0, int sadness = 0, int happiness = 0, 
            int mouseX = 0, int mouseY = 0, int gazeX = 0, int gazeY = 0, int meditation = 0, int attention = 0, int alpha1 = 0, int alpha2 = 0, 
                int beta1 = 0, int beta2 = 0, int gamma1 = 0, int gamma2 = 0, int poor_signal = 200, int blink = 0, int raw = 0, string path = "")
        {
            this.id = id;
            this.timeStamp = timeStamp;

            this.emo_id = emo_id;
            this.anger = anger;
            this.fear = fear;
            this.sadness = sadness;
            this.happiness = happiness;

            this.mouseX = mouseX;
            this.mouseY = mouseY;
            this.gazeX = gazeX;
            this.gazeY = gazeY;

            this.meditation = meditation;
            this.attention = attention;
            this.alpha1 = alpha1;
            this.alpha2 = alpha2;
            this.beta1 = beta1;
            this.beta2 = beta2;
            this.gamma1 = gamma1;
            this.gamma2 = gamma2;
            this.poor_signal = poor_signal;
            this.blink = blink;
            this.raw = raw;
            this.path = path;
        }
    }
}
