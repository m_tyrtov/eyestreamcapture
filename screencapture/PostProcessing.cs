﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using OxyPlot;

namespace screencapture
{
    class PostProcessing
    {
        public List<FramePoint> Clusterize(List<FramePoint> csv_list, int difPercent)
        {
            int dif = (Screen.PrimaryScreen.Bounds.Height / 100) * difPercent; //находим заданый процент от высоты экрана в пикселях

            List<RowCluster> points = new List<RowCluster>();
            List<ClusterTimeFixation> clusters = new List<ClusterTimeFixation>();
            List<FramePoint> framePoints = new List<FramePoint>();

            int StartX = 0; //Координаты первой точки в кластере
            int StartY = 0;
            int ClusterCounter = 1; //Счетчик кластеров, когда по условию создается новый кластер, увеличивается
            double startTimeFixed;
            try
            {
                startTimeFixed = csv_list[0].timeStamp; //Меняется, когда объявляется новый кластер и указывает на время начала фиксации в номо кластере
            }
            catch
            {
                return framePoints;
            }
            double timeFixed = 0; //Вычисляется на каждой строке, время фиксирования от начала фиксации на кластере до данной точки
            double tempTimeStamp = 0; //для хранения значения timeStamp предыдущей точки, при последовательном проходе по ним

            int idx = 0;
            foreach (var record in csv_list)
            {
                //Первую строку определяем как стартовую для кластера 1, по идее это условие срабатывает только 1 раз вначале
                if (StartX == 0 && StartY == 0)
                {
                    StartX = record.gazeX;
                    StartY = record.gazeY;
                }
                //Вычитаем из XY текущей строки значения XY стартовой точки последнего кластера, 
                //если разница не больше, чем заданый пользователем предел, то добавляем в List<UpdateRow> id этой строки с номером текущего кластера...                
                if (((Math.Abs(record.gazeX - StartX)) <= dif) && (Math.Abs(record.gazeY - StartY) <= dif))
                {
                    points.Add(new RowCluster(idx, ClusterCounter)); //добавляется новая точка в list, с номером текущего кластера
                    timeFixed = record.timeStamp - startTimeFixed;
                    tempTimeStamp = record.timeStamp; //временная переменная хранит таймстамп предыдущего шага
                }
                else //... иначе номер кластера увеличиваем на единицу и данная точка становится стартовой для нового кластера
                {
                    clusters.Add(new ClusterTimeFixation(ClusterCounter, timeFixed));//кластер записывается в list
                    ClusterCounter++; //начинается новый кластер, абстрактно
                    points.Add(new RowCluster(idx, ClusterCounter)); //добавляется новая точка в list, с номером  нового кластера
                    StartX = record.gazeX; //определяются стартовые точки, как начальная точка нового кластера
                    StartY = record.gazeY;

                    startTimeFixed = tempTimeStamp; //Время начала фиксации нового кластера
                    tempTimeStamp = record.timeStamp; //временная переменная для сохранения таймстампа этого шага для следующей строки
                }

                idx++;
            }
            clusters.Add(new ClusterTimeFixation(ClusterCounter, timeFixed));//добавляется последний кластер

            foreach (var i in points)
            {
                foreach (var j in clusters)
                {
                    if (i.cluster == j.cluster)
                    {
                        FramePoint currentPoint = csv_list[i.id];

                        framePoints.Add(new FramePoint(currentPoint.dataset_id, currentPoint.gazeX, currentPoint.gazeY,
                            currentPoint.mouseY, currentPoint.mouseX, currentPoint.timeStamp, j.timeFixation, j.cluster, currentPoint.meditation,
                                currentPoint.attention, currentPoint.alpha1, currentPoint.alpha2, currentPoint.beta1, currentPoint.beta2,
                                    currentPoint.gamma1, currentPoint.gamma2, currentPoint.poor_signal, currentPoint.blink, currentPoint.raw));
                    }
                }
            }

            return framePoints;
        }

        public List<RawPoint> ClusterizeRaw(List<RawPoint> csv_list, int difPercent)
        {
            int dif = (Screen.PrimaryScreen.Bounds.Height / 100) * difPercent; //находим заданый процент от высоты экрана в пикселях

            List<RowCluster> points = new List<RowCluster>();
            List<ClusterTimeFixation> clusters = new List<ClusterTimeFixation>();
            List<RawPoint> framePoints = new List<RawPoint>();

            int StartX = 0; //Координаты первой точки в кластере
            int StartY = 0;
            int ClusterCounter = 1; //Счетчик кластеров, когда по условию создается новый кластер, увеличивается
            double startTimeFixed;
            try
            {
                startTimeFixed = csv_list[0].timeStamp; //Меняется, когда объявляется новый кластер и указывает на время начала фиксации в номо кластере
            }
            catch
            {
                return framePoints;
            }
            double timeFixed = 0; //Вычисляется на каждой строке, время фиксирования от начала фиксации на кластере до данной точки
            double tempTimeStamp = 0; //для хранения значения timeStamp предыдущей точки, при последовательном проходе по ним

            int idx = 0;
            foreach (var record in csv_list)
            {
                //Первую строку определяем как стартовую для кластера 1, по идее это условие срабатывает только 1 раз вначале
                if (StartX == 0 && StartY == 0)
                {
                    StartX = record.gazeX;
                    StartY = record.gazeY;
                }
                //Вычитаем из XY текущей строки значения XY стартовой точки последнего кластера, 
                //если разница не больше, чем заданый пользователем предел, то добавляем в List<UpdateRow> id этой строки с номером текущего кластера...                
                if (((Math.Abs(record.gazeX - StartX)) <= dif) && (Math.Abs(record.gazeY - StartY) <= dif))
                {
                    points.Add(new RowCluster(idx, ClusterCounter)); //добавляется новая точка в list, с номером текущего кластера
                    timeFixed = record.timeStamp - startTimeFixed;
                    tempTimeStamp = record.timeStamp; //временная переменная хранит таймстамп предыдущего шага
                }
                else //... иначе номер кластера увеличиваем на единицу и данная точка становится стартовой для нового кластера
                {
                    clusters.Add(new ClusterTimeFixation(ClusterCounter, timeFixed));//кластер записывается в list
                    ClusterCounter++; //начинается новый кластер, абстрактно
                    points.Add(new RowCluster(idx, ClusterCounter)); //добавляется новая точка в list, с номером  нового кластера
                    StartX = record.gazeX; //определяются стартовые точки, как начальная точка нового кластера
                    StartY = record.gazeY;

                    startTimeFixed = tempTimeStamp; //Время начала фиксации нового кластера
                    tempTimeStamp = record.timeStamp; //временная переменная для сохранения таймстампа этого шага для следующей строки
                }

                idx++;
            }
            clusters.Add(new ClusterTimeFixation(ClusterCounter, timeFixed));//добавляется последний кластер

            foreach (var i in points)
            {
                foreach (var j in clusters)
                {
                    if (i.cluster == j.cluster)
                    {
                        RawPoint currentPoint = csv_list[i.id];

                        framePoints.Add(new RawPoint(currentPoint.id, currentPoint.timeStamp, currentPoint.mouseX, currentPoint.mouseY,
                            currentPoint.gazeX, currentPoint.gazeY, j.cluster, j.timeFixation, currentPoint.emo_id,
                                currentPoint.anger, currentPoint.fear, currentPoint.sadness, currentPoint.happiness, currentPoint.raw));
                    }
                }
            }

            return framePoints;
        }
    }

    struct RowCluster
    {
        public int id { get; set; }
        public int cluster { get; set; }

        public RowCluster(int id, int cluster)
        {
            this.id = id;
            this.cluster = cluster;
        }
    }

    struct ClusterTimeFixation
    {
        public int cluster { get; set; }
        public double timeFixation { get; set; }

        public ClusterTimeFixation(int cluster, double timeFixation)
        {
            this.cluster = cluster;
            this.timeFixation = timeFixation;
        }
    }
}