﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpOSC; //библиотека для работы с протоколом OSC (Open Sound Control) - с помощью него шапка передает данные
using System.Threading;
using System.Windows.Forms;

//примерное (требует редактирования) описание входных данных шапки:

//EEG:
//    * eeg - EEG - электроэнцефалограмма
//    * variance_eeg - EEG Variance - электроэнцефалограмма с дисперсией
//    * notch_filtered_eeg - Notch Filtered EEG - электроэнцефалограмма c режекторным фильтром
//    * variance_notch_filtered_eeg - Notch Filtered EEG Variance - электроэнцефалограмма с дисперсией и режекторным фильтром
//Other sensors:
//    * acc - Accelerometer - акселерометр
//    * gyro - Gyroscope - гироскоп
//Brain waves:
//    * alpha_absolute - Alpha Absolute - Абсолютная альфа
//    * alpha_relative - Alpha Relative - Относительная альфа
//    * alpha_session_score - Alpha Session Score - Оценка сессии по альфе
//    * beta_absolute - Beta Absolute - Абсолютная бета
//    * beta_relative - Beta Relative - Относительная бета
//    * beta_session_score - Beta Session Score - Оценка сессии по бете
//    * delta_absolute - Delta Absolute - Абсолютная дельте
//    * delta_relative - Delta Relative - Относительная дельте
//    * delta_session_score - Delta Session Score - Оценка сессии по дельте
//    * gamma_absolute - Gamma Absolute - Абсолютная гамма
//    * gamma_absolute_relative - Gamma Relative - Относительная гамма
//    * gamma_session_score - Gamma Session Score - Оценка сессии по гамме
//    * theta_absolute - Theta Absolute - Абсолютная зета
//    * theta_absolute_relative - Theta Relative - Относительная зета
//    * theta_session_score - Theta Session Score - Оценка сессии по зете
//Other info:
//    * touching_forehead - Headband On - Касание лба/Включенная шапка?
//    * jaw_clench - Jaw Clench - сжимание челюсти
//    * is_good - Real Time EEG Quality - Касание датчиков лба
//    * horseshoe - Headband Status Indicator - Индикатор состония оголовья
//    * blink - Blink - Индикатор моргания

namespace screencapture
{
    public class Muse
    {
        //public long timestamp { get; set; }

        public int id_edu { get; set; }

        public double timeStamp { get; set; }
        public int mouseX { get; set; }
        public int mouseY { get; set; }
        public int gazeX { get; set; }
        public int gazeY { get; set; }

        public int emo_id { get; set; }
        public int anger { get; set; }
        public int fear { get; set; }
        public int sadness { get; set; }
        public int happiness { get; set; }

        public float eeg_1 { get; set; }
        public float eeg_2 { get; set; }
        public float eeg_3 { get; set; }
        public float eeg_4 { get; set; }
        //public float eeg_5 { get; set; } //NaN :(
        //public float eeg_6 { get; set; } //NaN :(

        public float variance_eeg_1 { get; set; }
        public float variance_eeg_2 { get; set; }
        public float variance_eeg_3 { get; set; }
        public float variance_eeg_4 { get; set; }

        public float notch_filtered_eeg_1 { get; set; }
        public float notch_filtered_eeg_2 { get; set; }
        public float notch_filtered_eeg_3 { get; set; }
        public float notch_filtered_eeg_4 { get; set; }

        public float variance_notch_filtered_eeg_1 { get; set; }
        public float variance_notch_filtered_eeg_2 { get; set; }
        public float variance_notch_filtered_eeg_3 { get; set; }
        public float variance_notch_filtered_eeg_4 { get; set; }

        public int is_good_1 { get; set; }
        public int is_good_2 { get; set; }
        public int is_good_3 { get; set; }
        public int is_good_4 { get; set; }

        public Muse() { }

        private Muse(int id_edu = 0, double timeStamp = 0, int mouseX = 0, int mouseY = 0, int gazeX = 0, int gazeY = 0,
            int emo_id = 0, int anger = 0, int fear = 0, int sadness = 0, int happiness = 0,
            float eeg_1 = 0, float eeg_2 = 0, float eeg_3 = 0, float eeg_4 = 0,
            float variance_eeg_1 = 0, float variance_eeg_2 = 0, float variance_eeg_3 = 0, float variance_eeg_4 = 0,
            float notch_filtered_eeg_1 = 0, float notch_filtered_eeg_2 = 0, float notch_filtered_eeg_3 = 0, float notch_filtered_eeg_4 = 0,
            float variance_notch_filtered_eeg_1 = 0, float variance_notch_filtered_eeg_2 = 0, float variance_notch_filtered_eeg_3 = 0, float variance_notch_filtered_eeg_4 = 0,
            int is_good_1 = 0, int is_good_2 = 0, int is_good_3 = 0, int is_good_4 = 0)
        {
            //this.timestamp = timestamp;
            this.id_edu = id_edu;

            this.timeStamp = timeStamp;
            this.mouseX = mouseX;
            this.mouseY = mouseY;
            this.gazeX = gazeX;
            this.gazeY = gazeY;

            this.emo_id = emo_id;
            this.anger = anger;
            this.fear = fear;
            this.sadness = sadness;
            this.happiness = happiness;

            this.eeg_1 = eeg_1;
            this.eeg_2 = eeg_2;
            this.eeg_3 = eeg_3;
            this.eeg_4 = eeg_4;
            //this.eeg_5 = eeg_5;
            //this.eeg_6 = eeg_6;

            this.variance_eeg_1 = variance_eeg_1;
            this.variance_eeg_2 = variance_eeg_2;
            this.variance_eeg_3 = variance_eeg_3;
            this.variance_eeg_4 = variance_eeg_4;

            this.notch_filtered_eeg_1 = notch_filtered_eeg_1;
            this.notch_filtered_eeg_2 = notch_filtered_eeg_2;
            this.notch_filtered_eeg_3 = notch_filtered_eeg_3;
            this.notch_filtered_eeg_4 = notch_filtered_eeg_4;

            this.variance_notch_filtered_eeg_1 = variance_notch_filtered_eeg_1;
            this.variance_notch_filtered_eeg_2 = variance_notch_filtered_eeg_2;
            this.variance_notch_filtered_eeg_3 = variance_notch_filtered_eeg_3;
            this.variance_notch_filtered_eeg_4 = variance_notch_filtered_eeg_4;

            this.is_good_1 = is_good_1;
            this.is_good_2 = is_good_2;
            this.is_good_3 = is_good_3;
            this.is_good_4 = is_good_4;
        }

        //переменные необходимые для получения OSC стрима
        private UDPListener listener;
        public static Muse pointDump = new Muse();

        private HandleOscPacket callback = delegate (OscPacket packet)
        {
            var messageReceived = (OscMessage)packet;
            //pointDump.timestamp = Data.stopWatch.Elapsed.Milliseconds;
            List<float> EEG_list = new List<float>();
            List<int> signal_list = new List<int>();

            switch (messageReceived.Address)
            {
                case "Muse-04A9/eeg":
       
                    foreach (var record in messageReceived.Arguments)
                        EEG_list.Add(float.Parse(record.ToString()));

                    pointDump.eeg_1 = EEG_list[0];
                    pointDump.eeg_2 = EEG_list[1];
                    pointDump.eeg_3 = EEG_list[2];
                    pointDump.eeg_4 = EEG_list[3];

                    //if (Data.id_edu != 0)
                    //    Data.MuseList.Add(new Muse(
                    //        Data.id_edu, (DateTime.Now - Data.startVideoTime).TotalSeconds, Cursor.Position.X, Cursor.Position.Y, 
                    //        Data.GazeX, Data.GazeX, Data.emotion.id, Data.emotion.anger, Data.emotion.fear, Data.emotion.sadness, Data.emotion.happiness,
                    //        pointDump.eeg_1, pointDump.eeg_2, pointDump.eeg_3, pointDump.eeg_4,
                    //        pointDump.variance_eeg_1, pointDump.variance_eeg_2, pointDump.variance_eeg_3, pointDump.variance_eeg_4,
                    //        pointDump.notch_filtered_eeg_1, pointDump.notch_filtered_eeg_2, pointDump.notch_filtered_eeg_3, pointDump.notch_filtered_eeg_4,
                    //        pointDump.variance_notch_filtered_eeg_1, pointDump.variance_notch_filtered_eeg_2, pointDump.variance_notch_filtered_eeg_3, pointDump.variance_notch_filtered_eeg_4,
                    //        pointDump.is_good_1, pointDump.is_good_2, pointDump.is_good_3, pointDump.is_good_4
                    //        ));
                    break;

                case "Muse-04A9/variance_eeg":
                    
                    foreach (var record in messageReceived.Arguments)
                        EEG_list.Add(float.Parse(record.ToString()));

                    pointDump.variance_eeg_1 = EEG_list[0];
                    pointDump.variance_eeg_2 = EEG_list[1];
                    pointDump.variance_eeg_3 = EEG_list[2];
                    pointDump.variance_eeg_4 = EEG_list[3];

                    break;

                case "Muse-04A9/notch_filtered_eeg":

                    foreach (var record in messageReceived.Arguments)
                        EEG_list.Add(float.Parse(record.ToString()));

                    pointDump.notch_filtered_eeg_1 = EEG_list[0];
                    pointDump.notch_filtered_eeg_2 = EEG_list[1];
                    pointDump.notch_filtered_eeg_3 = EEG_list[2];
                    pointDump.notch_filtered_eeg_4 = EEG_list[3];

                    break;
                case "Muse-04A9/variance_notch_filtered_eeg":

                    foreach (var record in messageReceived.Arguments)
                        EEG_list.Add(float.Parse(record.ToString()));

                    pointDump.variance_notch_filtered_eeg_1 = EEG_list[0];
                    pointDump.variance_notch_filtered_eeg_2 = EEG_list[1];
                    pointDump.variance_notch_filtered_eeg_3 = EEG_list[2];
                    pointDump.variance_notch_filtered_eeg_4 = EEG_list[3];

                    break;
                case "Muse-04A9/is_good":

                    foreach (var record in messageReceived.Arguments)
                        signal_list.Add(Int32.Parse(record.ToString()));

                    pointDump.is_good_1 = signal_list[0];
                    pointDump.is_good_2 = signal_list[1];
                    pointDump.is_good_3 = signal_list[2];
                    pointDump.is_good_4 = signal_list[3]; 

                    break;
            }
        };
        
        public void MuseDisconnect() //прерывает процесс получения сигнала с шапки
        {
            listener.Close();
        }

        public void MuseConnect(int port)
        {
            listener = new UDPListener(port, callback);
        }
    }
}
