﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tobii.Interaction;

namespace screencapture
{
    class MuseBluetooth
    {
        MuseClient client = new MuseClient();
        //private EyeTracker eyeTracker;
        private int i = 1; //counter

        public async Task StartFeed()
        {
            //eyeTracker = EyeTracker.GetEyeTracker();
            //eyeTracker.StartEyeStream();

            var ok = await client.Connect();
            if (ok)
            {
                await client.Subscribe(
                    Channel.EEG_AF7,
                    Channel.EEG_AF8,
                    Channel.EEG_TP10,
                    Channel.EEG_TP9
                    );

                client.NotifyEeg += Client_NotifyEeg;
                await client.Resume();
            }
        }

        private void Client_NotifyEeg(Channel channel, Encefalogram gram)
        {
            //if (Data.eeg_mode == "muse2" & Data.id_edu != 0)
            //{
            //    //Data.emotion.id, Data.emotion.anger, Data.emotion.fear, Data.emotion.sadness, Data.emotion.happiness
            //    Data.museList.Add(new EncefalogramNamed(i, (DateTime.Now - Data.startVideoTime).TotalSeconds, (int)eyeTracker.gazeX, (int)eyeTracker.gazeY,
            //        0, 0, 0, gram.Raw[0], gram.Raw[1], gram.Raw[2], gram.Raw[3], gram.Raw[4], 
            //            gram.Raw[5], gram.Raw[6], gram.Raw[7], gram.Raw[8], gram.Raw[9], gram.Raw[10], gram.Raw[11], gram.Raw[12], gram.Raw[13], gram.Raw[14], 
            //                gram.Raw[15], gram.Raw[16], gram.Raw[17], gram.Raw[18], gram.Raw[19],
            //                    gram.Samples[0], gram.Samples[1], gram.Samples[2], gram.Samples[3], gram.Samples[4],
            //                        gram.Samples[5], gram.Samples[6], gram.Samples[7], gram.Samples[8], gram.Samples[9],
            //                            gram.Samples[10], gram.Samples[11], Data.picPath));
            //    i++;
            //}
            //else
            //    i = 1;
        }
        
        private async void SetPause()
        {
            await client.Pause();
        }

        private async void Resume()
        {
            await client.Resume();
        }

        public async void Disconnect()
        {
            await client.Disconnect();
        }
    }
}
