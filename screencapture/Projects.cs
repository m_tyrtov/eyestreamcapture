﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using System.Data.SQLite.Linq;
using System.Windows.Forms;
using System.Data;
using System.Configuration;

namespace screencapture
{
    class Projects
    {
        SQLiteConnection _SQLiteConnetion;
        SQLiteCommand _SQLiteCommand;
        private string pathToDB = ConfigurationManager.AppSettings["Default_Project_Directory"] + @"\project.sqlite";
        private string tableName = "projects";

        public int id { get; set; }
        public int product_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string path { get; set; }

        public Projects() { }

        private Projects(int id, int product_id, string name, string description, string path)
        {
            this.id = id;
            this.product_id = product_id;
            this.name = name;
            this.description = description;
            this.path = path;
        }

        private void CreateOrUsingDB()
        {
            if (!File.Exists(pathToDB))
                SQLiteConnection.CreateFile(pathToDB);
            try
            {
                _SQLiteCommand = new SQLiteCommand();
                _SQLiteConnetion = new SQLiteConnection("Data Source=" + pathToDB + "; Synchronous=OFF;");
                _SQLiteConnetion.Open();
                _SQLiteCommand.Connection = _SQLiteConnetion;
                _SQLiteCommand.CommandText = "CREATE TABLE IF NOT EXISTS " + tableName + " (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "product_id INTEGER, name TEXT, description TEXT)";
                _SQLiteCommand.ExecuteNonQuery();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Disconnected, Ошибка:{0}", ex.Message);
            }
            finally
            {
                _SQLiteConnetion.Close();
            }
        }

        private void ConnectToDB()
        {
            CreateOrUsingDB();

            _SQLiteCommand = new SQLiteCommand();
            _SQLiteConnetion = new SQLiteConnection(@"Data Source=" + pathToDB);
            _SQLiteCommand.Connection = _SQLiteConnetion;
            _SQLiteConnetion.Open();
        }

        private void CommandSQLQuery(string sqlQuery)
        {
            ConnectToDB();
            _SQLiteCommand.CommandText = sqlQuery;
            _SQLiteCommand.ExecuteNonQuery();
            _SQLiteConnetion.Close();
        }

        public List<Projects> GetList()
        {
            ConnectToDB();

            string sort = "ASC";

            List<Projects> list = new List<Projects>();
            DataTable dTable = new DataTable();
            String sqlQuery;

            try
            {
                sqlQuery = "SELECT * FROM " + tableName + " ORDER BY id " + sort;
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, _SQLiteConnetion);
                adapter.Fill(dTable);

                if (dTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dTable.Rows.Count; i++)
                    {
                        list.Add(new Projects(Int32.Parse(dTable.Rows[i].ItemArray[0].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[1].ToString()),
                            dTable.Rows[i].ItemArray[2].ToString(), dTable.Rows[i].ItemArray[3].ToString(), 
                                Path.Combine(ConfigurationManager.AppSettings["Default_Project_Directory"], dTable.Rows[i].ItemArray[2].ToString())));
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            _SQLiteConnetion.Close();

            return list;
        }

        public Projects Create(int product_id, string name, string description, string path)
        {
            CommandSQLQuery("INSERT INTO " + tableName + " ( product_id, name, description ) "
                + "VALUES('" + product_id + "', '" + name + "', '" + description + "'); ");

            return ReturnNewProject();
        }

        public Projects ReturnNewProject()
        {
            ConnectToDB();
            
            Projects current_project = new Projects();
            DataTable dTable = new DataTable();
            String sqlQuery;

            try
            {
                sqlQuery = "SELECT * FROM " + tableName + " ORDER BY id DESC LIMIT 1";
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, _SQLiteConnetion);
                adapter.Fill(dTable);

                if (dTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dTable.Rows.Count; i++)
                    {
                        current_project = new Projects(Int32.Parse(dTable.Rows[i].ItemArray[0].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[1].ToString()),
                            dTable.Rows[i].ItemArray[2].ToString(), dTable.Rows[i].ItemArray[3].ToString(),
                                Path.Combine(ConfigurationManager.AppSettings["Default_Project_Directory"], dTable.Rows[i].ItemArray[2].ToString()));
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            _SQLiteConnetion.Close();

            return current_project;
        }
    }
}