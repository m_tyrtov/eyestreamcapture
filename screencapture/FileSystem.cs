﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

namespace screencapture
{
    class FileSystem
    {
        Result result = new Result();

        public void CopyFileExamination(string source)
        {
            string dir_objects = Path.Combine(Data.project.path, Data.merge.name) + @"\objects\";
            Directory.CreateDirectory(dir_objects);
            File.Copy(source, Path.Combine(dir_objects, Path.GetFileName(source)));
        }

        public string CopyFileTraining(string source, string name, int id = 0)
        {
            string dir_objects = Path.Combine(Data.project.path, Data.merge.name, Data.examination.name, "training");
            bool kostyl = false;

            try
            {
                File.Copy(source, Path.Combine(dir_objects, name + ".jpg"));
            }
            catch
            {
                if (Path.Combine(dir_objects, name + ".jpg") != source)
                {
                    //File.Delete(Path.Combine(dir_objects, name + ".jpg"));
                    File.Copy(source, Path.Combine(dir_objects, name + "_2.jpg"));
                    kostyl = true;
                }
            }
            
            if(id != 0)
                result.UpdateTrainigImage(id, Path.Combine(dir_objects, name + ".jpg"));

            File.Delete(source);
            if(kostyl)
                return Path.Combine(dir_objects, name + "_2.jpg");
            else
                return Path.Combine(dir_objects, name + ".jpg");
        }

        public string CopyFileExamination(string source, string name, int id = 0)
        {
            string dir_objects = Path.Combine(Data.project.path, Data.merge.name, Data.examination.name, "output");
            Directory.CreateDirectory(dir_objects);
            File.Copy(source, Path.Combine(dir_objects, name + ".jpg"));

            if (id != 0)
                result.UpdateTrainigImage(id, Path.Combine(dir_objects, name + ".jpg"));

            return dir_objects;
        }

        public string CreateOutputDirectory(string project_path, string merge_name, string exam_name)
        {
            string exam_mode;
            switch (Data.edu_code)
            {
                case 0:
                    exam_mode = "output";
                    break;
                case 1:
                    exam_mode = "training";
                    break;
                default:
                    exam_mode = "output";
                    break;
            }

            string path = Path.Combine(project_path, merge_name, exam_name);
            Directory.CreateDirectory(path); //создали директорию для исследования

            path = Path.Combine(path, exam_mode);
            Directory.CreateDirectory(path);

            return path;
        }

        public void CopyDirectory(string source)
        {
            string dir_objects = Path.Combine(Data.project.path, Data.merge.name) + @"\objects\";
            Directory.CreateDirectory(dir_objects);
            CopyDir(source, dir_objects);
        }

        private void CopyDir(string FromDir, string ToDir)
        {
            Directory.CreateDirectory(ToDir);
            foreach (string s1 in Directory.GetFiles(FromDir))
            {
                string s2 = ToDir + "\\" + Path.GetFileName(s1);
                File.Copy(s1, s2);
            }
            foreach (string s in Directory.GetDirectories(FromDir))
            {
                CopyDir(s, ToDir + "\\" + Path.GetFileName(s));
            }
        }
    }
}
