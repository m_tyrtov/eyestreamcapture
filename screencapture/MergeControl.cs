﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using System.Data.SQLite.Linq;
using System.Windows.Forms;
using System.Data;
using System.Configuration;

namespace screencapture
{
    class MergeControl
    {
        SQLiteConnection _SQLiteConnetion;
        SQLiteCommand _SQLiteCommand;
        private string pathToDB = ConfigurationManager.AppSettings["Default_Project_Directory"] + @"\project.sqlite";
        private string tableName = "projects_has_stimulants";
        
        //main info
        public int id { get; set; }
        public string name { get; set; }
        public int project_id { get; set; }
        public int stimulant_id { get; set; }

        //settings...
        public string path{ get; set; }
        public string header{ get; set; }
        public string help{ get; set; }
        public string task{ get; set; }

        //picture stim mode...
        public int interval { get; set; }
        public int pause_time { get; set; }
        public int text_time { get; set; }

        public string flag { get; set; }
        public int part { get; set; }

        public MergeControl() { }

        private MergeControl(int id, string name, int project_id, int stimulant_id, string path, string header, string help, string task, int interval = 0, 
            int pause_time = 0, int text_time = 0, string flag = "ok", int part = 0)
        {
            this.id = id;
            this.name = name;
            this.project_id = project_id;
            this.stimulant_id = stimulant_id;

            this.path = path;
            this.header = header;
            this.help = help;
            this.task = task;

            this.interval = interval;
            this.pause_time = pause_time;
            this.text_time = text_time;

            this.flag = flag;
            this.part = part;
        }

        private void CreateOrUsingDB()
        {
            if (!File.Exists(pathToDB))
                SQLiteConnection.CreateFile(pathToDB);
            try
            {
                _SQLiteCommand = new SQLiteCommand();
                _SQLiteConnetion = new SQLiteConnection("Data Source=" + pathToDB + "; Synchronous=OFF;");
                _SQLiteConnetion.Open();
                _SQLiteCommand.Connection = _SQLiteConnetion;
                _SQLiteCommand.CommandText = "CREATE TABLE IF NOT EXISTS " + tableName + " (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "name TEXT, project_id INTEGER, stimulant_id INTEGER, path TEXT, header TEXT, help TEXT, task TEXT, interval INTEGER, pause_time INTEGER, " +
                    "text_time INTEGER, flag TEXT, part INTEGER)";
                _SQLiteCommand.ExecuteNonQuery();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Disconnected, Ошибка:{0}", ex.Message);
            }
            finally
            {
                _SQLiteConnetion.Close();
            }
        }

        private void ConnectToDB()
        {
            CreateOrUsingDB();

            _SQLiteCommand = new SQLiteCommand();
            _SQLiteConnetion = new SQLiteConnection(@"Data Source=" + pathToDB);
            _SQLiteCommand.Connection = _SQLiteConnetion;
            _SQLiteConnetion.Open();
        }

        private void CommandSQLQuery(string sqlQuery)
        {
            ConnectToDB();
            _SQLiteCommand.CommandText = sqlQuery;
            _SQLiteCommand.ExecuteNonQuery();
            _SQLiteConnetion.Close();
        }

        public MergeControl GetInfoAboutStim(int merge_id)
        {
            ConnectToDB();

            MergeControl current_record = new MergeControl();
            DataTable dTable = new DataTable();
            String sqlQuery;

            try
            {
                sqlQuery = "SELECT * FROM " + tableName + "  WHERE id = '" + merge_id + "'";
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, _SQLiteConnetion);
                adapter.Fill(dTable);

                if (dTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dTable.Rows.Count; i++)
                    {
                        current_record = new MergeControl(Int32.Parse(dTable.Rows[i].ItemArray[0].ToString()), dTable.Rows[i].ItemArray[1].ToString(), Int32.Parse(dTable.Rows[i].ItemArray[2].ToString()),
                            Int32.Parse(dTable.Rows[i].ItemArray[3].ToString()), dTable.Rows[i].ItemArray[4].ToString(), dTable.Rows[i].ItemArray[5].ToString(),
                                dTable.Rows[i].ItemArray[6].ToString(), dTable.Rows[i].ItemArray[7].ToString(), Int32.Parse(dTable.Rows[i].ItemArray[8].ToString()),
                                    Int32.Parse(dTable.Rows[i].ItemArray[9].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[10].ToString()), dTable.Rows[i].ItemArray[11].ToString(),
                                        Int32.Parse(dTable.Rows[i].ItemArray[12].ToString()));
                    }
                }
        }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            _SQLiteConnetion.Close();
            return current_record;
        }

        public void UpdateMerge(int merge_id, string name, string header, string help, string task, string path, int interval = 6, int pause_time = 1, int text_time = 3, int part = 15 )
        {
            CommandSQLQuery("UPDATE " + tableName + " SET name = '" + name + "', header = '" + header
                + "', help = '" + help + "', task = '" + task + "', path = '" + path + "', interval = '" + interval
                    + "', pause_time = '" + pause_time + "', text_time = '" + text_time  + "', part = '" + part +  "' WHERE id = '" + merge_id + "'");

            Data.merge = this.GetInfoAboutStim(merge_id);
        }

        public void AddNewMerge(int project_id, int stimulant_id, string name = "Новый стимулятор", string header = "Заголовок по умолчанию", 
            string help = "Подсказка по умолчанию", string task = "Задание по умолчанию", string path = "null", int interval = 6, int pause_time = 1,
                int text_time = 3, string flag = "ok", int part = 15)
        {

            if (path == "null")
            {
                switch (stimulant_id)
                {
                    case 1:
                        path = Environment.CurrentDirectory + @"\examples\file.txt";
                        break;
                    case 2:
                        path = Environment.CurrentDirectory + @"\examples\img";
                        break;
                }
            }

            CommandSQLQuery("INSERT INTO " + tableName + " ( project_id, stimulant_id, name, path, header, help, task, interval, pause_time, text_time, flag, part ) "
                + "VALUES('" + project_id + "', '" + stimulant_id + "', '" + name + "', '" + path + "', '" + header 
                + "', '" + help +"', '" + task + "', '" + interval + "', '" + pause_time + "', '" + text_time + "', '" + flag + "', '" + part + "'); ");
        }

        public void RemoveMerge(int merge_id)
        {
            CommandSQLQuery("UPDATE " + tableName + " SET flag = 'rm' WHERE id = '" + merge_id + "'");
        }

        public void SaveNamingResult(List<string> resultList)
        {
            Examination examination = new Examination();
            int i = 1;

            foreach (string record in resultList)
            {
                CommandSQLQuery("INSERT INTO naming_rating ( exam_id, range, word ) "
                + "VALUES('" + Data.examination.id + "', '" + i + "', '" + record + "'); ");
                i++;
            }
        }

        public List<MergeControl> StimulantsLoad(int project_id)
        {
            ConnectToDB();

            List<MergeControl> list = new List<MergeControl>();
            DataTable dTable = new DataTable();
            String sqlQuery;

            try
            {
                sqlQuery = "SELECT * FROM " + tableName + " WHERE project_id = '" + project_id + "' and flag != 'rm'";
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, _SQLiteConnetion);
                adapter.Fill(dTable);

                if (dTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dTable.Rows.Count; i++)
                    {
                        list.Add(new MergeControl(Int32.Parse(dTable.Rows[i].ItemArray[0].ToString()), dTable.Rows[i].ItemArray[1].ToString(), Int32.Parse(dTable.Rows[i].ItemArray[2].ToString()),
                            Int32.Parse(dTable.Rows[i].ItemArray[3].ToString()), dTable.Rows[i].ItemArray[4].ToString(), dTable.Rows[i].ItemArray[5].ToString(),
                                dTable.Rows[i].ItemArray[6].ToString(), dTable.Rows[i].ItemArray[7].ToString(), Int32.Parse(dTable.Rows[i].ItemArray[8].ToString()),
                                    Int32.Parse(dTable.Rows[i].ItemArray[9].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[10].ToString()), dTable.Rows[i].ItemArray[11].ToString(), 
                                        Int32.Parse(dTable.Rows[i].ItemArray[12].ToString())));
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            _SQLiteConnetion.Close();
            return list;
        }
    }
}