﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.Data.SQLite.Linq;
using System.Threading;
using System.Drawing;
using CsvHelper;

namespace screencapture
{
    public class PointDataSQLite
    {
        //для анализа нейросетью сигнала могут использоваться следующие источники сигнала:
        //айтрекер, мозгошапка, клавиатура, мышь, вебкамера, микрофон, датчик сердцебиения

        //при работе стимулятора изображения используются: айтрекер, мозгошапка, веб-камера/микрофон (в перспективе)

        SQLiteConnection _SQLiteConnetion;
        SQLiteCommand _SQLiteCommand;
        Result result = new Result();
        Emotions emotions = new Emotions();
        Examination examination = new Examination();
        PostProcessing postProcessing = new PostProcessing();

        string tableName;
        public List<FramePoint> ListPointsData;
        public List<RawPoint> ListPointsRaw;

        public void CreateOrUsingDB(string pathToDB, string tableName)
        {
            this.tableName = tableName;

            if (!File.Exists(pathToDB))
                SQLiteConnection.CreateFile(pathToDB);

            try
            {
                _SQLiteCommand = new SQLiteCommand();
                _SQLiteConnetion = new SQLiteConnection("Data Source=" + pathToDB + "; Synchronous=OFF;");
                _SQLiteConnetion.Open();
                _SQLiteCommand.Connection = _SQLiteConnetion;
                _SQLiteCommand.CommandText = "CREATE TABLE IF NOT EXISTS " + tableName + " (id INTEGER PRIMARY KEY AUTOINCREMENT, gazeX INTEGER, gazeY INTEGER, mouseX INTEGER, mouseY INTEGER, timeStamp DOUBLE, timeGazeFixed DOUBLE, cluster INTEGER, " +
                    "meditation INTEGER, attention INTEGER, alpha1 INTGER, alpha2 INETEGER, beta1 ITNEGER, beta2 INTEGER, gamma1 INTEGER, gamma2 INTEGER, poor_signal INTEGER, blink INTEGER)";
                _SQLiteCommand.ExecuteNonQuery();

            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Disconnected, Ошибка:{0}", ex.Message);
            }
        }

        public void RemovePausedPoints(int id_edu)
        {
            lock (ListPointsData)
            {
                RemoveElements(id_edu);
            }
        }

        private void RemoveElements(int id_edu)
        {
            foreach (var point in ListPointsData)
            {
                if (id_edu == point.dataset_id)
                {
                    ListPointsData.Remove(point);
                    RemoveElements(id_edu);
                    return;
                }
            }
        }

        public void RemovePausedPointsRaw(int id_edu)
        {
            lock (ListPointsRaw)
            {
                RemoveElementsRaw(id_edu);
            }
        }

        private void RemoveElementsRaw(int id_edu)
        {
            foreach (var rawPoint in ListPointsRaw)
            {
                if (id_edu == rawPoint.id)
                {
                    ListPointsRaw.Remove(rawPoint);
                    RemoveElements(id_edu);
                    return;
                }
            }
        }

        //public void SerializeToBD() //записывает результаты в csv
        //{
            //if (Data.exam_state != "dontsave" & Data.examination.state != "start" & Data.eeg_mode != "muse2") //проверяет что их точно нужно запиывать
            //{
            //    lock (ListPointsData)
            //    {
            //        int i = 1;
            //        foreach (var record in Data.education_list) //идет по всем объектам исследования
            //        {
            //            emotions = emotions.GetEmotionsInfo(record.emotion_id); //получает информацию об эмоции
            //            List<FramePoint> csv_list = new List<FramePoint>(); //листы для временного хранения данных
            //            List<MindTraining> mind_list = new List<MindTraining>();
            //            List<RawPoint> raw_list = new List<RawPoint>();

            //            FileInfo fi = new FileInfo(Path.Combine(Data.project.path, record.stimulant));
            //            Data.training_folder = fi.DirectoryName;

            //            foreach (var point in ListPointsData) //проверяет все записи показателей и фиксирует только для текущего объекта исследования
            //            {
            //                if (record.id == point.dataset_id)
            //                    csv_list.Add(point); //добавляет точку в список для последующей записи в csv
            //            }

            //            csv_list = postProcessing.Clusterize(csv_list, 15); //обрабатывает список, разделяя на кластеры и записывая время каждого

            //            foreach (var rawPoint in ListPointsRaw)
            //            {
            //                if (record.id == rawPoint.id)
            //                    raw_list.Add(rawPoint);
            //            }
            //            raw_list = postProcessing.ClusterizeRaw(raw_list, 15); //обрабатывает список, разделяя на кластеры и записывая время каждого

            //            if (Data.edu_code == 1) //если пишем обучающий дата-сет
            //            {
            //                if (csv_list.Count > 0 & record.flag != "R") //если найдено по нему найдена хоть одна фиксация и флаг не равен битому 
            //                {
            //                    string csvpath = Path.Combine(Data.training_folder, Data.respondent.name.Replace(" ", "_") + "_" + emotions.GetEmotionsName(record.emotion_id) + "_training.csv");

            //                    int idx = 1;
            //                    foreach (var point in csv_list)
            //                    {
            //                        mind_list.Add(new MindTraining(idx, point.timeStamp, point.mouseX, point.mouseY, point.gazeX, point.gazeY, point.cluster, point.timeGazeFixed,
            //                            emotions.id, emotions.anger, emotions.fear, emotions.sadness, emotions.happiness, point.meditation, point.attention, point.alpha1, point.alpha2,
            //                                point.beta1, point.beta2, point.gamma1, point.gamma2, point.poor_signal, point.blink));
            //                        idx++;
            //                    }

            //                    using (var writer = new StreamWriter(csvpath))
            //                    using (var csv = new CsvWriter(writer))
            //                    {
            //                        csv.WriteRecords(mind_list);
            //                    }

            //                    idx = 1;
            //                    List<RawPoint> save_list = new List<RawPoint>();
            //                    foreach (var point in raw_list)
            //                    {
            //                        save_list.Add(new RawPoint(idx, point.timeStamp, point.mouseX, point.mouseY, point.gazeX, point.gazeY, point.cluster, point.timeGazeFixed,
            //                            emotions.id, emotions.anger, emotions.fear, emotions.sadness, emotions.happiness, point.raw));
            //                        idx++;
            //                    }

            //                    csvpath = Path.Combine(Data.training_folder, Data.respondent.name.Replace(" ", "_") + "_" + emotions.GetEmotionsName(record.emotion_id) + "_training_raw.csv");
            //                    using (var writer = new StreamWriter(csvpath))
            //                    using (var csv = new CsvWriter(writer))
            //                    {
            //                        csv.WriteRecords(save_list);
            //                    }

            //                    result.SaveTrainingResult(record.id, csvpath);
            //                    result.UpdateTrainingVideo(record.id, Data.pathtovideo);
            //                }
            //            }
            //            else
            //            {
            //                if (csv_list.Count > 0)
            //                {
            //                    string csvpath = Path.Combine(Data.training_folder, Data.respondent.name.Replace(" ", "_") + "_" + i + "_outputdata.csv");

            //                    using (var writer = new StreamWriter(csvpath))
            //                    using (var csv = new CsvWriter(writer))
            //                    {
            //                        csv.WriteRecords(csv_list);
            //                    }

            //                    result.SaveTrainingResult(record.id, csvpath);
            //                    result.UpdateTrainingVideo(record.id, Data.pathtovideo);

            //                    i++;
            //                }
            //            }
            //        }
            //    }
            //}
            //else
            //    Data.exam_state = "next";
        //}

        //public void LogWriting(double tempGazeX, double tempGazeY, double mouseCursorX, double mouseCursorY, double timeCounter, double timeFixed, int cluster,
        //    int meditation, int attention, int alpha1, int alpha2, int beta1, int beta2, int gamma1, int gamma2, int poor_signal, int blink, int raw)
        //{
        //    Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
        //    lock (ListPointsData)
        //    {
        //        //if (Data.paused)
        //        //{
        //        //    //if (Data.id_edu != 0)
        //        //    //{
        //        //    //    this.RemovePausedPoints(Data.id_edu);
        //        //    //    Data.id_edu = 0;
        //        //    //}
        //        //}
        //        //else
        //            //this.ListPointsData.Add(new FramePoint(Data.id_edu, (int)tempGazeX, (int)tempGazeY, (int)mouseCursorX, (int)mouseCursorY, timeCounter, timeFixed,
        //            //cluster, meditation, attention, alpha1, alpha2, beta1, beta2, gamma1, gamma2, poor_signal, blink, raw));
        //    }
        //}

        //public void RawWriting(double timeCounter, double mouseCursorX, double mouseCursorY, int tempGazeX, int tempGazeY, int cluster, double timeFixed, int raw)
        //{
        //    Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
        //    lock (ListPointsRaw)
        //    {
        //        //if (Data.paused)
        //        //{
        //        //    //if (Data.id_edu != 0)
        //        //    //{
        //        //    //    //this.RemovePausedPoints(Data.id_edu);
        //        //    //    //Data.id_edu = 0;
        //        //    //}
        //        //}
        //        //else
        //            //this.ListPointsRaw.Add(new RawPoint(Data.id_edu, timeCounter, (int)mouseCursorX, (int)mouseCursorY, (int)tempGazeX, (int)tempGazeY, cluster, timeFixed, 0, 0, 0, 0, 0, raw));
        //    }
        //}
    }
}