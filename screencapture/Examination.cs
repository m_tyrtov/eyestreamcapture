﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using System.Data.SQLite.Linq;
using System.Windows.Forms;
using System.Data;
using System.Configuration;

namespace screencapture
{
    class Examination
    {
        SQLiteConnection _SQLiteConnetion;
        SQLiteCommand _SQLiteCommand;
        SQLiteDataReader _SQLiteDataReader;

        private string pathToDB = ConfigurationManager.AppSettings["Default_Project_Directory"] + @"\project.sqlite";
        private string tableName = "examination";
        
        public int id { get; set; }
        public int merge_id { get; set; } //id исследования
        public int respondent_id { get; set; } //id респондента
        public string name { get; set; } //название исследования
        public string description { get; set; } //описание исследования
        public string datetime { get; set; } //время создаия исследования
        public string state { get; set; } //статус исследования

        public Examination() { }

        private Examination(int id, int merge_id, int respondent_id, string name, string description, string datetime, string state)
        {
            this.id = id;
            this.merge_id = merge_id;
            this.respondent_id = respondent_id;
            this.name = name;
            this.description = description;
            this.datetime = datetime;
            this.state = state;
        }

        private void CreateOrUsingDB() //создает таблицу или БД, если их еще нет
        {
            if (!File.Exists(pathToDB))
                SQLiteConnection.CreateFile(pathToDB);
            try
            {
                _SQLiteCommand = new SQLiteCommand();
                _SQLiteConnetion = new SQLiteConnection("Data Source=" + pathToDB + "; Synchronous=OFF;");
                _SQLiteConnetion.Open();
                _SQLiteCommand.Connection = _SQLiteConnetion;
                _SQLiteCommand.CommandText = "CREATE TABLE IF NOT EXISTS " + tableName +
                    " (id INTEGER PRIMARY KEY AUTOINCREMENT, merge_id INTEGER, respondent_id INTEGER, name TEXT, description TEXT, datetime TEXT, state TEXT)";
                _SQLiteCommand.ExecuteNonQuery();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Disconnected, Ошибка:{0}", ex.Message);
            }
            finally
            {
                _SQLiteConnetion.Close();
            }
        }

        private void ConnectToDB() //метод подключения к бд
        {
            CreateOrUsingDB();

            _SQLiteCommand = new SQLiteCommand();
            _SQLiteConnetion = new SQLiteConnection(@"Data Source=" + pathToDB);
            _SQLiteCommand.Connection = _SQLiteConnetion;
            _SQLiteConnetion.Open();
        }

        private void CommandSQLQuery(string sqlQuery) //метод для SQL запросов к БД
        {
            ConnectToDB();
            _SQLiteCommand.CommandText = sqlQuery;
            _SQLiteCommand.ExecuteNonQuery();
            _SQLiteConnetion.Close();
        }

        public List<Examination> GetListExaminationsItems(int merge_id) //получает информацию о всех исследованиях с текущим стимулятором
        {
            ConnectToDB();

            List<Examination> list = new List<Examination>();
            DataTable dTable = new DataTable();
            String sqlQuery;

            try
            {
                sqlQuery = "SELECT * FROM " + tableName + " WHERE merge_id = '" + merge_id + "'";
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, _SQLiteConnetion);
                adapter.Fill(dTable);

                if (dTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dTable.Rows.Count; i++)
                    {
                        list.Add(new Examination(Int32.Parse(dTable.Rows[i].ItemArray[0].ToString()),
                            Int32.Parse(dTable.Rows[i].ItemArray[1].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[2].ToString()),
                                dTable.Rows[i].ItemArray[3].ToString(), dTable.Rows[i].ItemArray[4].ToString(), dTable.Rows[i].ItemArray[5].ToString(), dTable.Rows[i].ItemArray[6].ToString()));
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            _SQLiteConnetion.Close();

            return list;
        }

        public int AddNewExamination(int merge_id, int respondent_id, string name, string description) //добавляет новое исследование в БД
        {
            CommandSQLQuery("INSERT INTO " + tableName + " ( merge_id, respondent_id, name, description, datetime, state) "
                + "VALUES('" + merge_id + "', '" + respondent_id + "', '" + name + "', '" + description + "', datetime('now','localtime'), 'start'); ");

            Directory.CreateDirectory(Path.Combine(Data.project.path, Data.merge.name, name, "training"));
            return GetLastId();
        }

        private int GetLastId() //получает id последнего исследования
        {
            ConnectToDB();
            _SQLiteCommand.CommandText = "SELECT MAX(id) FROM " + tableName;

            int new_id = Int32.Parse(_SQLiteCommand.ExecuteScalar().ToString());

            return new_id;
        }

        public string CheckState(int id) //проверяет статус исследования по id исследования
        {
            ConnectToDB();
            _SQLiteCommand.CommandText = "SELECT * FROM " + tableName + " WHERE id = '" + id + "'";
            _SQLiteDataReader = _SQLiteCommand.ExecuteReader();

            string state = "";

            while (_SQLiteDataReader.Read())
            {
                state = _SQLiteDataReader[6].ToString();
            }

            _SQLiteConnetion.Close();

            return state;
        }

        public void EditState(int id, string state) //меняет статус исследования (тестовый прогон, боевой, завершено и тд)
        {
            CommandSQLQuery("UPDATE " + tableName + " SET state = '" + state + "' WHERE id = '" + id + "'");
        }
    }
}
