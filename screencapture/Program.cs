﻿using screencapture.Forms;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;

namespace screencapture
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }

    //класс отвечает за передачу данных между экранными формами
    static class Data
    {
        //muse 2
        public static List<EncefalogramNamed> museList = new List<EncefalogramNamed>();
        public static string eeg_mode { get; set; }

        //отвечает за отмену измеений на форме редактирования записи из справочника
        public static bool cancel { get; set; } = false; //form control
        //отвечает за режим открытия формы редактирования записи из справочника
        public static string mode { get; set; } //form control
        //статус обучения
        public static int edu_code { get; set; } = 0;
        //id обучающего стимула
        public static int id_edu { get; set; } = 0;
        //путь до видео
        public static string pathtovideo { get; set; }
        //предсохраненный путь до директории с выбором картинок
        public static string pathtostims { get; set; } = "";

        public static DateTime startVideoTime { get; set; }

        public static List<Result> education_list = new List<Result>();
        public static string training_folder { get; set; }

        public static bool paused { get; set; } = false;
        public static string exam_state { get; set; } = "next";
        public static int poor_signal { get; set; }

        public static Emotions emotion { get; set; }
        public static Respondents respondent { get; set; }
        public static Projects project { get; set; }
        public static Stimulants stimulants { get; set; }
        public static MergeControl merge { get; set; }
        public static TargetAudience target { get; set; }
        public static Products product { get; set; }
        public static Examination examination { get; set; }
        public static Result result { get; set; } = new Result();

        public static List<MindTraining> mindList = new List<MindTraining>();
        public static string tempDir { get; set; }
        public static string picPath { get; set; }
    }
    
}
