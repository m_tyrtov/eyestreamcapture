﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace screencapture.Forms
{
    public partial class FormLoadStimulantsAssociation : Form
    {
        FileSystem fileSystem = new FileSystem();
        TargetAudience target = new TargetAudience();
        Result result = new Result();
        private bool pic_edit = false;
        private string dir_objects = Path.Combine(Data.merge.name, Data.examination.name, "training");

        public FormLoadStimulantsAssociation()
        {
            InitializeComponent();
            this.ShowInTaskbar = false;

            AssocBox.Text = Data.result.stim_name;
            pathBox.Text = Data.result.stimulant;

            if (File.Exists(Path.Combine(Data.project.path, dir_objects, Data.result.stimulant)))
                try
                {
                    previewBox.Image = Image.FromFile(Path.Combine(Data.project.path, dir_objects, Data.result.stimulant));
                }
                catch
                {
                    MessageBox.Show("Не корректное изображение, загрузите другое!", "Eye Stream Capture");
                }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //if ((AssocBox.Text.Length > 0) & (pathBox.Text.Length > 0))
            if ((AssocBox.Text.Length > 0))
            {
                //if (File.Exists(pathBox.Text) || File.Exists(Path.Combine(dir_objects, pathBox.Text))) //подумать над условием..!
                //{                    
                    result.SetFlag(Data.result.id, "N");

                    Data.result.stim_name = AssocBox.Text; //записывает ассоциацию
                    Data.result.flag = "N"; //записывает флаг

                    Directory.CreateDirectory(Path.Combine(Data.project.path, dir_objects)); //создает ее, если она еще не существует
                    result.AddTrainingItem(Data.examination.id, Data.project.product_id, Data.merge.id, Data.emotion.id, Data.respondent.id, AssocBox.Text, Path.Combine(dir_objects, Data.result.stimulant));

                    Data.cancel = false;

                    this.Close();
                //}
                //else
                //    MessageBox.Show("Не корректный путь до файла!", "Eye Stream Capture");
            }
        }

        private void FormLoadStimulantsAssociation_Load(object sender, EventArgs e)
        {
            emotionLabel.Text = Data.emotion.name;
            label2.Text = "Что конкретно вызывает у вас вызывает «" + Data.emotion.name + "»";
            Data.cancel = true;
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            if(AssocBox.Text.Length > 0)
                System.Diagnostics.Process.Start("https://yandex.ru/images/search?text=" + AssocBox.Text);
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";

            if(string.IsNullOrWhiteSpace(Data.pathtostims))
                openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
            else
                openFileDialog1.InitialDirectory = Data.pathtostims;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileInfo fi = new FileInfo(openFileDialog1.FileName);
                if (fi.DirectoryName != Data.pathtostims)
                    Data.pathtostims = fi.DirectoryName;
                pathBox.Text = openFileDialog1.FileName;

                if (previewBox.Image != null)
                    previewBox.Image.Dispose();

                try
                {
                    previewBox.Image = Image.FromFile(openFileDialog1.FileName);

                    List<TargetAudience> targetAudiences = target.GetListTargetAudinceItems("ASC");
                    string target_name = "";

                    foreach (var item in targetAudiences)
                        if (item.id == Data.respondent.audince_id)
                            target_name = item.name;

                    Data.result.stimulant = Data.emotion.name + "_" + target_name + "_" + Data.respondent.name.Replace(" ", "_") + ".jpg"; //записывает название изображения
                    previewBox.Image.Dispose();
                    
                    if (pic_edit)
                        previewBox.Image = Image.FromFile(fileSystem.CopyFileTraining(pathBox.Text, Data.emotion.name + "_" + target_name + "_" + Data.respondent.name.Replace(" ", "_")));
                    else
                        previewBox.Image = Image.FromFile(fileSystem.CopyFileTraining(Path.Combine(dir_objects, pathBox.Text), Data.emotion.name + "_" + target_name + "_" + Data.respondent.name.Replace(" ", "_")));
                
                    File.Delete(openFileDialog1.FileName);
                }
                catch
                {
                    
                }

                pic_edit = true;
            }
        }

        private void previewBox_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Загуглить как открыть картинку стандартным способом");
        }

        private void savePic_Click(object sender, EventArgs e)
        {
            //string dir_objects = Path.Combine(Data.project.path, Data.merge.name, Data.examination.name, "training");

            saveFileDialog1.FileName = AssocBox.Text + ".jpg";

            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = saveFileDialog1.FileName;

            //копируем
            if (File.Exists(saveFileDialog1.FileName))
            {
                File.Delete(saveFileDialog1.FileName);
                File.Copy(Path.Combine(dir_objects, pathBox.Text), saveFileDialog1.FileName);
            }
            else
            {
                File.Copy(Path.Combine(dir_objects, pathBox.Text), saveFileDialog1.FileName);
            }

            //if (Data.pathtostims.Length > 0)
            //{
            //if(File.Exists(Path.Combine(Data.pathtostims, AssocBox.Text + ".jpg")))
            //{
            //    DialogResult dialogResult = MessageBox.Show("Файл существует. Заменить?", "Eye Stream Capture", MessageBoxButtons.OKCancel);
            //    if (dialogResult == DialogResult.OK)
            //    {
            //        File.Delete(Path.Combine(Data.pathtostims, AssocBox.Text + ".jpg"));
            //        File.Copy(Path.Combine(dir_objects, pathBox.Text), Path.Combine(Data.pathtostims, AssocBox.Text + ".jpg"));
            //    }
            //}
            //else
            //{
            //    File.Copy(Path.Combine(dir_objects, pathBox.Text), Path.Combine(Data.pathtostims, AssocBox.Text + ".jpg"));
            //}
            //} 
            //else
            //{
            //string new_dir = Path.Combine(Data.project.path, Data.merge.name, Data.examination.name, "new img");
            //Directory.CreateDirectory(new_dir);
            //Data.pathtostims = new_dir;

            //if (File.Exists(Path.Combine(new_dir, AssocBox.Text + ".jpg")))
            //{
            //    DialogResult dialogResult = MessageBox.Show("Файл существует. Заменить?", "Eye Stream Capture", MessageBoxButtons.OKCancel);
            //    if (dialogResult == DialogResult.OK)
            //    {
            //        File.Delete(Path.Combine(new_dir, AssocBox.Text + ".jpg"));
            //        File.Copy(Path.Combine(dir_objects, pathBox.Text), Path.Combine(new_dir, AssocBox.Text + ".jpg"));
            //    }
            //}
            //else
            //{
            //    File.Copy(Path.Combine(dir_objects, pathBox.Text), Path.Combine(new_dir, AssocBox.Text + ".jpg"));
            //}
            //}  
        }

        private void FormLoadStimulantsAssociation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
