﻿using screencapture.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture
{
    public partial class FormRecordDialog : Form
    {
        VideoRecorder recorder;
        TextBox tbProjectDirectory;
        Button btnRecStop;
        Form form1;
        Examination examination = new Examination();

        public static IsRecording isRecording = IsRecording.No;
        
        public FormRecordDialog(TextBox tbProjectDirectory, VideoRecorder recorder, Button btnRecStop, Form form1)
        {
            this.btnRecStop = btnRecStop;
            this.recorder = recorder;
            this.tbProjectDirectory = tbProjectDirectory;
            this.form1 = form1;
            InitializeComponent();
        }

        private void btnStartRec_Click(object sender, EventArgs e)
        {
            examination.AddNewExamination(Data.merge.id, Data.respondent.id, tbName.Text, tbDescription.Text);
            this.Close();
        }

        private void StartRecord()
        {
            try
            {

                switch (Data.merge.stimulant_id)
                {
                    case 1:
                        NamingForm namingForm = new NamingForm();
                        this.Owner = namingForm;
                        namingForm.Show();
                        break;
                    case 2:
                        PictureForm pictureForm = new PictureForm();
                        this.Owner = pictureForm;
                        pictureForm.Show();
                        break;
                }

                BeginInvoke(new MethodInvoker(delegate
                {
                    SynchronizationContext uiContext = SynchronizationContext.Current;

                    btnRecStop.Text = "СТОП";
                    //recorder.StartRecording(tbProjectDirectory.Text, Path.Combine(tbProjectDirectory.Text, tbName.Text), tbName.Text.Replace(" ", "_"), tbDescription.Text);
                    form1.WindowState = FormWindowState.Minimized;
                }));
            }
            catch
            {
                MessageBox.Show("Ошибка при подготовке к запуску исследования!");
            }
            finally
            {
                //examination.AddNewExamination(Data.merge.id, Data.respondent.id, tbName.Text, tbDescription.Text);
                //this.Hide();
            }
        }
        
        private void btnCancelRD_Click(object sender, EventArgs e)
        {
            this.Close();
            recorder.ThinkGearStop();
        }

        private void FormRecordDialog_Load(object sender, EventArgs e)
        {
            tbName.Text = "Исследование_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss");
        }

        private void respondentBrowse_Click(object sender, EventArgs e)
        {
            Data.mode = "select";

            FormRespondents formRespondents = new FormRespondents();
            formRespondents.Owner = this;
            formRespondents.FormClosed += (object s, FormClosedEventArgs args) =>
            {
                if (!Data.cancel)
                    respondentBox.Text = Data.respondent.name;
                else
                {
                    Data.cancel = false;
                    respondentBox.Clear();
                }
            };

            formRespondents.Show();
        }
    }
}