﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.VisualBasic.FileIO;
using CsvHelper;

namespace screencapture.Forms
{
    public partial class VectorForm : Form
    {   
        MergeControl projects = new MergeControl();
        Respondents respondents = new Respondents();
        Examination examinations = new Examination();
        List<Examination> exam_list = new List<Examination>();
        List<MergeControl> merge_list = new List<MergeControl>();

        List<string> errorLog = new List<string>();

        public VectorForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void VectorForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (Data.project.id > 0)
                    UpdateStimList();
            }
            catch
            {
                processButton.Enabled = false;
                MessageBox.Show("Сначала выберите проект!", "Eye Stream Capture");     
            }
        }

        private void UpdateStimList()
        {
            stimBox.Items.Clear();

            try
            {
                merge_list = projects.StimulantsLoad(Data.project.id); //лист связей (проект + стимулятор + настройки)
                if (merge_list.Count > 0)
                {
                    foreach (MergeControl record in merge_list)
                        stimBox.Items.Add(record.name);

                    stimBox.SelectedIndex = 0;
                }
            }
            catch
            {

            }
        }

        private void stimBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateExaminationList();
        }

        private void UpdateExaminationList()
        {
            lbListVideo.Items.Clear();
            exam_list = examinations.GetListExaminationsItems(merge_list[stimBox.SelectedIndex].id);

            if(exam_list.Count > 0)
            {
                foreach (Examination record in exam_list)
                    lbListVideo.Items.Add(record.name);

                lbListVideo.SelectedIndex = 0;
            }
        }

        private void lbListVideo_SelectedIndexChanged(object sender, EventArgs e)
        {
            respBox.Text = respondents.GetInfoRespondentsItem(exam_list[lbListVideo.SelectedIndex].respondent_id).name;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDown1.Enabled = checkBox1.Checked;
            numericUpDown2.Enabled = checkBox1.Checked;

            numericUpDown3.Enabled = checkBox2.Checked;
            numericUpDown4.Enabled = checkBox2.Checked;

            numericUpDown5.Enabled = checkBox3.Checked;
            numericUpDown6.Enabled = checkBox3.Checked;

            numericUpDown7.Enabled = checkBox4.Checked;
            numericUpDown8.Enabled = checkBox4.Checked;
        }

        //по клику на кнопку выбираются тайминги для вырезки в зависимости от отмеченных чек-поинтов
        private void processButton_Click(object sender, EventArgs e)
        {
            if (lbListVideo.Items.Count > 0)
            {
                List<int[]> timeIntervals = new List<int[]>();

                if(checkBox1.Checked)
                    timeIntervals.Add(new int[] { Convert.ToInt32(numericUpDown1.Value), Convert.ToInt32(numericUpDown2.Value) });
                if (checkBox2.Checked)
                    timeIntervals.Add(new int[] { Convert.ToInt32(numericUpDown3.Value), Convert.ToInt32(numericUpDown4.Value) });
                if (checkBox3.Checked)
                    timeIntervals.Add(new int[] { Convert.ToInt32(numericUpDown5.Value), Convert.ToInt32(numericUpDown6.Value) });
                if (checkBox4.Checked)
                    timeIntervals.Add(new int[] { Convert.ToInt32(numericUpDown7.Value), Convert.ToInt32(numericUpDown8.Value) });

                csvProcessor(timeIntervals);
            }
        }

        //обрабатывает дата сет по каждому из выбранных диапазонов
        private void csvProcessor(List<int[]> times)
        {
            foreach (var time in times)
            {
                csvFixer(Path.Combine(
                    Data.project.path,
                    Data.merge.name,
                    exam_list[lbListVideo.SelectedIndex].name,
                    "training"
                ), time);
            }

            foreach (var item in errorLog)
            {
                richTextBox1.AppendText(item + "\r\n");
            }
        }

        //исправляет проблемные файлы в дата-сете
        private void csvFixer(string dirPath, int[] time)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(dirPath);

                foreach (var item in dir.GetFiles())
                {
                    if (item.Name.IndexOf("_raw.csv") > 0)
                    {
                        List<MindTraining> fixedList = csvTimeFixer(csvReader(item.FullName, item.Name));
                        fixedList = csvTrim(fixedList, time[0], time[1]);

                        csvWrite(fixedList, Path.Combine(dir.FullName, "trim_" + time[0].ToString() + "-" + time[1].ToString()), item.Name);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Папка с исследованием пуста", "Eye Stream Capture");
            }
        }

        //записывает отредаченные дата-сеты в соседнюю папку
        private void csvWrite(List<MindTraining> RawPointList, string dirPath, string csvName)
        {
            Directory.CreateDirectory(dirPath);

            using (var writer = new StreamWriter(Path.Combine(dirPath, csvName)))
            using (var csv = new CsvWriter(writer))
            {
                csv.WriteRecords(RawPointList);
            }
        }

        //обрезает csv в выбранных диапазонах
        private List<MindTraining> csvTrim(List<MindTraining> RawPointList, int start, int end)
        {
            List<MindTraining> TrimList = new List<MindTraining>();

            foreach (var item in RawPointList)
            {
                if (float.Parse(item.timeStamp) > start & float.Parse(item.timeStamp) < end)
                {
                    TrimList.Add(item);
                }
            }

            return TrimList;
        }

        //считывает данные из csv в лист классов
        private List<MindTraining> csvReader(string path, string fileName)
        {
            List<MindTraining> RawPointList = new List<MindTraining>();

            using (TextFieldParser parser = new TextFieldParser(path))
            {
                parser.TextFieldType = FieldType.Delimited; 
                parser.SetDelimiters(";");
                int i = 0;
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();
                    if (i != 0)
                        try
                        {
                            RawPointList.Add(new MindTraining(fields[0], fields[1], 0.ToString(), fields[2], fields[3], fields[4], fields[5],
                                fields[6], fields[7], fields[8], fields[9], fields[10], fields[11], fields[12], fields[13], fields[14],
                                    fields[15], fields[16], fields[17], fields[18], fields[19], fields[20], fields[21]));
                        }
                        catch
                        {
                            errorLog.Add(fileName + " ошибка чтения");
                            return new List<MindTraining>();
                        }
                    i++;
                }
            }

            for (var i = 0; i < RawPointList.Count - 1; i++)
            {
                double timeSlot = Math.Round(double.Parse(RawPointList[i + 1].timeStamp) - double.Parse(RawPointList[i].timeStamp), 3);
                double backup = timeSlot;

                if (timeSlot > 0.001)
                {
                    timeSlot = (timeSlot - 0.001) * 1000;
                    double tempSlot = Double.Parse(RawPointList[i].timeStamp);
                    for (var j = 0; j < timeSlot; j++)
                    {
                        tempSlot = tempSlot + 0.001;

                        //порядок верный, просто поменять ссылки
                        RawPointList.Insert(i + j + 1, new MindTraining(RawPointList[i].id, tempSlot.ToString(), backup.ToString(), RawPointList[i].emo_id, 
                            RawPointList[i].anger, RawPointList[i].fear, RawPointList[i].sadness, RawPointList[i].happiness, RawPointList[i].mouseX, RawPointList[i].mouseY,
                                RawPointList[i].gazeX, RawPointList[i].gazeY, RawPointList[i].meditation, RawPointList[i].attention, RawPointList[i].alpha1, RawPointList[i].alpha2, 
                                    RawPointList[i].beta1, RawPointList[i].beta2, RawPointList[i].gamma1, RawPointList[i].gamma2, RawPointList[i].poor_signal, RawPointList[i].blink, RawPointList[i].raw));
                    }
                }
            }
            
            return RawPointList;
        }

        //исправляет timestamp, если он начинается не с нуля
        private static List<MindTraining> csvTimeFixer(List<MindTraining> RawPointList)
        {
            try
            {
                float setNull = float.Parse(RawPointList[0].timeStamp);

                //исправили таймштамп
                for (var i = 0; i < RawPointList.Count; i++)
                {
                    MindTraining tempPoint = RawPointList[i];
                    tempPoint.timeStamp = Math.Round((double.Parse(tempPoint.timeStamp) - setNull), 3).ToString();
                    RawPointList[i] = tempPoint;
                }

                return RawPointList;
            }
            catch
            {
                return new List<MindTraining>();
            }
        }
    }

    public struct MindTraining
    {
        public string id { get; set; }
        public string timeStamp { get; set; }
        public string timeSlot { get; set; }

        public string emo_id { get; set; }
        public string anger { get; set; }
        public string fear { get; set; }
        public string sadness { get; set; }
        public string happiness { get; set; }

        public string mouseX { get; set; }
        public string mouseY { get; set; }

        public string gazeX { get; set; }
        public string gazeY { get; set; }

        public string meditation { get; set; }
        public string attention { get; set; }
        public string alpha1 { get; set; }
        public string alpha2 { get; set; }
        public string beta1 { get; set; }
        public string beta2 { get; set; }
        public string gamma1 { get; set; }
        public string gamma2 { get; set; }
        public string poor_signal { get; set; }
        public string blink { get; set; }

        public string raw { get; set; }

        public MindTraining(string id, string timeStamp, string timeSlot, string emo_id, string anger, string fear, string sadness, string happiness, 
            string mouseX, string mouseY, string gazeX, string gazeY, string meditation, string attention, string alpha1, string alpha2,
                string beta1, string beta2, string gamma1, string gamma2, string poor_signal, string blink, string raw)
        {
            this.id = id;
            this.timeStamp = timeStamp;
            this.timeSlot = timeSlot;

            this.emo_id = emo_id;
            this.anger = anger;
            this.fear = fear;
            this.sadness = sadness;
            this.happiness = happiness;

            this.mouseX = mouseX;
            this.mouseY = mouseY;

            this.gazeX = gazeX;
            this.gazeY = gazeY;

            this.meditation = meditation;
            this.attention = attention;
            this.alpha1 = alpha1;
            this.alpha2 = alpha2;
            this.beta1 = beta1;
            this.beta2 = beta2;
            this.gamma1 = gamma1;
            this.gamma2 = gamma2;
            this.poor_signal = poor_signal;
            this.blink = blink;

            this.raw = raw;
        }
    }
}
