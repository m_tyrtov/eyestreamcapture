﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class FormAnalytics : Form
    {
        VideoProcessor _videoProcessor;
        TextBox tbProjectDirectory;
        Label labelNameProjectForm1;

        Examination examinations = new Examination();
        List<Examination> exam_list = new List<Examination>();

        public FormAnalytics(TextBox tbProjectDirectory, Label labelNameProjectForm1)
        {
            InitializeComponent();
            this.tbProjectDirectory = tbProjectDirectory;
            this.labelNameProjectForm1 = labelNameProjectForm1;
        }

        private void FormAnalytics_Load(object sender, EventArgs e)
        {            
            this.labelNameProject.Text = labelNameProjectForm1.Text;
            //загрузка списка исследований

            UpdateExaminationList();
            //DirectoryInfo dir = new DirectoryInfo(tbProjectDirectory.Text);
            //FileInfo[] files = dir.GetFiles("*.avi");
            //foreach (FileInfo f in files)
            //{
            //    lbListVideo.Items.Add(f.ToString().Replace(".avi", ""));
            //}
            //this.lbListVideo.SetSelected(0, true);
        }

        private void UpdateExaminationList()
        {
            lbListVideo.Items.Clear();
            exam_list = examinations.GetListExaminationsItems(Data.merge.id);

            foreach (Examination record in exam_list)
            {
                lbListVideo.Items.Add(record.name);
            }
        }

        private void lbListVideo_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelNameResearch.Text = lbListVideo.SelectedItem.ToString();
        }

        private void tbDifPercent_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void btnGazeMarking_Click(object sender, EventArgs e)
        {
            //Form1.formPleaseWait.Show();
            this.Enabled = false;

            _videoProcessor = new VideoProcessor();
            _videoProcessor.GazeMarking(Path.Combine(tbProjectDirectory.Text, labelNameResearch.Text) + ".avi", Path.Combine(tbProjectDirectory.Text, labelNameProject.Text + ".sqlite"), labelNameResearch.Text);

            this.Enabled = true;
            //Form1.formPleaseWait.Hide();
        }
    }
}
