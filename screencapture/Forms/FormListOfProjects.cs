﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace screencapture
{
    public partial class FormListOfProjects : Form
    {
        private int selectedProject;
        private TextBox tbDirectoryProject;

        private Projects project = new Projects();
        private List<Projects> list = new List<Projects>();

        public FormListOfProjects(TextBox tbDirectoryProject)
        {
            InitializeComponent();
            this.ShowInTaskbar = false;
            this.tbDirectoryProject = tbDirectoryProject;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            SelectedThis();
        }

        private void FormListOfProject_Load(object sender, EventArgs e)
        {
            try
            {
                list = project.GetList();

                foreach (Projects record in list)
                {
                    lbListProjects.Items.Add(record.name);
                }

                if (list.Count > 0)
                    lbListProjects.SelectedIndex = 0;
            }
            catch
            {

            }
        }

        private void lbListProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                selectedProject = lbListProjects.SelectedIndex;
            }
            catch
            {

            }
        }

        private void lbListProjects_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            SelectedThis();
        }

        private void SelectedThis()
        {
            try
            {
                Data.project = list[selectedProject];
                tbDirectoryProject.Text = list[selectedProject].path;
            }
            catch
            {

            }
            this.Close();
        }

        private void FormListOfProjects_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter) //по нажатию enter выбирает проект
            {
                SelectedThis();
            }

            if (e.KeyCode == Keys.Escape) //закрывает форму по Esc
            {
                this.Close();
            }
        }
    }
}
