﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace screencapture.Forms
{
    public partial class FormLoadStimulants : Form
    {
        Result result = new Result();
        Examination examination = new Examination();

        Emotions emotions = new Emotions();
        List<Emotions> list = new List<Emotions>();

        Dictionary<int, string> global_list = new Dictionary<int, string>(); //path
        Dictionary<int, string> assoc_list = new Dictionary<int, string>(); //name
        Dictionary<int, string> flag_list = new Dictionary<int, string>(); //flag

        Dictionary<int, Result> result_list = new Dictionary<int, Result>();

        private int current_index;

        public FormLoadStimulants()
        {
            InitializeComponent();
            this.ShowInTaskbar = false;
        }

        private void FormLoadStimulants_Load(object sender, EventArgs e)
        {
            foreach (Emotions record in list)
            {
                global_list.Add(record.id, null);
                assoc_list.Add(record.id, null);
                flag_list.Add(record.id, null);
                result_list.Add(record.id, null);
            }

            if (result.CheckTraining(Data.examination.id))
            {
                Data.education_list = result.GetListExaminationsItems(Data.examination.id);
                foreach (var item in Data.education_list)
                {
                    if (File.Exists(Path.Combine(Data.project.path, item.stimulant)))
                    {
                        FileInfo fi = new FileInfo(Path.Combine(Data.project.path, item.stimulant));
                        global_list[item.emotion_id] = fi.Name;
                    }
                    assoc_list[item.emotion_id] = item.stim_name;
                    flag_list[item.emotion_id] = item.flag;
                    result_list[item.emotion_id] = item;
                }
            }

            DBListWithdraw();
            list = emotions.GetListEmotionsItems("ASC", "Все");
                
            textBox1.Text = Data.respondent.name;
            textBox2.Text = Data.examination.name;

            if (tableView.Items.Count > 0)
            {
                tableView.Items[0].Selected = true;
                tableView.Select();
                tableView.HideSelection = false;
            }
        }

        private void DBListWithdraw(string select = "Все")
        {
            tableView.Items.Clear();
            list = emotions.GetListEmotionsItems("ASC", select);

            foreach (Emotions record in list)
            {
                ListViewItem item = new ListViewItem(record.sort.ToString());

                item.SubItems.Add(record.name);

                if (assoc_list.ContainsKey(record.id))
                    item.SubItems.Add(assoc_list[record.id]);
                else
                    item.SubItems.Add("");

                if (flag_list.ContainsKey(record.id))
                    item.SubItems.Add(flag_list[record.id]);
                else
                    item.SubItems.Add("");

                item.SubItems.Add(record.anger.ToString());
                item.SubItems.Add(record.fear.ToString());
                item.SubItems.Add(record.sadness.ToString());
                item.SubItems.Add(record.happiness.ToString());

                item.SubItems.Add(Checked(record.goldman));
                item.SubItems.Add(Checked(record.microsoft));
                item.SubItems.Add(Checked(record.leontyev));
                item.SubItems.Add(Checked(record.emodetect));

                if (global_list.ContainsKey(record.id))
                {
                    item.SubItems.Add(global_list[record.id]);
                }

                tableView.Items.AddRange(new ListViewItem[] { item });
            }
        }

        private string Checked(bool value)
        {
            if (value)
                return "+";
            else
                return "";
        }
        
        private void loadButton_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioButton = (RadioButton)sender;
            if (radioButton.Checked)
            {
                DBListWithdraw(radioButton.Text);
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            CreateEditeForm("add");
        }

        private void CreateEditeForm(string mode)
        {
            Data.mode = mode;
            FormEmotionsDataEdit FormDataEdit = new FormEmotionsDataEdit();

            FormDataEdit.Owner = this;
            FormDataEdit.FormClosed += (object s, FormClosedEventArgs args) =>
            {
                DBListWithdraw();
            };

            FormDataEdit.Show();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (CheckCurrentRecord())
                CreateEditeForm("edit");
        }

        private bool CheckCurrentRecord()
        {
            if (this.tableView.SelectedItems.Count > 0)
            {
                foreach (ListViewItem item in this.tableView.SelectedItems)
                {
                    try
                    {
                        Data.result.id = result_list[item.Index + 1].id; //костыль, нужно исправлять!
                    }
                    catch{}
                    
                    Data.emotion = list[item.Index];
                    current_index = item.Index;
                    
                    switch (item.SubItems.Count)
                    {
                        case 12:
                            Data.result.stimulant = item.SubItems[11].Text;
                            Data.result.stim_name = string.Empty;
                            break;
                        case 13:
                            Data.result.stimulant = item.SubItems[12].Text;
                            Data.result.stim_name = item.SubItems[2].Text;
                            break;
                        default:
                            Data.result.stimulant = string.Empty;
                            Data.result.stim_name = string.Empty;
                            break;
                    }
                }  
                return true;
            }
            else
                return false;
        }

        private void tableView_DoubleClick(object sender, EventArgs e)
        {
            button2.PerformClick();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (CheckCurrentRecord())
            {
                //FormLoadStimulantsAssociation FormLoadStimulantsAssociation = new FormLoadStimulantsAssociation();

                //FormLoadStimulantsAssociation.Owner = this;
                //FormLoadStimulantsAssociation.FormClosed += (object s, FormClosedEventArgs args) =>
                //{
                //    if(!Data.cancel)
                //    {
                //        //в конец
                //        if (tableView.Items[current_index].SubItems.Count < 13)
                //            tableView.Items[current_index].SubItems.Add(Data.result.stimulant);
                //        else
                //            tableView.Items[current_index].SubItems[12].Text = Data.result.stimulant;

                //        tableView.Items[current_index].SubItems[2].Text = Data.result.stim_name;
                //        tableView.Items[current_index].SubItems[3].Text = Data.result.flag;

                //        global_list[list[current_index].id] = Data.result.stimulant;
                //        assoc_list[list[current_index].id] = Data.result.stim_name;
                //        flag_list[list[current_index].id] = Data.result.flag;
                //    }
                //    else
                //    {
                //        Data.cancel = false;
                //    }
                //};

                //FormLoadStimulantsAssociation.Show();

                BrowserTestingForm browserTestingForm = new BrowserTestingForm();
                browserTestingForm.Show();
            }
            else
            {
                MessageBox.Show("Сначала выберите запись!", "Eye Stream Capture");
            }
        }

        private void FormLoadStimulants_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void tableView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button2.PerformClick();
            }
        }
    }
}
