﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.VFW;
using AForge.Video.DirectShow;
using ThinkGearNET;
using screencapture.Properties;
using System.Configuration;


namespace screencapture.Forms
{
    public partial class FormDeviceCalibration : Form
    {
        //для записи веб-камеры
        private VideoCaptureDevice device;
        private VideoCapabilities videoCfg;
        private EyeTracker eyeTracker;

        string comPort = ConfigurationManager.AppSettings["COM_Port"];

        public static ThinkGearWrapper thinkGear = new ThinkGearWrapper(); //шапка
        public static bool connectStatus;

        public FormDeviceCalibration()
        {
            InitializeComponent();
        }

        private void FormDeviceCalibration_Load(object sender, EventArgs e)
        {
            try
            {
                cbDevice.DataSource = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                cbDevice.SelectedIndex = 0;
                cbDevice.DisplayMember = "Name";

                eyeTracker = EyeTracker.GetEyeTracker();
                eyeTracker.StartEyeStream();
            }
            catch
            {
                MessageBox.Show("Не обнаружено ни одной веб-камеры!");
                this.Close();
            }
        }

        private void cbDevice_SelectedIndexChanged(object sender, EventArgs e)
        {
            var filter = cbDevice.SelectedItem as FilterInfo;
            device = new VideoCaptureDevice(filter.MonikerString);

            foreach (var item in device.VideoCapabilities)
            {
                if (item.FrameSize.Width == 640 & item.FrameSize.Height == 480 & item.AverageFrameRate == 30)
                {
                    videoCfg = item;

                    device.Start();
                    device.NewFrame += device_NewFrame;

                    break;
                }     
            }
        }

        private void device_NewFrame(object sender, NewFrameEventArgs e)
        {
            Bitmap frame = (Bitmap)e.Frame.Clone();
            videoBox.Image = new Bitmap(frame, new Size(videoBox.Width, videoBox.Height));
            frame.Dispose();

            BeginInvoke(new MethodInvoker(delegate
            {
                SynchronizationContext uiContext = SynchronizationContext.Current;

                if(eyeTracker.gazeX != 0 & eyeTracker.gazeY != 0)
                {
                    gazeXlabel.Text = eyeTracker.gazeX.ToString();
                    gazeYlabel.Text = eyeTracker.gazeY.ToString();
                }
                else
                {
                    gazeXlabel.Text = "Не найден!";
                    gazeYlabel.Text = "Не найден!";
                }
            }));
        }

        private void FormDeviceCalibration_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                device.NewFrame -= device_NewFrame;
                videoBox.Image.Dispose();
                device.SignalToStop();
                eyeTracker.StopEyeStream();
            }
            catch
            {

            }
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (!connectStatus)
            {
                connectStatus = true;
                thinkGear.ThinkGearChanged += thinkGear_ThinkGearChanged;
                thinkGear.Connect(comPort, ThinkGear.BAUD_57600, true);

                SetUI(false);
            }
            else
            {
                connectStatus = false;
                thinkGear.ThinkGearChanged -= thinkGear_ThinkGearChanged;
                thinkGear.Disconnect();

                SetUI(true);
            }
        }

        void thinkGear_ThinkGearChanged(object sender, ThinkGearChangedEventArgs e) //событие на обновление данных с шапки
        {
            BeginInvoke(new MethodInvoker(delegate
            {
                SynchronizationContext uiContext = SynchronizationContext.Current;

                //attention - вот по идее это и есть контроллируемый головой параметр
                attentionVal.Text = e.ThinkGearState.Attention.ToString() + "%";
                //attentionBar.Value = Convert.ToInt32(e.ThinkGearState.Attention.ToString());

                //meditation
                meditationVal.Text = e.ThinkGearState.Meditation.ToString() + "%";
                //meditationBar.Value = Convert.ToInt32(e.ThinkGearState.Meditation.ToString());

                switch (e.ThinkGearState.PoorSignal)
                {
                    case 0:
                        connectBox.Image = Resources.connected;
                        break;
                    case 200:
                        connectBox.Image = Resources.nosignal;
                        break;
                    default:
                        connectBox.Image = Resources.wait;
                        break;
                }

            }));
            Thread.Sleep(10);
        }

        private void SetUI(bool status)
        {
            if (status)
            {
                connectButton.Text = "Подключиться";
                infolabel.Text = "Отключено";
            }
            else
            {
                infolabel.Text = "Подключено";
                connectButton.Text = "Отключиться";
            }
        }
    }
}