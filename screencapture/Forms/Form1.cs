﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using screencapture.Forms;
using System.Diagnostics;
using System.Threading;
using CsvHelper;
//using System.Configuration;

namespace screencapture
{
    public partial class Form1 : Form
    {
        public VideoRecorder recorder;
        public static IsRecording isRecording = IsRecording.No;
        private Projects project = new Projects();

        Respondents respondents = new Respondents();
        Emotions emotions = new Emotions();

        MergeControl projects = new MergeControl();
        List<MergeControl> merge_list = new List<MergeControl>();

        Examination examinations = new Examination();
        List<Examination> exam_list = new List<Examination>();
        
        FileSystem fileSystem = new FileSystem();
        Result result = new Result();
        //Muse muse = new Muse();
        MuseBluetooth museBluetooth = new MuseBluetooth();

        public Form1()
        {
            Microsoft.Win32.RegistryKey reg0 = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Classes\\eyegaze\\");
            if (reg0 == null)
            {
                MessageBox.Show("Для полноценного функционирования программы необходимо установить драйвера Tobii!", "Eye Stream Capture");
                //this.Close();
            }
            else
            {
                recorder = new VideoRecorder();
            }
            Data.tempDir = ConfigurationManager.AppSettings["Temp_Folder"];
            InitializeComponent();
            this.KeyPreview = true;
        }

        private async void btnRecStop_Click(object sender, EventArgs e)
        {
            if (Data.project.id != 0)
            {
                if (stimBoxList.SelectedItems.Count > 0)
                {
                    //TODO: пока кнопка актульна только для экспериментального стимулятора
                    //if (result.CheckTrainingPreset(Data.examination.id))
                    //{
                    //    examinations.EditState(Data.examination.id, "load");
                    //    Data.edu_code = 0;

                    //    FormPleaseWait formPleaseWait = new FormPleaseWait();
                    //    formPleaseWait.Show();
                    //    Thread.CurrentThread.IsBackground = true;
                    //    if (recorder.ThinkGearStart())
                    //    {
                    //        Thread.Sleep(50); // <-- зачем это здесь?
                    //        Form1.isRecording = IsRecording.Yes;
                    //        formPleaseWait.Close();
                    //        StartRecord();
                    //    }
                    //}
                    //else
                    //    MessageBox.Show("Сначала загрузите обучающие стимуляторы!", "Eye Stream Capture");

                    if(Data.merge.stimulant_id == 4)
                    {

                        if (Data.eeg_mode != "muse2")
                        {
                            FormPleaseWait formPleaseWait = new FormPleaseWait();
                            formPleaseWait.Show();
                            Thread.CurrentThread.IsBackground = true;
                            if (recorder.ThinkGearStart())
                            {
                                Thread.Sleep(50); // <-- зачем это здесь?
                                Form1.isRecording = IsRecording.Yes;
                                formPleaseWait.Close();
                                StartRecord();
                            }
                        }
                        else
                        {
                            //FormPleaseWait formPleaseWait = new FormPleaseWait();
                            //formPleaseWait.Show();
                            //dataSetButton.Enabled = false;
                            //try
                            //{
                            //    await museBluetooth.StartFeed();
                            //    formPleaseWait.Close();
                            //    StartRecord();
                            //}
                            //catch
                            //{
                            //    MessageBox.Show("Шапка занята другим процессом!");
                            //}

                            Process.Start(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "start.bat"));
                            Thread.Sleep(1000);

                            Form1.isRecording = IsRecording.Yes;
                            StartRecord();
                        }
                    }
                }
                else
                    MessageBox.Show("Выберите стимулятор из списка или добавьте новый!", "Eye Stream Capture");
            }
            else
                MessageBox.Show("Создайте или выберите проект!", "Eye Stream Capture");
        }

        private void StopRecording()
        {
            this.WindowState = FormWindowState.Normal;
            //formPleaseWait.Show();
            Cursor.Show();
            this.Enabled = false;
            recorder.EndRecording();
            this.Enabled = true;
            UpdateExaminationList();
            btnRecStop.Text = "Начать исследование";
            isRecording = IsRecording.No;
            //tbProjectDirectory_TextChanged(sender, e);
            //formPleaseWait.Hide();

            stopPyMuse();
        }

        private void stopPyMuse()
        {
            FileStream fs = File.Create(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "stop.txt"));
            fs.Close();
            Thread.Sleep(700);
        }

        private void btnCreateProject_Click(object sender, EventArgs e)
        {
            FormCreateProject formCreateProject = new FormCreateProject(this.tbProjectDirectory, this.labelNameProject);
            formCreateProject.Owner = this;
            formCreateProject.FormClosed += (object s, FormClosedEventArgs args) =>
            {
                UpdateStimList();
            };

            formCreateProject.Show();
        }

        private void btnBrowseProject_Click(object sender, EventArgs e)
        {
            FormListOfProjects FormListOfProjects = new FormListOfProjects(this.tbProjectDirectory);
            FormListOfProjects.Owner = this;
            FormListOfProjects.FormClosed += (object s, FormClosedEventArgs args) =>
            {
                try
                {
                    UpdateStimList();
                    labelNameProject.Text = Data.project.name;
                }
                catch
                {

                }
            };

            FormListOfProjects.Show();
        }

        private void UpdateStimList()
        {
            stimBoxList.Items.Clear();

            try
            {
                merge_list = projects.StimulantsLoad(Data.project.id); //лист связей (проект + стимулятор + настройки)
                if (merge_list.Count > 0)
                {
                    foreach (MergeControl record in merge_list)
                    {
                        stimBoxList.Items.Add(record.name);
                    }
                    stimBoxList.SelectedIndex = 0;
                }
            }
            catch
            {

            }
        }

        private void btnOpenAnalytics_Click(object sender, EventArgs e)
        {
            if (Data.project.id == 0)
                MessageBox.Show("Создайте или выберите проект!", "Eye Stream Capture");
            else
            {
                FormAnalytics formAnalytics = new FormAnalytics(this.tbProjectDirectory, this.labelNameProject);
                formAnalytics.ShowDialog();
            }
        }
      
        private void tbProjectDirectory_TextChanged(object sender, EventArgs e)
        {
            //DirectoryInfo dir = new DirectoryInfo(tbProjectDirectory.Text);
            //FileInfo[] files = dir.GetFiles("*.avi");
            //foreach (FileInfo f in files)
            //{
            //    lbListVideo.Items.Add(f.ToString().Replace(".avi", ""));
            //}
        }

        private void tbProjectDirectory_DoubleClick(object sender, EventArgs e)
        {
            //Process Proc = new Process();
            //Proc.StartInfo.FileName = tbProjectDirectory.Text;

            //Proc.Start();
            //Proc.Close();
        }

        private void lbListVideo_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //try
            //{
            //    Process load = new Process();
            //    load.StartInfo.FileName = Path.Combine(tbProjectDirectory.Text, lbListVideo.SelectedItem.ToString() + ".avi");
            //    load.Start();
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
        }

        private void lbListVideo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Data.respondent = respondents.GetInfoRespondentsItem(exam_list[lbListVideo.SelectedIndex].respondent_id);

                tbDescription.Text = exam_list[lbListVideo.SelectedIndex].description;
                respondentBox.Text = Data.respondent.name;
                respondentLabel.Text = Data.respondent.name;
                dateLabel.Text = exam_list[lbListVideo.SelectedIndex].datetime;

                Data.examination = exam_list[lbListVideo.SelectedIndex];
                dataSetButton.Enabled = result.CheckTraining(Data.examination.id);

                switch (examinations.CheckState(Data.examination.id))
                {
                    case "start":
                        loadStimulants.Enabled = true;
                        btnRecStop.Enabled = false;
                        statelabel.Text = "Создано";
                        break;
                    case "load":
                        btnRecStop.Enabled = false;
                        loadStimulants.Enabled = true;
                        statelabel.Text = "Обучение пройдено";
                        break;
                    case "end":
                        testButton.Enabled = false;
                        dataSetButton.Enabled = false;
                        btnRecStop.Enabled = false;
                        statelabel.Text = "Завершено";
                        break;
                }

                if (Data.merge.stimulant_id == 4)
                    btnRecStop.Enabled = true;
            }
            catch
            {
                tbDescription.Text = "Описание не найдено";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //string path = Path.Combine(Directory.GetCurrentDirectory(), "project");
            //if (!Directory.Exists(path))
            //    Directory.CreateDirectory(path);

            //устанавливаем директорию по умолчанию
            //Configuration currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            //currentConfig.AppSettings.Settings["Default_Project_Directory"].Value = Path.Combine(Directory.GetCurrentDirectory(), "project");
            //currentConfig.Save(ConfigurationSaveMode.Modified);
            //ConfigurationManager.RefreshSection("appSettings");

            //открываем проект по умолчанию
            //List<Projects> list = new List<Projects>();
            //list = project.GetList();
            //Data.project = list[0];

            //try
            //{
            //    UpdateStimList();
            //    labelNameProject.Text = Data.project.name;
            //}
            //catch
            //{

            //}

            Data.eeg_mode = ConfigurationManager.AppSettings["EEG_Mode"];
        }

        private void btnRecStop_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2 && this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                btnRecStop.PerformClick();
            }
        }

        private void recoveryProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void целеваяАудиторияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAudience FormAudience = new FormAudience();
            FormAudience.Show();
        }

        private void респондентыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormRespondents FormRespondents = new FormRespondents();
            FormRespondents.Show();
        }

        private void продуктыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Data.mode = "view";
            FormProducts FormProducts = new FormProducts();
            FormProducts.Show();
        }

        private void стимуляторыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Data.mode = "view";
            FormStimulants FormStimulants = new FormStimulants();
            FormStimulants.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if(Data.project.id != 0)
                {
                    Data.mode = "select";

                    FormStimulants FormStimulantsSelect = new FormStimulants();
                    FormStimulantsSelect.Owner = this;
                    FormStimulantsSelect.FormClosed += (object s, FormClosedEventArgs args) =>
                    {
                        if (!Data.cancel)
                        {
                            projects.AddNewMerge(Data.project.id, Data.stimulants.id, Data.stimulants.name);
                            UpdateStimList();
                            CallFormSettings(Data.stimulants.id);
                        }
                        else
                            Data.cancel = false;
                    };

                    FormStimulantsSelect.Show();
                }
            }
            catch
            {
                MessageBox.Show("Создайте или выберите проект!", "Eye Stream Capture");
            }
        }

        private void CallFormSettings(int stim_id)
        {
            Data.mode = "create";
            
            switch (stim_id)
            {
                case 1:
                    FormStimNamingSettings FormNaming = new FormStimNamingSettings();
                    FormNaming.Owner = this;
                    FormNaming.FormClosed += (object s, FormClosedEventArgs args) =>
                    {
                        Directory.CreateDirectory(Path.Combine(Data.project.path, Data.merge.name));
                        fileSystem.CopyFileExamination(Data.merge.path);
                        UpdateStimList();
                    };
                    FormNaming.Show();
                    break;
                case 2:
                    FormStimPictureSettings FormPicture = new FormStimPictureSettings();
                    FormPicture.Owner = this;
                    FormPicture.FormClosed += (object s, FormClosedEventArgs args) =>
                    {
                        Directory.CreateDirectory(Path.Combine(Data.project.path, Data.merge.name));
                        fileSystem.CopyDirectory(Data.merge.path);
                        UpdateStimList();
                    };
                    FormPicture.Show();
                    break;
            }
        }

        private void button2_Click(object sender, EventArgs e) //remove
        {
            if (stimBoxList.SelectedItems.Count > 0)
            {
                projects.RemoveMerge(merge_list[stimBoxList.SelectedIndex].id);
                UpdateStimList();
            }
            else
                MessageBox.Show("Не выбран стимулятор!", "Eye Stream Capture");
        }

        private void button3_Click(object sender, EventArgs e) //setting options
        {
            if (stimBoxList.SelectedItems.Count > 0)
            {
                Data.merge = merge_list[stimBoxList.SelectedIndex];
                Data.mode = "settings";

                switch (merge_list[stimBoxList.SelectedIndex].stimulant_id)
                {
                    case 1:
                        FormStimNamingSettings formStimNamingSettings = new FormStimNamingSettings();
                        formStimNamingSettings.Owner = this;
                        formStimNamingSettings.FormClosed += (object s, FormClosedEventArgs args) =>
                        {
                            UpdateStimList();
                        };
                        formStimNamingSettings.Show();
                        break;
                    case 2:
                        FormStimPictureSettings formStimPictureSettings = new FormStimPictureSettings();
                        formStimPictureSettings.Owner = this;
                        formStimPictureSettings.FormClosed += (object s, FormClosedEventArgs args) =>
                        {
                            UpdateStimList();
                        };
                        formStimPictureSettings.Show();
                        break;
                    case 4:
                        FormStimPictureSettings formStimExpSettings = new FormStimPictureSettings();
                        formStimExpSettings.Owner = this;
                        formStimExpSettings.FormClosed += (object s, FormClosedEventArgs args) =>
                        {
                            UpdateStimList();
                        };
                        formStimExpSettings.Show();
                        break;
                }
            }
            else
                MessageBox.Show("Не выбран стимулятор!", "Eye Stream Capture");
        }

        private void stimBoxList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Data.merge = merge_list[stimBoxList.SelectedIndex];
                UpdateExaminationList();
                if (Data.merge.stimulant_id == 4)
                    btnRecStop.Enabled = true;
            }
            catch
            {

            }
        }

        private void UpdateExaminationList()
        {
            lbListVideo.Items.Clear();
            exam_list = examinations.GetListExaminationsItems(merge_list[stimBoxList.SelectedIndex].id);

            if (exam_list.Count > 0)
            {
                foreach (Examination record in exam_list)
                {
                    string exam_name = String.Concat(record.name, " ", record.description);
                    if (exam_name.Length > 41)
                        exam_name = exam_name.Substring(0, 41) + "...";
                    lbListVideo.Items.Add(exam_name);
                }

                lbListVideo.SelectedIndex = lbListVideo.Items.Count - 1;
            }
            else
                dataSetButton.Enabled = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            { 
                if (stimBoxList.SelectedItems.Count > 0 & Data.project.id != 0)
                {
                    Data.merge = merge_list[stimBoxList.SelectedIndex];

                    FormRecordDialog formRecordDialog = new FormRecordDialog(this.tbProjectDirectory, this.recorder, this.btnRecStop, this);

                    formRecordDialog.Owner = this;
                    formRecordDialog.FormClosed += (object s, FormClosedEventArgs args) =>
                    {
                        UpdateExaminationList();
                    };

                    formRecordDialog.Show();
                }
                else
                    MessageBox.Show("Выберите стимулятор из списка или добавьте новый!", "Eye Stream Capture");
            }
            catch
            {
                MessageBox.Show("Создайте или выберите проект!", "Eye Stream Capture");
            }
        }

        private void loadStimulants_Click(object sender, EventArgs e)
        {
            if (respondentLabel.Text == "Респондент не выбран")
                MessageBox.Show("Сначала выберите респондента!", "Eye Stream Capture");
            else
            {
                if(Data.merge.stimulant_id == 2)
                {
                    FormLoadStimulants formLoadStimulants = new FormLoadStimulants();
                    formLoadStimulants.Owner = this;
                    formLoadStimulants.FormClosed += (object s, FormClosedEventArgs args) =>
                    {
                        UpdateExaminationList();
                    };

                    formLoadStimulants.Show();
                }
                else
                {
                    MessageBox.Show("Выберите или создайте стимулятор изображений!", "Eye Stream Capture");
                }
            }
        }

        private async void dataSetButton_Click(object sender, EventArgs e)
        {
            if (Data.project.id != 0)
            {
                if (stimBoxList.SelectedItems.Count > 0)
                {
                    if (true)
                    //if (result.CheckTrainingPreset(Data.examination.id))
                    {
                        examinations.EditState(Data.examination.id, "load");
                        Data.edu_code = 1;

                        if (Data.eeg_mode != "muse2")
                        {
                            FormPleaseWait formPleaseWait = new FormPleaseWait();
                            formPleaseWait.Show();
                            Thread.CurrentThread.IsBackground = true;
                            if (recorder.ThinkGearStart())
                            {
                                Thread.Sleep(50); // <-- зачем это здесь?
                                Form1.isRecording = IsRecording.Yes;
                                formPleaseWait.Close();
                                StartRecord();
                            }
                        }
                        else
                        {
                            FormPleaseWait formPleaseWait = new FormPleaseWait();
                            formPleaseWait.Show();
                            dataSetButton.Enabled = false;
                            try
                            {
                                await museBluetooth.StartFeed();
                                formPleaseWait.Close();
                                StartRecord();
                            }
                            catch
                            {
                                StartRecord();
                            }
                        }
                    }
                    else
                        MessageBox.Show("Сначала загрузите обучающие стимуляторы!", "Eye Stream Capture");
                }
                else
                    MessageBox.Show("Выберите стимулятор из списка или добавьте новый!", "Eye Stream Capture");
            }
            else
                MessageBox.Show("Создайте или выберите проект!", "Eye Stream Capture");

            //Data.edu_code = 1;
            //StartRecord();
        }

        private void StartRecord()
        {
            try
            {
                switch (Data.merge.stimulant_id)
                {
                    case 1:
                        NamingForm namingForm = new NamingForm();
                        //this.Owner = namingForm;
                        namingForm.Show();
                        break;
                    case 2:
                        PictureForm pictureForm = new PictureForm();
                        pictureForm.Owner = this;
                        pictureForm.FormClosed += (object s, FormClosedEventArgs args) =>
                        {
                            Cursor.Show();
                            this.WindowState = FormWindowState.Normal;
                            FormPleaseWait formPleaseWait = new FormPleaseWait();
                            formPleaseWait.Show();
                            StopRecording();
                            formPleaseWait.Close();
                        };
                        pictureForm.Show();
                        break;
                    case 4:
                        ExperimentalStimulatorForm expForm = new ExperimentalStimulatorForm(11, Data.merge.pause_time, Data.merge.text_time,
                            Data.respondent.name, Data.merge.path);
                        expForm.Owner = this;
                        expForm.FormClosed += (object s, FormClosedEventArgs args) =>
                        {
                            Cursor.Show();
                            this.WindowState = FormWindowState.Normal;
                            FormPleaseWait formPleaseWait = new FormPleaseWait();
                            formPleaseWait.Show();
                            StopRecording();
                            formPleaseWait.Close();
                        };
                        expForm.Show();
                        break;
                }
                recorder.StartRecording();
            }
            catch
            {

            }
        }

        private void settingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormSetting FormSetting = new FormSetting(recorder);
            FormSetting.Show();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void testButton_Click(object sender, EventArgs e)
        {
            examinations.EditState(Data.examination.id, "start");
            Data.examination.state = "start";
            Data.edu_code = 1;
            StartRecord();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.O)
            {
                FormListOfProjects FormListOfProjects = new FormListOfProjects(this.tbProjectDirectory);
                FormListOfProjects.Owner = this;
                FormListOfProjects.FormClosed += (object s, FormClosedEventArgs args) =>
                {
                    try
                    {
                        UpdateStimList();
                        labelNameProject.Text = Data.project.name;
                    }
                    catch
                    {

                    }
                };

                FormListOfProjects.Show();
                e.SuppressKeyPress = true;
            }
        }

        private void cSVВекторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VectorForm vectorForm = new VectorForm();
            vectorForm.Show();
        }

        private void removeExamFiles(string dirPath)
        {
            DirectoryInfo dir = new DirectoryInfo(dirPath);
            foreach (var item in dir.GetFiles())
            {
                if (item.Name.IndexOf("_raw.csv") > 0)
                {
                    File.Delete(item.FullName);
                }
            }
        }

        private void resetBtn_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Вы уверены что хотите обнулить результаты исследования?", "Eye Stream Capture", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (res == DialogResult.Yes)
            {
                DialogResult res2 = MessageBox.Show("Удалить CSV-фалы?", "Eye Stream Capture",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
                if (res2 == DialogResult.Yes)
                {
                    result.Reset(Data.examination.id);
                    removeExamFiles(Path.Combine(Data.project.path, Data.merge.name, Data.examination.name, "training"));
                    MessageBox.Show("Результаты исследования успешно сброшены, а файлы исследования удалены!", "Eye Stream Capture");
                }
                if (res2 == DialogResult.No)
                {
                    result.Reset(Data.examination.id);
                    MessageBox.Show("Результаты исследования успешно сброшены!", "Eye Stream Capture");
                }
                if (res2 == DialogResult.Cancel)
                {
                    MessageBox.Show("Обнуление результатов исследования отменено!", "Eye Stream Capture");
                }
            }
        }

        private void калибровкаПриборовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormDeviceCalibration formDeviceCalibration = new FormDeviceCalibration();
            formDeviceCalibration.Show();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
    }
}