﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using CsvHelper;
using Timer = System.Timers.Timer;
using AForge.Video;
using AForge.Video.VFW;
using AForge.Video.DirectShow;

namespace screencapture.Forms
{
    public partial class PictureForm : Form
    {
        //информация о настройках
        PointDataSQLite PointDataSQLite = new PointDataSQLite();
        MergeControl project = new MergeControl();
        Result education = new Result();
        Emotions emotions = new Emotions();
        Examination examination = new Examination();
        FileSystem fileSystem = new FileSystem();

        //текст интерфейса
        private List<Label> headerList = new List<Label>();
        private List<Label> helpList = new List<Label>();

        //текст о текущем стимуле (картинке)
        private List<Emotions> emotions_names = new List<Emotions>();
        private List<string> assoc_names = new List<string>();

        //картинки
        private Image img;
        private List<string> pictureList = new List<string>();
        private int countPic = 0;
        private int interval = 1;
        private int startTime = 10;

        //количество в пачке показа
        private int partCount = 10;

        //записывается, какая пауза (до картинки, или после)
        private string pause_mode;
        
        const int pauseKoef = 3; //коэфицент паузы (пауза после картинки длинне в pauseKoef раз, чем пауза до картинки) 

        //переменные для организации потока данных
        private Rectangle bounds;
        private Size screenSize;
        //для записи веб-камеры
        private AVIWriter videoWebWriter;
        private VideoCaptureDevice device;

        //ОПИСАНИЕ ФЛАГОВ:
        //N - никакой, установлен по умолчанию (None)
        //S - исследование по объекту прошло успешно (Success)
        //R - возникла ошибка по картинке или не совпадение по эмоции (Reload)

        public PictureForm()
        {
            InitializeComponent();
        }

        private void PictureExclusionForm_Load(object sender, EventArgs e) //действия при загрузке формы
        {            
            if (Data.edu_code == 1) //узнает в каком режиме запущен стимулятор (обучение или обычное исследование)
            {
                project = project.GetInfoAboutStim(Data.merge.id); //получает информацию о текущем стимуляторе
                Data.education_list = education.GetListExaminationsItems(Data.examination.id, 1); //загружает информацию об объектах исследованияё

                float tempPart = project.part;
                tempPart = tempPart / 100;
                //partCount = (int) Math.Ceiling(Data.education_list.Count * tempPart);

                //перемешиваем список эмоций
                Random RND = new Random();
                for (int i = 0; i < Data.education_list.Count; i++)
                {
                    var tmp = Data.education_list[0];
                    Data.education_list.RemoveAt(0);
                    Data.education_list.Insert(RND.Next(Data.education_list.Count), tmp);
                }

                foreach (var record in Data.education_list)
                {
                    if (record.flag == "R")
                    {
                        MessageBox.Show("Найдены не корректные картинки или несоотвествия! Внесите изменения и повторите попытку!", "Eye Stream Capture");
                        this.Close();
                        break;
                    }
                }

                Data.exam_state = "next"; //устанавливает статус просмотра (пауза, не пауза и тд)
                this.WindowState = FormWindowState.Maximized; //делает окно во весь экран
                
                pictureBox1.Size = new Size(Convert.ToInt32(this.ClientSize.Width * 0.8), Convert.ToInt32(this.ClientSize.Height * 0.8)); //пресет размера и положения картинки (бокса)
                pictureBox1.Location = new Point(Convert.ToInt32((this.ClientSize.Width - pictureBox1.Size.Width) / 2), Convert.ToInt32((this.ClientSize.Height - pictureBox1.Size.Height) / 2));

                AssocLabel.Size = new Size(this.ClientSize.Width, Convert.ToInt32(this.ClientSize.Height * 0.8)); //пресет размера и положения текста
                AssocLabel.Location = new Point(0, Convert.ToInt32((this.ClientSize.Height - pictureBox1.Size.Height) / 2));

                foreach (Result record in Data.education_list) //обрабатывает каждый объект, заносит о нем инфомрацию в память
                {
                    FileInfo fi = new FileInfo(Path.Combine(Data.project.path, record.stimulant));
                    Data.training_folder = fi.DirectoryName;

                    emotions_names.Add(emotions.GetEmotionsInfo(record.emotion_id));
                    assoc_names.Add(record.stim_name);
                    pictureList.Add(record.stimulant);
                }

                if (pictureList.Count < 0) //если элементов исследования не найдено, то закрое форму
                {
                    MessageBox.Show("Нет подходящих объектов для обучения!", "Eye Stream Capture");
                    this.Close();
                }                    
            }
            else
            {

            }

            ShowRebuildTask(project.task, "Начать"); //покажет панель, с кнопкой продолжить для старта исследования
        }
        
        private void PictureForm_KeyDown(object sender, KeyEventArgs e) //сочетание клавиш на форме
        {
            if (!descPanel.Visible) //проверяет, актуально ли сейчас сочетание клавиш
            {
                if (e.KeyCode == Keys.Escape) //Esc ставит паузу
                {
                    SetPause();
                    e.SuppressKeyPress = true;
                }

                if (e.KeyCode == Keys.Space) //Esc или пробел ставят паузу
                {
                    if (Data.examination.state == "start")
                        ShowPicture();
                    else
                        SetPause();
                    e.SuppressKeyPress = true;
                }
                
                if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Right) // Стрелка вправо/стрелка вверх листают вперед (шаг 1)
                {
                    ShowPicture();
                    e.SuppressKeyPress = true;
                }

                if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Left) // Стрелка влево/стрелка вниз листают назад (шаг 1)
                {
                    if (countPic - 2 < 0)
                        countPic = 0;
                    else
                        countPic = countPic - 2;

                    ShowPicture();
                    e.SuppressKeyPress = true;
                }

                if (e.KeyCode == Keys.PageDown) // Page Up листает вперед (шаг 10)
                {
                    if (countPic + 9 < pictureList.Count)
                        countPic = countPic + 9;
                    else
                        countPic = pictureList.Count - 1;

                    ShowPicture();
                    e.SuppressKeyPress = true;
                }

                if (e.KeyCode == Keys.PageUp) // Page Down листает назад (шаг 10)
                {
                    if (countPic - 11 > 0)
                        countPic = countPic - 11;
                    else
                        countPic = 0;

                    ShowPicture();
                    e.SuppressKeyPress = true;
                }

                if (e.KeyCode == Keys.Home) //возвращает к первой картинке
                {
                    countPic = 0;
                    ShowPicture();
                    e.SuppressKeyPress = true;
                }

                if (e.KeyCode == Keys.End) //переносит к последней картинке
                {
                    countPic = pictureList.Count - 1;
                    ShowPicture();
                    e.SuppressKeyPress = true;
                }
            }
        }

        //private void SetPause() //метод устанавливает паузу
        //{
        //    if (Data.examination.state == "start") //пауза в режиме тестового прогона
        //    {
        //        PicturePaused picturePaused = new PicturePaused(emotions_names[countPic-1].name, assoc_names[countPic-1], countPic-1);
        //        picturePaused.Owner = this;
        //        picturePaused.FormClosed += (object s, FormClosedEventArgs args) =>
        //        {
        //            if (Data.exam_state != "next")
        //            {
        //                if (!formClosed)
        //                {
        //                    Cursor.Hide();
        //                    this.formClosed = true;
        //                    this.Close();
        //                }
        //            }
        //        };

        //        picturePaused.Show();
        //    }
        //    else //пауза в боевом режиме...
        //    {
        //        Data.id_edu = 0;
        //        timer1.Stop();
        //        textTimer.Stop();
        //        pauseTimer.Stop();

        //        PointDataSQLite pointDataSQLite = new PointDataSQLite();

        //        Data.paused = true;

        //        PicturePaused picturePaused = new PicturePaused(emotions_names[countPic].name, assoc_names[countPic], countPic);
        //        picturePaused.Owner = this;
        //        picturePaused.FormClosed += (object s, FormClosedEventArgs args) =>
        //        {
        //            if (Data.exam_state == "next")
        //            {
        //                Data.paused = false;
        //                loglabel.Text = Data.id_edu.ToString()  + " countPic: " + countPic;
        //                Data.id_edu = Data.education_list[countPic].id;

        //                textTimer.Start();
        //                Cursor.Hide();
        //                Thread.Sleep(100);
        //            }
        //            else
        //            {
        //                if (!formClosed)
        //                {
        //                    this.formClosed = true;
        //                    this.Close();
        //                }
        //            }
        //        };

        //        picturePaused.Show();
        //    }
        //}

        private void CreateHeadlineLabels() //выводит хедеры на форму
        {
            for (int i = 0; i < 1; i++)
            {
                CreateHeader(32, i);
            }
        }

        private void CreateHeader(int textSize, int position) //создает хедеры
        {
            header.BringToFront();
            header.Text = emotions_names[0].name + " - " + assoc_names[0];
            header.Font = new Font("Arial", textSize);
            header.Width = this.ClientSize.Width;
            header.TextAlign = ContentAlignment.TopCenter;
            header.Visible = true;
            header.Size = new Size(header.Size.Width, Convert.ToInt32(textSize * 1.56));
            header.ForeColor = SystemColors.ControlDarkDark;
            
            header.Location = new Point(0, Convert.ToInt32((this.ClientSize.Height - img.Height) / 2) + img.Height + 10);
        }

        private void ShowRebuildTask(string message, string button) //вызов окна с указанием для юзера
        {
            Cursor.Show();
            //HelpVisible(false);
            nextButton.Visible = true;
            descriptionLabel.Text = message;
            nextButton.Text = button;
            descPanel.Location = new Point((this.ClientSize.Width - descPanel.Size.Width) / 2, (this.ClientSize.Height - descPanel.Size.Height) / 2);
            descPanel.Visible = true;
            nextButton.Focus();
        }

        private void nextButton_Click(object sender, EventArgs e) //кнопка для старта исследования
        {
            Cursor.Hide();

            if (nextButton.Text == "ОК")
            {
                this.Hide();
                this.Close();
            }

            if (nextButton.Text == "Начать")
            {
                //выбор устройства (веб-камеры)
                var filter = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                device = new VideoCaptureDevice(filter[0].MonikerString);

                //Установка разрешения
                device.VideoResolution = device.VideoCapabilities[device.VideoCapabilities.Length - 7];

                //Старт записи
                device.Start();
                
                nextButton.Visible = false;
                descriptionLabel.Font = new Font(FontFamily.GenericSansSerif, 72);
                descriptionLabel.Text = startTime.ToString();
                startTimer.Start();
            }

            if (nextButton.Text == "Продолжить")
            {
                descPanel.Visible = false;
                nextButton.Visible = false;
                partCount = countPic + 10;
                Cursor.Hide();
                
                if (!Data.paused)
                {
                    Thread.Sleep(3000);
                    swapMode();
                }
                else
                {
                    pictureBox1.Visible = false;
                    Data.paused = false;
                    countPic++;
                    Thread.Sleep(3000);
                    ShowPreset(emotions_names[countPic].name + " - " + assoc_names[countPic], project.text_time);
                }
            }
        }

        private void SetPause()
        {
            Data.paused = true;

            //тормозим таймеры
            timer1.Enabled = false;
            pauseTimer.Enabled = false;
            textTimer.Enabled = false;

            //чистим лист для музы
            Data.museList.Clear();
            //чистим лист для нейроская
            Data.mindList.Clear();

            //тормозим запись фрагмена с вебки
            device.NewFrame -= device_NewFrame;
            videoWebWriter.Close();

            //добавляем картинку в конец
            Data.education_list.Add(new Result(Data.education_list[countPic].id, Data.education_list[countPic].flag,
                Data.education_list[countPic].exam_id, Data.education_list[countPic].product_id, Data.education_list[countPic].stim_id,
                     Data.education_list[countPic].emotion_id, Data.education_list[countPic].stim_name, Data.education_list[countPic].stimulant, Data.education_list[countPic].respondent_id, Data.education_list[countPic].edu_code,
                           Data.education_list[countPic].output_data, Data.education_list[countPic].output_video));

            emotions_names.Add(emotions.GetEmotionsInfo(Data.education_list[countPic].emotion_id));
            assoc_names.Add(Data.education_list[countPic].stim_name);
            pictureList.Add(Data.education_list[countPic].stimulant);

            //перемешиваем список эмоций
            Random RND = new Random();
            for (int i = 0; i < Data.education_list.Count; i++)
            {
                var tmp = Data.education_list[0];
                Data.education_list.RemoveAt(0);
                Data.education_list.Insert(RND.Next(Data.education_list.Count), tmp);
            }

            ShowRebuildTask("Пауза. Выполнено " + Math.Floor((double)countPic / Data.education_list.Count * 100).ToString()
                + "%. Чтобы продолжить нажмите любую клавишу.", "Продолжить");
        }


        private void startTimer_Tick(object sender, EventArgs e)
        {
            startTime--;
            descriptionLabel.Text = startTime.ToString();
            if (startTime == 0)
            {
                descriptionLabel.Font = new Font(FontFamily.GenericSansSerif, 32);
                StartExam();
            }
        }
        
        private void StartExam()
        {
            startTimer.Stop();

            Cursor.Hide();
            Thread.Sleep(100);

            if (Data.examination.state == "start")
            {
                ShowPicture();
                CreateHeadlineLabels();
            }
            else
            {
                if (project.interval < 1)
                    this.interval = 1;
                else
                    this.interval = project.interval;
                ShowPreset(emotions_names[countPic].name + " - " + assoc_names[countPic], project.text_time);
            }

            descPanel.Visible = false;
        }

        private void device_NewFrame(object sender, NewFrameEventArgs e)
        {
            Bitmap pbFrame = (Bitmap)e.Frame.Clone();
            try
            {
                videoWebWriter.AddFrame(pbFrame);
            }
            catch
            {

            }
        }

        private void ShowPicture() //показывает юзеру картинку и задает следующую
        {
            if (countPic < pictureList.Count)
            {
                pictureBox1.Visible = true;

                header.Text = emotions_names[countPic].name + " - " + assoc_names[countPic];
                pictureBox1.Image = Image.FromFile(Path.Combine(Data.project.path, pictureList[countPic]));
                img = Image.FromFile(Path.Combine(Data.project.path, pictureList[countPic]));

                header.Location = new Point(0, Convert.ToInt32((this.ClientSize.Height - img.Height) / 2) + img.Height + 10);
                Data.id_edu = Data.education_list[countPic].id;
                Data.emotion = emotions.GetEmotionsInfo(Data.education_list[countPic].emotion_id);

                if (Data.education_list[countPic].flag != "R")
                {
                    education.SetFlag(Data.education_list[countPic].id, "S");
                    Font fnt = new Font(header.Font, FontStyle.Regular);
                    header.Font = fnt;
                } 
                else
                {
                    Font fnt = new Font(header.Font, FontStyle.Strikeout);
                    header.Font = fnt;
                }

                loglabel.Text = "Data.id_edu: " + Data.id_edu.ToString() + " countPic: " + countPic + " Гнев: " + emotions_names[countPic].anger.ToString() + " Страх: " + emotions_names[countPic].fear.ToString() +
                    " Печаль: " + emotions_names[countPic].sadness.ToString() + " Радость: " + emotions_names[countPic].happiness.ToString() + " poor_signal: " + Data.poor_signal;
                countPic++;

                //делаем скриншот
                SaveScreen();

                if (Data.examination.state != "start")
                {
                    timer1.Interval = this.interval * 1000;
                    timer1.Start();
                }
            }
            else
            {
                if (Data.examination.state == "start")
                {
                    bool badpic = false;

                    foreach (var record in Data.education_list)
                    {
                        if (record.flag == "R")
                        {
                            badpic = true;
                            break;
                        }
                    }

                    //Cursor.Show();

                    if (badpic)
                    {
                        DialogResult stopExamination = MessageBox.Show("Найдены не корректные картинки или несоотвествия! Завершить исследование?", "Eye Stream Capture", MessageBoxButtons.OKCancel);
                        if (stopExamination == DialogResult.OK)
                        {
                            MessageBox.Show("Замените картинки и повторите исследование!", "Eye Stream Capture");
                            this.Close();
                        }
                    }
                    else
                    {
                        DialogResult stopExamination = MessageBox.Show("Проверка закочена. Обновить статус исследования?", "Eye Stream Capture", MessageBoxButtons.YesNoCancel);
                        if (stopExamination == DialogResult.Yes)
                        {
                            examination.EditState(Data.examination.id, "load");
                            this.Close();
                        }
                        if (stopExamination == DialogResult.No)
                        {
                            this.Close();
                        }
                    }
                }
                else
                    this.Close();
            }
        }

        private void timer1_Tick(object sender, EventArgs e) //меняет картинку, задает следующую и если больше нет - завершает исследование
        {
            SetWait(project.pause_time);
            pause_mode = "text";
            timer1.Stop();
        }

        private void ShowPreset(string text, int sec) //показывает текст по центру экрана с заданными параметрами (текст, время показа)
        {
            AssocLabel.Visible = true;
            AssocLabel.Text = text;
            Data.id_edu = 0;
            loglabel.Text = Data.id_edu.ToString() + " countPic: " + countPic;

            if (sec == 0)
                sec = 1;
            textTimer.Interval = sec * 1000;
            textTimer.Start();
        }

        private void textTimer_Tick(object sender, EventArgs e) //завершает показ текста 
        {
            AssocLabel.Visible = false;

            SetWait(project.pause_time);
            pause_mode = "pic";

            textTimer.Stop();
        }

        private void SetWait(int sec) //ставит паузу между текстом и картинкой на установленное время
        {
            pictureBox1.Visible = false;
            AssocLabel.Visible = false;

            int id_edu_backup = Data.id_edu;
            Data.id_edu = 0;

            loglabel.Text = Data.id_edu.ToString() + " countPic: " + countPic;

            if (sec == 0)
                sec = 1;

            if (pause_mode == "pic")
            {
                //тормозим запись фрагмена с вебки
                device.NewFrame -= device_NewFrame;
                videoWebWriter.Close();

                pauseTimer.Interval = sec * 1000 * pauseKoef; //в режиме "пауза после картинки" - время паузы длиннее на заднных коэф (см. начало)
                //записываем данные с шапки в csv
                if (Data.eeg_mode == "muse2")
                    WriteMuse(id_edu_backup);
                else
                    WriteCSV(id_edu_backup);
            }
            else
                pauseTimer.Interval = sec * 1000;

            pauseTimer.Start();
        }

        private void WriteMuse(int id_edu)
        {
            if (Data.museList.Count > 300)
            {
                string csvpath = Path.Combine(Data.training_folder, Data.respondent.name.Replace(" ", "_") + "_" + emotions_names[countPic - 1].name + "_training_muse.csv");

                using (var writer = new StreamWriter(csvpath))
                using (var csv = new CsvWriter(writer))
                {
                    csv.WriteRecords(Data.museList);
                }

                education.SaveTrainingResult(id_edu, Data.respondent.name.Replace(" ", "_") + "_" + emotions_names[countPic - 1].name + "_training_muse.csv");
            }
            else
            {
                Data.education_list.Add(new Result(Data.education_list[countPic - 1].id, Data.education_list[countPic - 1].flag,
                    Data.education_list[countPic - 1].exam_id, Data.education_list[countPic - 1].product_id, Data.education_list[countPic - 1].stim_id,
                         Data.education_list[countPic - 1].emotion_id, Data.education_list[countPic - 1].stim_name, Data.education_list[countPic - 1].stimulant, Data.education_list[countPic - 1].respondent_id, Data.education_list[countPic - 1].edu_code,
                               Data.education_list[countPic - 1].output_data, Data.education_list[countPic - 1].output_video));

                emotions_names.Add(emotions.GetEmotionsInfo(Data.education_list[countPic - 1].emotion_id));
                assoc_names.Add(Data.education_list[countPic - 1].stim_name);
                pictureList.Add(Data.education_list[countPic - 1].stimulant);

                //перемешиваем список эмоций
                Random RND = new Random();
                for (int i = 0; i < Data.education_list.Count; i++)
                {
                    var tmp = Data.education_list[0];
                    Data.education_list.RemoveAt(0);
                    Data.education_list.Insert(RND.Next(Data.education_list.Count), tmp);
                }
            }
            
            Data.museList.Clear();
        }

        private void WriteCSV(int id_edu)
        {
            bool bad = false;

            if(Data.mindList.Count > 2400)
            {
                foreach (var item in Data.mindList)
                {
                    if (item.poor_signal != 0)
                    {
                        bad = true;
                        break;
                    }
                }
            }
            else
            {
                bad = true;
            }
            
            if(!bad)
            {
                string csvpath = Path.Combine(Data.training_folder, Data.respondent.name.Replace(" ", "_") + "_" + emotions_names[countPic - 1].name + "_training_raw.csv");

                using (var writer = new StreamWriter(csvpath))
                using (var csv = new CsvWriter(writer))
                {
                    csv.WriteRecords(Data.mindList);
                }

                education.SaveTrainingResult(id_edu, Data.respondent.name.Replace(" ", "_") + "_" + emotions_names[countPic - 1].name + "_training_raw.csv");
            }
            else
            {
                Data.education_list.Add(new Result(Data.education_list[countPic - 1].id, Data.education_list[countPic - 1].flag, 
                    Data.education_list[countPic - 1].exam_id, Data.education_list[countPic - 1].product_id, Data.education_list[countPic - 1].stim_id,
                         Data.education_list[countPic - 1].emotion_id, Data.education_list[countPic - 1].stim_name, Data.education_list[countPic - 1].stimulant, Data.education_list[countPic - 1].respondent_id, Data.education_list[countPic - 1].edu_code,
                               Data.education_list[countPic - 1].output_data, Data.education_list[countPic - 1].output_video));

                emotions_names.Add(emotions.GetEmotionsInfo(Data.education_list[countPic - 1].emotion_id));
                assoc_names.Add(Data.education_list[countPic - 1].stim_name);
                pictureList.Add(Data.education_list[countPic - 1].stimulant);
            }

            Data.mindList.Clear();
        }

        private void pauseTimer_Tick(object sender, EventArgs e) //снимает паузу, показывает после этого текст или картинку
        {
            pauseTimer.Stop();

            if (countPic >= partCount & pause_mode != "pic") // TODO: должно быть 10, а ваще переделать эту схему нужно!!!
            {
                ShowRebuildTask("Пауза. Выполнено " + Math.Floor(((double) countPic / Data.education_list.Count * 100)).ToString() + "%. Чтобы продолжить нажмите любую клавишу.", "Продолжить");
            }
            else
            {
                swapMode();
            }   
        }

        private void swapMode()
        {
            if (pause_mode == "pic")
            {
                //открывает поток для записи вебки
                try
                {
                    videoWebWriter = new AVIWriter();
                    videoWebWriter.FrameRate = 15;
                    videoWebWriter.Open(Path.Combine(Data.training_folder, Data.respondent.name.Replace(" ", "_") + "_" + emotions_names[countPic].name + "-web.avi"),
                        1280, 960); //настроечки под кабинку
                    device.NewFrame += device_NewFrame;
                }
                catch
                {

                }

                ShowPicture();
            }
            else
                if (countPic < pictureList.Count)
                    ShowPreset(emotions_names[countPic].name + " - " + assoc_names[countPic], project.text_time);
                else
                    ShowRebuildTask("Исследование завершено. Позовите ассистента!", "Выход");
        }

        private void SaveScreen()
        {
            Thread myThread = new Thread(new ThreadStart(ScrSave));
            myThread.Start(); // запускаем поток
        }

        void ScrSave()
        {
            Thread.Sleep(300);
            bounds = Screen.PrimaryScreen.Bounds;
            screenSize = new Size(bounds.Width, bounds.Height);
            try
            {

                Bitmap frameImage = new Bitmap(screenSize.Width, screenSize.Height);

                using (Graphics g = Graphics.FromImage(frameImage))
                {
                    g.CopyFromScreen(0, 0, 0, 0, screenSize, CopyPixelOperation.SourceCopy);
                }

                frameImage.Save(Path.Combine(Data.training_folder, Data.respondent.name.Replace(" ", "_") + "_" + emotions_names[countPic-1].name + ".bmp"));
            }
            catch
            {

            }
        }

        private void PictureForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            device.Stop();
        }
    }
}