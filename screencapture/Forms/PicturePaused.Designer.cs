﻿namespace screencapture.Forms
{
    partial class PicturePaused
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PicturePaused));
            this.cancelButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.correctingLabel = new System.Windows.Forms.Button();
            this.emotion = new System.Windows.Forms.Label();
            this.assoc = new System.Windows.Forms.Label();
            this.znak = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(12, 82);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(118, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Прекратить сессию";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(310, 82);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(83, 23);
            this.nextButton.TabIndex = 0;
            this.nextButton.Text = "Продолжить";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // correctingLabel
            // 
            this.correctingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.correctingLabel.Location = new System.Drawing.Point(136, 82);
            this.correctingLabel.Name = "correctingLabel";
            this.correctingLabel.Size = new System.Drawing.Size(168, 23);
            this.correctingLabel.TabIndex = 1;
            this.correctingLabel.Text = "Не правильная картинка!";
            this.correctingLabel.UseVisualStyleBackColor = true;
            this.correctingLabel.Click += new System.EventHandler(this.correctingLabel_Click);
            // 
            // emotion
            // 
            this.emotion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.emotion.Location = new System.Drawing.Point(12, 9);
            this.emotion.Name = "emotion";
            this.emotion.Size = new System.Drawing.Size(174, 70);
            this.emotion.TabIndex = 3;
            this.emotion.Text = "EmotionRatio";
            this.emotion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // assoc
            // 
            this.assoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.assoc.Location = new System.Drawing.Point(218, 9);
            this.assoc.Name = "assoc";
            this.assoc.Size = new System.Drawing.Size(175, 70);
            this.assoc.TabIndex = 4;
            this.assoc.Text = "EmotionRatio";
            this.assoc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // znak
            // 
            this.znak.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.znak.Location = new System.Drawing.Point(191, 1);
            this.znak.Name = "znak";
            this.znak.Size = new System.Drawing.Size(26, 57);
            this.znak.TabIndex = 5;
            this.znak.Text = "=";
            this.znak.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PicturePaused
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 117);
            this.Controls.Add(this.correctingLabel);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.znak);
            this.Controls.Add(this.assoc);
            this.Controls.Add(this.emotion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PicturePaused";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Запись приостановлена";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PicturePaused_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button correctingLabel;
        private System.Windows.Forms.Label emotion;
        private System.Windows.Forms.Label assoc;
        private System.Windows.Forms.Label znak;
    }
}