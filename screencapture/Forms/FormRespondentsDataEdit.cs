﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class FormRespondentsDataEdit : Form
    {
        TargetAudience targetAudience = new TargetAudience();
        Respondents respondents = new Respondents();
        private List<TargetAudience> targetList;
        private Dictionary<int, int> targetListIdValues = new Dictionary<int, int>();

        //сохраняет изначальные параметры для диалога при закрытия формы по Esc
        string name;
        string sort;
        int gender;
        string age;
        int target;
        string description;

        public FormRespondentsDataEdit()
        {
            InitializeComponent();
        }

        private void FormRespondentsDataEdit_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
            TargetBoxLoad();

            if (Data.mode == "edit")
            {
                textBox1.Text = Data.respondent.name;
                textBox2.Text = Data.respondent.age.ToString();
                textBox3.Text = Data.respondent.sort.ToString();
                comboBox1.SelectedIndex = comboBox1.Items.IndexOf(Data.respondent.gender);
                comboBox2.SelectedIndex = targetListIdValues[Data.respondent.audince_id];
                richTextBox1.Text = Data.respondent.description;
                this.Text = "Изменение записи";
            }
            else
                this.Text = "Добавление записи";

            if (Data.mode == "edit")
            {
                name = textBox1.Text;
                sort = textBox3.Text;
                gender = comboBox1.SelectedIndex;
                age = textBox2.Text;
                target = comboBox2.SelectedIndex;
                description = richTextBox1.Text;
            }
        }

        private void TargetBoxLoad()
        {
            comboBox2.Items.Clear();
            targetListIdValues.Clear();

            targetList = targetAudience.GetListTargetAudinceItems("ASC");
            foreach (TargetAudience record in targetList)
            {
                comboBox2.Items.Add(record.name);
                targetListIdValues.Add(record.id, comboBox2.Items.Count - 1);
            }

            try
            {
                comboBox2.SelectedIndex = 0;
            }
            catch { }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((textBox1.TextLength > 0) & (textBox3.TextLength > 0))
            {
                if (Data.mode == "add")
                {
                    respondents.AddRespondentsItem(Int32.Parse(textBox3.Text), comboBox1.SelectedItem.ToString(), Int32.Parse(textBox2.Text),
                        targetListIdValues.FirstOrDefault(x => x.Value == comboBox2.SelectedIndex).Key, textBox1.Text, richTextBox1.Text);
                }
                else
                {
                    if (Data.mode == "edit")
                    {
                        respondents.UpdateRespondentsItem(Data.respondent.id, Int32.Parse(textBox3.Text), comboBox1.SelectedItem.ToString(), Int32.Parse(textBox2.Text),
                            targetListIdValues.FirstOrDefault(x => x.Value == comboBox2.SelectedIndex).Key, textBox1.Text, richTextBox1.Text);
                    }
                }
            }
            else
            {
                MessageBox.Show("Не введено имя или сортировка!");
            }
            this.Close();
        }

        private void FormRespondentsDataEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) //выход с формы с вопросом о сохранении если найдены изменения
            {
                if (name != textBox1.Text || sort != textBox3.Text || gender != comboBox1.SelectedIndex || age != textBox2.Text || target != comboBox2.SelectedIndex || description != richTextBox1.Text)
                {
                    DialogResult dialogResult = MessageBox.Show("Сохранить изменения?", "Eye Stream Capture", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        button1.PerformClick();
                    }
                    else
                        this.Close();
                }
                else
                    this.Close();
            }
        }
    }
}
