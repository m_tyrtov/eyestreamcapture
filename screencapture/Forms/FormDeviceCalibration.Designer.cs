﻿namespace screencapture.Forms
{
    partial class FormDeviceCalibration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDeviceCalibration));
            this.videoBox = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbDevice = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.gazeYlabel = new System.Windows.Forms.Label();
            this.gazeXlabel = new System.Windows.Forms.Label();
            this.connectButton = new System.Windows.Forms.Button();
            this.connectBox = new System.Windows.Forms.PictureBox();
            this.meditationVal = new System.Windows.Forms.Label();
            this.meditationLabel = new System.Windows.Forms.Label();
            this.attentionVal = new System.Windows.Forms.Label();
            this.attentionLabel = new System.Windows.Forms.Label();
            this.infolabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.videoBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.connectBox)).BeginInit();
            this.SuspendLayout();
            // 
            // videoBox
            // 
            this.videoBox.Location = new System.Drawing.Point(7, 19);
            this.videoBox.Name = "videoBox";
            this.videoBox.Size = new System.Drawing.Size(640, 480);
            this.videoBox.TabIndex = 0;
            this.videoBox.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.videoBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(653, 506);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Веб-камера";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(676, 381);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Веб-камера:";
            // 
            // cbDevice
            // 
            this.cbDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDevice.FormattingEnabled = true;
            this.cbDevice.Location = new System.Drawing.Point(752, 375);
            this.cbDevice.Name = "cbDevice";
            this.cbDevice.Size = new System.Drawing.Size(229, 21);
            this.cbDevice.TabIndex = 1;
            this.cbDevice.SelectedIndexChanged += new System.EventHandler(this.cbDevice_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.infolabel);
            this.groupBox2.Controls.Add(this.meditationVal);
            this.groupBox2.Controls.Add(this.meditationLabel);
            this.groupBox2.Controls.Add(this.attentionVal);
            this.groupBox2.Controls.Add(this.attentionLabel);
            this.groupBox2.Controls.Add(this.connectBox);
            this.groupBox2.Controls.Add(this.connectButton);
            this.groupBox2.Location = new System.Drawing.Point(671, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(310, 273);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Устройство ЭЭГ";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.gazeYlabel);
            this.groupBox3.Controls.Add(this.gazeXlabel);
            this.groupBox3.Location = new System.Drawing.Point(671, 291);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(310, 78);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Eye-трекер";
            // 
            // gazeYlabel
            // 
            this.gazeYlabel.AutoSize = true;
            this.gazeYlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gazeYlabel.Location = new System.Drawing.Point(6, 47);
            this.gazeYlabel.Name = "gazeYlabel";
            this.gazeYlabel.Size = new System.Drawing.Size(20, 24);
            this.gazeYlabel.TabIndex = 1;
            this.gazeYlabel.Text = "0";
            // 
            // gazeXlabel
            // 
            this.gazeXlabel.AutoSize = true;
            this.gazeXlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gazeXlabel.Location = new System.Drawing.Point(6, 23);
            this.gazeXlabel.Name = "gazeXlabel";
            this.gazeXlabel.Size = new System.Drawing.Size(20, 24);
            this.gazeXlabel.TabIndex = 0;
            this.gazeXlabel.Text = "0";
            // 
            // connectButton
            // 
            this.connectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.connectButton.Location = new System.Drawing.Point(81, 105);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(146, 23);
            this.connectButton.TabIndex = 7;
            this.connectButton.Text = "Протестировать";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // connectBox
            // 
            this.connectBox.Image = global::screencapture.Properties.Resources.nosignal;
            this.connectBox.Location = new System.Drawing.Point(122, 19);
            this.connectBox.Name = "connectBox";
            this.connectBox.Size = new System.Drawing.Size(43, 40);
            this.connectBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.connectBox.TabIndex = 21;
            this.connectBox.TabStop = false;
            // 
            // meditationVal
            // 
            this.meditationVal.AutoSize = true;
            this.meditationVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.meditationVal.ForeColor = System.Drawing.Color.DodgerBlue;
            this.meditationVal.Location = new System.Drawing.Point(91, 43);
            this.meditationVal.Name = "meditationVal";
            this.meditationVal.Size = new System.Drawing.Size(25, 15);
            this.meditationVal.TabIndex = 25;
            this.meditationVal.Text = "0%";
            // 
            // meditationLabel
            // 
            this.meditationLabel.AutoSize = true;
            this.meditationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.meditationLabel.Location = new System.Drawing.Point(7, 43);
            this.meditationLabel.Name = "meditationLabel";
            this.meditationLabel.Size = new System.Drawing.Size(77, 15);
            this.meditationLabel.TabIndex = 23;
            this.meditationLabel.Text = "Медитация:";
            // 
            // attentionVal
            // 
            this.attentionVal.AutoSize = true;
            this.attentionVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.attentionVal.ForeColor = System.Drawing.Color.Red;
            this.attentionVal.Location = new System.Drawing.Point(91, 19);
            this.attentionVal.Name = "attentionVal";
            this.attentionVal.Size = new System.Drawing.Size(25, 15);
            this.attentionVal.TabIndex = 24;
            this.attentionVal.Text = "0%";
            // 
            // attentionLabel
            // 
            this.attentionLabel.AutoSize = true;
            this.attentionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.attentionLabel.Location = new System.Drawing.Point(7, 19);
            this.attentionLabel.Name = "attentionLabel";
            this.attentionLabel.Size = new System.Drawing.Size(69, 15);
            this.attentionLabel.TabIndex = 22;
            this.attentionLabel.Text = "Внимание:";
            // 
            // infolabel
            // 
            this.infolabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.infolabel.Location = new System.Drawing.Point(63, 131);
            this.infolabel.Name = "infolabel";
            this.infolabel.Size = new System.Drawing.Size(182, 27);
            this.infolabel.TabIndex = 26;
            this.infolabel.Text = "Пожалуйста, подождите";
            this.infolabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.infolabel.Visible = false;
            // 
            // FormDeviceCalibration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(993, 531);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.cbDevice);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormDeviceCalibration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Калибровка устройств";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormDeviceCalibration_FormClosing);
            this.Load += new System.EventHandler(this.FormDeviceCalibration_Load);
            ((System.ComponentModel.ISupportInitialize)(this.videoBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.connectBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox videoBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbDevice;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label gazeYlabel;
        private System.Windows.Forms.Label gazeXlabel;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.PictureBox connectBox;
        private System.Windows.Forms.Label meditationVal;
        private System.Windows.Forms.Label meditationLabel;
        private System.Windows.Forms.Label attentionVal;
        private System.Windows.Forms.Label attentionLabel;
        private System.Windows.Forms.Label infolabel;
    }
}