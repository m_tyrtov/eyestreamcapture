﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace screencapture.Forms
{
    public partial class PicturePaused : Form
    {
        Result result = new Result();

        private string stateOK = " = ";
        private string stateNOT = " ≠ ";
        private int countPic;

        private bool state = true;

        public PicturePaused(string emotion, string assoc, int countPic)
        {
            InitializeComponent();
            this.emotion.Text = emotion;
            this.assoc.Text = assoc;
            znak.Text = stateOK;
            this.countPic = countPic;

            this.KeyDown += new KeyEventHandler(PicturePaused_KeyDown);
            Cursor.Show();
            Thread.Sleep(100);
        }

        private void correctingLabel_Click(object sender, EventArgs e)
        {
            if (state)
            {
                state = false;
                znak.Text = stateNOT;
                correctingLabel.Text = "Правильная картинка!";
                result.SetFlag(Data.education_list[countPic].id, "R");
                Data.education_list[countPic].flag = "R";
            }
            else
            {
                state = true;
                znak.Text = stateOK;
                correctingLabel.Text = "Не правильная картинка!";
                result.SetFlag(Data.education_list[countPic].id, "N");
                Data.education_list[countPic].flag = "N";


            }
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            Data.exam_state = "next";
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult stopExamination = MessageBox.Show("Вы уверены что хотите остановить исследование?", "Eye Stream Capture", MessageBoxButtons.YesNo);
            if (stopExamination == DialogResult.Yes)
            {
                if (Data.examination.state != "start")
                {
                    DialogResult saveResult = MessageBox.Show("Записать полученные данные?", "Eye Stream Capture", MessageBoxButtons.YesNo);
                    if (saveResult == DialogResult.Yes)
                        Data.exam_state = "save";
                    else
                        Data.exam_state = "dontsave";
                }
                else
                    Data.exam_state = "dontsave";

                this.Close();
            }
        }

        private void PicturePaused_KeyDown(object sender, KeyEventArgs e) //сочетание клавиш на этой форме
        {
            if (e.KeyCode == Keys.Escape) //отлавливает Esc для закрытия формы паузы
            {
                nextButton.PerformClick();
            }
        }
    }
}
