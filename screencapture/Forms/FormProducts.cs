﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class FormProducts : Form
    {
        Products products = new Products();
        List<Products> list = new List<Products>();

        public FormProducts()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            CreateEditeForm("add");
        }

        private void CreateEditeForm(string mode)
        {
            Data.mode = mode;
            FormProductsDataEdit FormDataEdit = new FormProductsDataEdit();

            FormDataEdit.Owner = this;
            FormDataEdit.FormClosed += (object s, FormClosedEventArgs args) =>
            {
                DBListWithdraw();
            };

            FormDataEdit.Show();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (CheckCurrentRecord())
                CreateEditeForm("edit");
        }

        private void DBListWithdraw()
        {
            tableView.Items.Clear();

            list = products.GetListProductsItems("ASC");

            foreach (Products record in list)
            {
                ListViewItem item = new ListViewItem(record.sort.ToString());

                item.SubItems.Add(record.name);
                item.SubItems.Add(record.description);

                tableView.Items.AddRange(new ListViewItem[] { item });
            }
        }

        private void FormProducts_Load(object sender, EventArgs e)
        {
            this.KeyPreview = true;
            this.KeyDown += new KeyEventHandler(FormProducts_KeyDown);

            toolTip1.SetToolTip(this.addButton, "CTRL + N");
            toolTip1.SetToolTip(this.editButton, "CTRL + E");
            toolTip1.SetToolTip(this.delButton, "CTRL + Del");

            DBListWithdraw();

            if (Data.mode == "select")
            {
                selectButton.Visible = true;
                cancelButton.Visible = true;
                this.Size = new Size(419, 420);
            }

            if (tableView.Items.Count > 0)
            {
                tableView.Items[0].Selected = true;
                tableView.Select();
                tableView.HideSelection = false;
            }
        }

        private void FormProducts_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.N) //new record
            {
                addButton.PerformClick();
                e.SuppressKeyPress = true;
            }
            if (e.Control && e.KeyCode == Keys.Delete) //delete record(s)
            {
                delButton.PerformClick();
                e.SuppressKeyPress = true;
            }
            if (e.Control && e.KeyCode == Keys.E) //edit record
            {
                editButton.PerformClick();
                e.SuppressKeyPress = true;
            }
            if (e.KeyCode == Keys.Escape) //closed this form
            {
                Data.cancel = true;
                this.Close();
            }
        }

        private void tableView_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckCurrentRecord();
        }

        private void delButton_Click(object sender, EventArgs e)
        {
            if (CheckCurrentRecord())
            {
                DialogResult dialogResult = MessageBox.Show("Удалить отмеченные записи?", "Eye Stream Capture", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK)
                {
                    foreach (ListViewItem item in this.tableView.SelectedItems)
                        products.DeleteProductsItem(list[item.Index].id);
                    DBListWithdraw();
                }
            }
        }

        private bool CheckCurrentRecord()
        {
            if (this.tableView.SelectedItems.Count > 0)
            {
                foreach (ListViewItem item in this.tableView.SelectedItems)
                    Data.product = list[item.Index];
                return true;
            }
            else
                return false;
        }

        private void tableView_DoubleClick(object sender, EventArgs e)
        {
            if (CheckCurrentRecord())
            {
                editButton.PerformClick();
            }
        }

        private void selectButton_Click(object sender, EventArgs e)
        {
            if (CheckCurrentRecord())
            {
                this.Close();
            }
        }

        private void tableView_Click(object sender, EventArgs e)
        {
            CheckCurrentRecord();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Data.cancel = true;
            this.Close(); 
        }

        private void FormProducts_FormClosing(object sender, FormClosingEventArgs e)
        {
            Data.cancel = true;
        }
    }
}