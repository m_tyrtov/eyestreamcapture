﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using System.Xml.Serialization;

namespace screencapture.Forms
{
    public partial class FormCreateProject : Form
    {
        private TextBox tbForm1_projectDirectory;
        private Label tbForm1_labelNameProject;
        private Projects project = new Projects();
        public static readonly string researchObjectCatalog = "ResearchObject"; // название папки для объекта исследования
        
        public FormCreateProject(TextBox projectDirectory, Label projectName)
        {
            this.tbForm1_projectDirectory = projectDirectory;
            this.tbForm1_labelNameProject = projectName;
            InitializeComponent();
        }
   
        private void btnCreateProject_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(tbProjectName.Text) & !String.IsNullOrWhiteSpace(productNameBox.Text))
            {
                string pathToProject = Path.Combine(ConfigurationManager.AppSettings["Default_Project_Directory"], tbProjectName.Text);

                Directory.CreateDirectory(pathToProject);
                Data.project = project.Create(Data.product.id, tbProjectName.Text, richDescProject.Text, pathToProject);

                tbForm1_projectDirectory.Text = pathToProject;
                tbForm1_labelNameProject.Text = tbProjectName.Text;

                this.Close();
            }
            else
            {
                MessageBox.Show("Вы не ввели название проекта или не выбрали продукт!", "Eye Stream Capture");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SelectButton_Click(object sender, EventArgs e)
        {
            Data.mode = "select";

            FormProducts FormProductSelect = new FormProducts();
            FormProductSelect.Owner = this;
            FormProductSelect.FormClosed += (object s, FormClosedEventArgs args) =>
            {
                if (!Data.cancel)
                    productNameBox.Text = Data.product.name; //баг при закрытии формы!
                else
                {
                    Data.cancel = false;
                    productNameBox.Clear();
                } 
            };

            FormProductSelect.Show();
        }

        private void FormCreateProject_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                DialogResult dialogResult = MessageBox.Show("Не создавать новый проект?", "Eye Stream Capture", MessageBoxButtons.OKCancel);
                if (dialogResult == DialogResult.OK)
                {
                    this.Close();
                }
            }
        }
    }
}