﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class FormProductsDataEdit : Form
    {
        Products products = new Products();
        private Dictionary<int, int> targetListIdValues = new Dictionary<int, int>();

        string name;
        string sort;
        string description;

        public FormProductsDataEdit()
        {
            InitializeComponent();
        }

        private void FormProductsDataEdit_Load(object sender, EventArgs e)
        {
            if (Data.mode == "edit")
            {
                textBox1.Text = Data.product.name;
                textBox2.Text = Data.product.sort.ToString();
                richTextBox1.Text = Data.product.description;

                this.Text = "Изменение записи";
            }
            else
                this.Text = "Добавление записи";

            name = textBox1.Text;
            sort = textBox2.Text;
            description = richTextBox1.Text;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Data.mode == "add")
            {
                products.AddProductsItem(Int32.Parse(textBox2.Text), textBox1.Text, richTextBox1.Text);
            }
            else
            {
                if (Data.mode == "edit")
                {
                    products.UpdateProductsItem(Data.product.id, Int32.Parse(textBox2.Text), textBox1.Text, richTextBox1.Text);
                }
            }

            this.Close();
        }

        private void cancelButton_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if ((textBox1.TextLength > 0) & (textBox2.TextLength > 0))
            {
                if (Data.mode == "add")
                {
                    products.AddProductsItem(Int32.Parse(textBox2.Text), textBox1.Text, richTextBox1.Text);
                }
                else
                {
                    if (Data.mode == "edit")
                    {
                        products.UpdateProductsItem(Data.product.id, Int32.Parse(textBox2.Text), textBox1.Text, richTextBox1.Text);
                    }
                }
            }
            else
            {
                MessageBox.Show("Не введено имя или сортировка!");
            }
            this.Close();
        }

        private void FormProductsDataEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) //выход с формы с вопросом о сохранении если найдены изменения
            {
                if (name != textBox1.Text || sort != textBox2.Text || description != richTextBox1.Text)
                {
                    DialogResult dialogResult = MessageBox.Show("Сохранить изменения?", "Eye Stream Capture", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        button1.PerformClick();
                    }
                    else
                        this.Close();
                }
                else
                    this.Close();
            }
        }
    }
}
