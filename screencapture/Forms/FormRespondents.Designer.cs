﻿namespace screencapture.Forms
{
    partial class FormRespondents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRespondents));
            this.editButton = new System.Windows.Forms.Button();
            this.delButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.tableView = new System.Windows.Forms.ListView();
            this.id_col = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gender_col = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.years_col = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ta_col = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.name_col = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.desc_col = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cancelButton = new System.Windows.Forms.Button();
            this.selectButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // editButton
            // 
            this.editButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.editButton.Location = new System.Drawing.Point(203, 12);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(89, 23);
            this.editButton.TabIndex = 14;
            this.editButton.Text = "Изменить";
            this.editButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // delButton
            // 
            this.delButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.delButton.Location = new System.Drawing.Point(108, 12);
            this.delButton.Name = "delButton";
            this.delButton.Size = new System.Drawing.Size(89, 23);
            this.delButton.TabIndex = 13;
            this.delButton.Text = "Удалить";
            this.delButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.delButton.UseVisualStyleBackColor = true;
            this.delButton.Click += new System.EventHandler(this.delButton_Click);
            // 
            // addButton
            // 
            this.addButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.addButton.Location = new System.Drawing.Point(12, 12);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(90, 23);
            this.addButton.TabIndex = 12;
            this.addButton.Text = "Добавить";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // tableView
            // 
            this.tableView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.id_col,
            this.gender_col,
            this.years_col,
            this.ta_col,
            this.name_col,
            this.desc_col});
            this.tableView.FullRowSelect = true;
            this.tableView.GridLines = true;
            this.tableView.Location = new System.Drawing.Point(13, 41);
            this.tableView.Name = "tableView";
            this.tableView.Size = new System.Drawing.Size(678, 300);
            this.tableView.TabIndex = 11;
            this.tableView.UseCompatibleStateImageBehavior = false;
            this.tableView.View = System.Windows.Forms.View.Details;
            this.tableView.SelectedIndexChanged += new System.EventHandler(this.tableView_SelectedIndexChanged);
            this.tableView.DoubleClick += new System.EventHandler(this.tableView_DoubleClick);
            // 
            // id_col
            // 
            this.id_col.Text = "Сорт.";
            this.id_col.Width = 42;
            // 
            // gender_col
            // 
            this.gender_col.DisplayIndex = 3;
            this.gender_col.Text = "Пол";
            this.gender_col.Width = 67;
            // 
            // years_col
            // 
            this.years_col.DisplayIndex = 4;
            this.years_col.Text = "Возраст";
            // 
            // ta_col
            // 
            this.ta_col.DisplayIndex = 2;
            this.ta_col.Text = "Целевая аудитория";
            this.ta_col.Width = 111;
            // 
            // name_col
            // 
            this.name_col.DisplayIndex = 1;
            this.name_col.Text = "ФИО";
            this.name_col.Width = 141;
            // 
            // desc_col
            // 
            this.desc_col.Text = "Описание";
            this.desc_col.Width = 253;
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cancelButton.Location = new System.Drawing.Point(13, 347);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(89, 23);
            this.cancelButton.TabIndex = 24;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Visible = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // selectButton
            // 
            this.selectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.selectButton.Location = new System.Drawing.Point(602, 347);
            this.selectButton.Name = "selectButton";
            this.selectButton.Size = new System.Drawing.Size(89, 23);
            this.selectButton.TabIndex = 23;
            this.selectButton.Text = "Выбрать";
            this.selectButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.selectButton.UseVisualStyleBackColor = true;
            this.selectButton.Visible = false;
            this.selectButton.Click += new System.EventHandler(this.selectButton_Click);
            // 
            // FormRespondents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 352);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.selectButton);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.delButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.tableView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormRespondents";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Респонденты";
            this.Load += new System.EventHandler(this.FormRespondents_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormRespondents_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button delButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.ListView tableView;
        private System.Windows.Forms.ColumnHeader id_col;
        private System.Windows.Forms.ColumnHeader years_col;
        private System.Windows.Forms.ColumnHeader gender_col;
        private System.Windows.Forms.ColumnHeader name_col;
        private System.Windows.Forms.ColumnHeader desc_col;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ColumnHeader ta_col;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button selectButton;
    }
}