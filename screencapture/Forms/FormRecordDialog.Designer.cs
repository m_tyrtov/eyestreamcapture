﻿namespace screencapture
{
    partial class FormRecordDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRecordDialog));
            this.btnStartRec = new System.Windows.Forms.Button();
            this.btnCancelRD = new System.Windows.Forms.Button();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbDescription = new System.Windows.Forms.RichTextBox();
            this.statusLabel = new System.Windows.Forms.Label();
            this.loadBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.respondentBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.respondentBrowse = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.loadBox)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStartRec
            // 
            this.btnStartRec.Location = new System.Drawing.Point(140, 210);
            this.btnStartRec.Name = "btnStartRec";
            this.btnStartRec.Size = new System.Drawing.Size(83, 23);
            this.btnStartRec.TabIndex = 0;
            this.btnStartRec.Text = "Создать";
            this.btnStartRec.UseVisualStyleBackColor = true;
            this.btnStartRec.Click += new System.EventHandler(this.btnStartRec_Click);
            // 
            // btnCancelRD
            // 
            this.btnCancelRD.Location = new System.Drawing.Point(229, 210);
            this.btnCancelRD.Name = "btnCancelRD";
            this.btnCancelRD.Size = new System.Drawing.Size(83, 23);
            this.btnCancelRD.TabIndex = 1;
            this.btnCancelRD.Text = "Отмена";
            this.btnCancelRD.UseVisualStyleBackColor = true;
            this.btnCancelRD.Click += new System.EventHandler(this.btnCancelRD_Click);
            // 
            // tbName
            // 
            this.tbName.Enabled = false;
            this.tbName.Location = new System.Drawing.Point(12, 25);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(300, 20);
            this.tbName.TabIndex = 2;
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(12, 108);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(300, 96);
            this.tbDescription.TabIndex = 3;
            this.tbDescription.Text = "";
            // 
            // statusLabel
            // 
            this.statusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.statusLabel.Location = new System.Drawing.Point(12, 255);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(300, 23);
            this.statusLabel.TabIndex = 7;
            this.statusLabel.Text = "Подождите, идет подготовка оборудования...";
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.statusLabel.Visible = false;
            // 
            // loadBox
            // 
            this.loadBox.Image = global::screencapture.Properties.Resources.ajax_loader;
            this.loadBox.Location = new System.Drawing.Point(131, 280);
            this.loadBox.Name = "loadBox";
            this.loadBox.Size = new System.Drawing.Size(43, 11);
            this.loadBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.loadBox.TabIndex = 8;
            this.loadBox.TabStop = false;
            this.loadBox.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Название:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Описание исследования:";
            // 
            // respondentBox
            // 
            this.respondentBox.Enabled = false;
            this.respondentBox.Location = new System.Drawing.Point(12, 66);
            this.respondentBox.Name = "respondentBox";
            this.respondentBox.Size = new System.Drawing.Size(211, 20);
            this.respondentBox.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Респондент:";
            // 
            // respondentBrowse
            // 
            this.respondentBrowse.Location = new System.Drawing.Point(229, 65);
            this.respondentBrowse.Name = "respondentBrowse";
            this.respondentBrowse.Size = new System.Drawing.Size(83, 23);
            this.respondentBrowse.TabIndex = 31;
            this.respondentBrowse.Text = "Выбрать";
            this.respondentBrowse.UseVisualStyleBackColor = true;
            this.respondentBrowse.Click += new System.EventHandler(this.respondentBrowse_Click);
            // 
            // FormRecordDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 243);
            this.Controls.Add(this.respondentBrowse);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.respondentBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.loadBox);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.tbDescription);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.btnCancelRD);
            this.Controls.Add(this.btnStartRec);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormRecordDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Новое исследование";
            this.Load += new System.EventHandler(this.FormRecordDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.loadBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStartRec;
        private System.Windows.Forms.Button btnCancelRD;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.RichTextBox tbDescription;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.PictureBox loadBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox respondentBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button respondentBrowse;
    }
}