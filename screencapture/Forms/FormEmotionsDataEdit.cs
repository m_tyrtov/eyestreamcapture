﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class FormEmotionsDataEdit : Form
    {
        Emotions emotions = new Emotions();

        //сохраняет изначальные параметры для диалога при закрытия формы по Esc
        string name;
        string name_en;
        string sort;

        int anger;
        int fear;
        int sadness;
        int happiness;

        bool goldman;
        bool microsoft;
        bool leontyev;
        bool emodetect;

        public FormEmotionsDataEdit()
        {
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Data.mode == "add")

            {
                emotions.AddEmotionsItem(Int32.Parse(textBox2.Text), textBox1.Text, textBox7.Text, (int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value, (int)numericUpDown4.Value,
                    checkBox1.Checked, checkBox2.Checked, checkBox3.Checked, checkBox4.Checked);
            }
            else
            {
                if (Data.mode == "edit")
                {
                    emotions.UpdateEmotionsItem(Data.emotion.id, Int32.Parse(textBox2.Text), textBox1.Text, textBox7.Text, (int) numericUpDown1.Value, (int)numericUpDown2.Value, (int) numericUpDown3.Value, (int) numericUpDown4.Value,
                        checkBox1.Checked, checkBox2.Checked, checkBox3.Checked, checkBox4.Checked);
                }
            }

            this.Close();
        }

        private void FormEmotionsDataEdit_Load(object sender, EventArgs e)
        {
            if (Data.mode == "edit")
            {
                textBox1.Text = Data.emotion.name;
                textBox2.Text = Data.emotion.sort.ToString();
                textBox7.Text = Data.emotion.name_en;

                numericUpDown1.Value = Data.emotion.anger;
                numericUpDown2.Value = Data.emotion.fear;
                numericUpDown3.Value = Data.emotion.sadness;
                numericUpDown4.Value = Data.emotion.happiness;

                checkBox1.Checked = Data.emotion.goldman;
                checkBox2.Checked = Data.emotion.microsoft;
                checkBox3.Checked = Data.emotion.leontyev;
                checkBox4.Checked = Data.emotion.emodetect;
                
                this.Text = "Изменение записи";
            }
            else
                this.Text = "Добавление записи";

            name = textBox1.Text;
            name_en = textBox7.Text;
            sort = textBox2.Text;

            anger = (int) numericUpDown1.Value;
            fear = (int) numericUpDown2.Value;
            sadness = (int) numericUpDown3.Value;
            happiness = (int) numericUpDown4.Value;

            goldman = checkBox1.Checked;
            microsoft = checkBox2.Checked;
            leontyev = checkBox3.Checked;
            emodetect = checkBox4.Checked;
        }

        private void FormEmotionsDataEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) //выход с формы с вопросом о сохранении если найдены изменения
            {
                if (name != textBox1.Text || name_en != textBox7.Text || sort != textBox2.Text || anger != (int) numericUpDown1.Value || fear != (int) numericUpDown2.Value || 
                        sadness != (int) numericUpDown3.Value || happiness != (int) numericUpDown4.Value || goldman != checkBox1.Checked || microsoft != checkBox2.Checked || 
                            leontyev != checkBox3.Checked || emodetect != checkBox4.Checked)
                {
                    DialogResult dialogResult = MessageBox.Show("Сохранить изменения?", "Eye Stream Capture", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        button1.PerformClick();
                    }
                    else
                        this.Close();
                }
                else
                    this.Close();
            }
        }
    }
}
