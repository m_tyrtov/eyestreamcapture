﻿namespace screencapture.Forms
{
    partial class FormAnalytics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.labelNameProject = new System.Windows.Forms.Label();
            this.labelStateAnalytics = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbListVideo = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelNameResearch = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnGazeMarking = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Выбранный проект:";
            // 
            // labelNameProject
            // 
            this.labelNameProject.AutoSize = true;
            this.labelNameProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.labelNameProject.Location = new System.Drawing.Point(140, 9);
            this.labelNameProject.Name = "labelNameProject";
            this.labelNameProject.Size = new System.Drawing.Size(130, 15);
            this.labelNameProject.TabIndex = 1;
            this.labelNameProject.Text = "Проект не выбран";
            // 
            // labelStateAnalytics
            // 
            this.labelStateAnalytics.AutoSize = true;
            this.labelStateAnalytics.Location = new System.Drawing.Point(13, 293);
            this.labelStateAnalytics.Name = "labelStateAnalytics";
            this.labelStateAnalytics.Size = new System.Drawing.Size(0, 13);
            this.labelStateAnalytics.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label3.Location = new System.Drawing.Point(480, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 15);
            this.label3.TabIndex = 14;
            this.label3.Text = "Список исследований:";
            // 
            // lbListVideo
            // 
            this.lbListVideo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbListVideo.FormattingEnabled = true;
            this.lbListVideo.ItemHeight = 16;
            this.lbListVideo.Location = new System.Drawing.Point(483, 43);
            this.lbListVideo.Name = "lbListVideo";
            this.lbListVideo.Size = new System.Drawing.Size(205, 260);
            this.lbListVideo.TabIndex = 13;
            this.lbListVideo.SelectedIndexChanged += new System.EventHandler(this.lbListVideo_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label4.Location = new System.Drawing.Point(12, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 15);
            this.label4.TabIndex = 15;
            this.label4.Text = "Выбрано исследование:";
            // 
            // labelNameResearch
            // 
            this.labelNameResearch.AutoSize = true;
            this.labelNameResearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.labelNameResearch.Location = new System.Drawing.Point(165, 33);
            this.labelNameResearch.Name = "labelNameResearch";
            this.labelNameResearch.Size = new System.Drawing.Size(162, 15);
            this.labelNameResearch.TabIndex = 16;
            this.labelNameResearch.Text = "Исследование не выбрано";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnGazeMarking);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox2.Location = new System.Drawing.Point(15, 209);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(462, 95);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Наложение маркера взгляда";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label7.Location = new System.Drawing.Point(6, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(450, 19);
            this.label7.TabIndex = 6;
            this.label7.Text = "Создание нового видео с отрисовкой маркера взгляда";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label6.Location = new System.Drawing.Point(6, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(450, 36);
            this.label6.TabIndex = 4;
            this.label6.Text = "Предварительная кластеризация приведет к цветовой дифференциации маркера.\r\n";
            // 
            // btnGazeMarking
            // 
            this.btnGazeMarking.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnGazeMarking.Location = new System.Drawing.Point(372, 64);
            this.btnGazeMarking.Name = "btnGazeMarking";
            this.btnGazeMarking.Size = new System.Drawing.Size(84, 23);
            this.btnGazeMarking.TabIndex = 3;
            this.btnGazeMarking.Text = "Применить";
            this.btnGazeMarking.UseVisualStyleBackColor = true;
            this.btnGazeMarking.Click += new System.EventHandler(this.btnGazeMarking_Click);
            // 
            // FormAnalytics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 316);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.labelNameResearch);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelStateAnalytics);
            this.Controls.Add(this.lbListVideo);
            this.Controls.Add(this.labelNameProject);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "FormAnalytics";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Анализ и обработка исследования";
            this.Load += new System.EventHandler(this.FormAnalytics_Load);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelNameProject;
        private System.Windows.Forms.Label labelStateAnalytics;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lbListVideo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelNameResearch;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnGazeMarking;
    }
}