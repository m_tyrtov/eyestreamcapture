﻿namespace screencapture.Forms
{
    partial class PictureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.descPanel = new System.Windows.Forms.Panel();
            this.nextButton = new System.Windows.Forms.Button();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.textTimer = new System.Windows.Forms.Timer(this.components);
            this.pauseTimer = new System.Windows.Forms.Timer(this.components);
            this.AssocLabel = new System.Windows.Forms.Label();
            this.loglabel = new System.Windows.Forms.Label();
            this.header = new System.Windows.Forms.Label();
            this.startTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.descPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(384, 355);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // descPanel
            // 
            this.descPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.descPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.descPanel.Controls.Add(this.nextButton);
            this.descPanel.Controls.Add(this.descriptionLabel);
            this.descPanel.Location = new System.Drawing.Point(103, 71);
            this.descPanel.Name = "descPanel";
            this.descPanel.Size = new System.Drawing.Size(1000, 400);
            this.descPanel.TabIndex = 3;
            this.descPanel.Visible = false;
            // 
            // nextButton
            // 
            this.nextButton.AutoSize = true;
            this.nextButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nextButton.Location = new System.Drawing.Point(431, 334);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(150, 36);
            this.nextButton.TabIndex = 1;
            this.nextButton.Text = "Продолжить";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.descriptionLabel.ForeColor = System.Drawing.Color.Black;
            this.descriptionLabel.Location = new System.Drawing.Point(74, 45);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(852, 271);
            this.descriptionLabel.TabIndex = 0;
            this.descriptionLabel.Text = "Сейчас вам предстоит выбрать из представленных на экране вариантов наилучший";
            this.descriptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textTimer
            // 
            this.textTimer.Tick += new System.EventHandler(this.textTimer_Tick);
            // 
            // pauseTimer
            // 
            this.pauseTimer.Tick += new System.EventHandler(this.pauseTimer_Tick);
            // 
            // AssocLabel
            // 
            this.AssocLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AssocLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.AssocLabel.Location = new System.Drawing.Point(12, 9);
            this.AssocLabel.Name = "AssocLabel";
            this.AssocLabel.Size = new System.Drawing.Size(102, 58);
            this.AssocLabel.TabIndex = 5;
            this.AssocLabel.Text = "assoc";
            this.AssocLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.AssocLabel.Visible = false;
            // 
            // loglabel
            // 
            this.loglabel.AutoSize = true;
            this.loglabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.loglabel.ForeColor = System.Drawing.Color.White;
            this.loglabel.Location = new System.Drawing.Point(27, 12);
            this.loglabel.Name = "loglabel";
            this.loglabel.Size = new System.Drawing.Size(51, 20);
            this.loglabel.TabIndex = 6;
            this.loglabel.Text = "label1";
            this.loglabel.Visible = false;
            // 
            // header
            // 
            this.header.BackColor = System.Drawing.Color.Transparent;
            this.header.Location = new System.Drawing.Point(49, 42);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(100, 23);
            this.header.TabIndex = 8;
            this.header.Text = "label2";
            // 
            // startTimer
            // 
            this.startTimer.Interval = 1000;
            this.startTimer.Tick += new System.EventHandler(this.startTimer_Tick);
            // 
            // PictureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1207, 542);
            this.Controls.Add(this.header);
            this.Controls.Add(this.loglabel);
            this.Controls.Add(this.AssocLabel);
            this.Controls.Add(this.descPanel);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "PictureForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PictureExclusionForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PictureForm_FormClosing);
            this.Load += new System.EventHandler(this.PictureExclusionForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PictureForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.descPanel.ResumeLayout(false);
            this.descPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel descPanel;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.Timer textTimer;
        private System.Windows.Forms.Timer pauseTimer;
        private System.Windows.Forms.Label AssocLabel;
        private System.Windows.Forms.Label loglabel;
        private System.Windows.Forms.Label header;
        private System.Windows.Forms.Timer startTimer;
    }
}