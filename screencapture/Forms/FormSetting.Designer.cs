﻿namespace screencapture
{
    partial class FormSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSetting));
            this.btnSetDefaultDirectory = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSelectDirectory = new System.Windows.Forms.Button();
            this.tbPathToDefaultDir = new System.Windows.Forms.TextBox();
            this.trackBarSizeMarkerGaze = new System.Windows.Forms.TrackBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelGMdiametr = new System.Windows.Forms.Label();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbMaxTimeGaze = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.connectButton = new System.Windows.Forms.Button();
            this.connectBox = new System.Windows.Forms.PictureBox();
            this.infolabel = new System.Windows.Forms.Label();
            this.meditationVal = new System.Windows.Forms.Label();
            this.searchButton = new System.Windows.Forms.Button();
            this.cboPort = new System.Windows.Forms.ComboBox();
            this.meditationLabel = new System.Windows.Forms.Label();
            this.attentionVal = new System.Windows.Forms.Label();
            this.attentionLabel = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.muse2 = new System.Windows.Forms.RadioButton();
            this.mindwave = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.selectTempButton = new System.Windows.Forms.Button();
            this.textBoxTempDir = new System.Windows.Forms.TextBox();
            this.defTempButton = new System.Windows.Forms.Button();
            this.ssdLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSizeMarkerGaze)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.connectBox)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSetDefaultDirectory
            // 
            this.btnSetDefaultDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnSetDefaultDirectory.Location = new System.Drawing.Point(84, 47);
            this.btnSetDefaultDirectory.Name = "btnSetDefaultDirectory";
            this.btnSetDefaultDirectory.Size = new System.Drawing.Size(102, 23);
            this.btnSetDefaultDirectory.TabIndex = 0;
            this.btnSetDefaultDirectory.Text = "По умолчанию";
            this.btnSetDefaultDirectory.UseVisualStyleBackColor = true;
            this.btnSetDefaultDirectory.Click += new System.EventHandler(this.btnSetDefaultDirectory_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSelectDirectory);
            this.groupBox1.Controls.Add(this.tbPathToDefaultDir);
            this.groupBox1.Controls.Add(this.btnSetDefaultDirectory);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(685, 77);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Директория для проектов:";
            // 
            // btnSelectDirectory
            // 
            this.btnSelectDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnSelectDirectory.Location = new System.Drawing.Point(6, 47);
            this.btnSelectDirectory.Name = "btnSelectDirectory";
            this.btnSelectDirectory.Size = new System.Drawing.Size(72, 23);
            this.btnSelectDirectory.TabIndex = 3;
            this.btnSelectDirectory.Text = "Выбрать";
            this.btnSelectDirectory.UseVisualStyleBackColor = true;
            this.btnSelectDirectory.Click += new System.EventHandler(this.btnSelectDirectory_Click);
            // 
            // tbPathToDefaultDir
            // 
            this.tbPathToDefaultDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.tbPathToDefaultDir.Location = new System.Drawing.Point(7, 20);
            this.tbPathToDefaultDir.Name = "tbPathToDefaultDir";
            this.tbPathToDefaultDir.Size = new System.Drawing.Size(672, 21);
            this.tbPathToDefaultDir.TabIndex = 2;
            // 
            // trackBarSizeMarkerGaze
            // 
            this.trackBarSizeMarkerGaze.Location = new System.Drawing.Point(6, 56);
            this.trackBarSizeMarkerGaze.Maximum = 100;
            this.trackBarSizeMarkerGaze.Minimum = 10;
            this.trackBarSizeMarkerGaze.Name = "trackBarSizeMarkerGaze";
            this.trackBarSizeMarkerGaze.Size = new System.Drawing.Size(186, 45);
            this.trackBarSizeMarkerGaze.TabIndex = 2;
            this.trackBarSizeMarkerGaze.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarSizeMarkerGaze.Value = 10;
            this.trackBarSizeMarkerGaze.Scroll += new System.EventHandler(this.trackBarSizeMarkerGaze_Scroll);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelGMdiametr);
            this.groupBox2.Controls.Add(this.trackBarSizeMarkerGaze);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox2.Location = new System.Drawing.Point(12, 181);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(198, 106);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Маркер взгляда";
            // 
            // labelGMdiametr
            // 
            this.labelGMdiametr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.labelGMdiametr.Location = new System.Drawing.Point(6, 26);
            this.labelGMdiametr.Name = "labelGMdiametr";
            this.labelGMdiametr.Size = new System.Drawing.Size(186, 32);
            this.labelGMdiametr.TabIndex = 3;
            this.labelGMdiametr.Text = "100px";
            this.labelGMdiametr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnApply
            // 
            this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnApply.Location = new System.Drawing.Point(604, 409);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(93, 23);
            this.btnApply.TabIndex = 4;
            this.btnApply.Text = "Сохранить";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnCancel.Location = new System.Drawing.Point(12, 409);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(93, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tbMaxTimeGaze
            // 
            this.tbMaxTimeGaze.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.tbMaxTimeGaze.Location = new System.Drawing.Point(391, 25);
            this.tbMaxTimeGaze.Name = "tbMaxTimeGaze";
            this.tbMaxTimeGaze.Size = new System.Drawing.Size(49, 21);
            this.tbMaxTimeGaze.TabIndex = 7;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.tbMaxTimeGaze);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox3.Location = new System.Drawing.Point(216, 181);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(481, 106);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Максимальное время фиксации взгляда";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label3.Location = new System.Drawing.Point(446, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "Сек.";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(385, 64);
            this.label2.TabIndex = 8;
            this.label2.Text = "Необходимо для цветовой дифференциации маркера взгляда: \r\n\r\n0-20% - Синий, 20-40%" +
    " - Голубой, 40-60% - Зеленый, 60-80% - Желтый, 80-100% - Оранжевый, 100%+ - Крас" +
    "ный";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.connectButton);
            this.groupBox4.Controls.Add(this.connectBox);
            this.groupBox4.Controls.Add(this.infolabel);
            this.groupBox4.Controls.Add(this.meditationVal);
            this.groupBox4.Controls.Add(this.searchButton);
            this.groupBox4.Controls.Add(this.cboPort);
            this.groupBox4.Controls.Add(this.meditationLabel);
            this.groupBox4.Controls.Add(this.attentionVal);
            this.groupBox4.Controls.Add(this.attentionLabel);
            this.groupBox4.Enabled = false;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox4.Location = new System.Drawing.Point(216, 293);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(481, 106);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Neurosky Mindwave";
            // 
            // connectButton
            // 
            this.connectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.connectButton.Location = new System.Drawing.Point(6, 49);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(146, 23);
            this.connectButton.TabIndex = 6;
            this.connectButton.Text = "Протестировать";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // connectBox
            // 
            this.connectBox.Image = global::screencapture.Properties.Resources.nosignal;
            this.connectBox.Location = new System.Drawing.Point(355, 37);
            this.connectBox.Name = "connectBox";
            this.connectBox.Size = new System.Drawing.Size(43, 40);
            this.connectBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.connectBox.TabIndex = 20;
            this.connectBox.TabStop = false;
            // 
            // infolabel
            // 
            this.infolabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.infolabel.Location = new System.Drawing.Point(6, 75);
            this.infolabel.Name = "infolabel";
            this.infolabel.Size = new System.Drawing.Size(471, 27);
            this.infolabel.TabIndex = 5;
            this.infolabel.Text = "Пожалуйста, подождите";
            this.infolabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.infolabel.Visible = false;
            // 
            // meditationVal
            // 
            this.meditationVal.AutoSize = true;
            this.meditationVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.meditationVal.ForeColor = System.Drawing.Color.DodgerBlue;
            this.meditationVal.Location = new System.Drawing.Point(242, 51);
            this.meditationVal.Name = "meditationVal";
            this.meditationVal.Size = new System.Drawing.Size(25, 15);
            this.meditationVal.TabIndex = 19;
            this.meditationVal.Text = "0%";
            // 
            // searchButton
            // 
            this.searchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.searchButton.Location = new System.Drawing.Point(6, 20);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(71, 23);
            this.searchButton.TabIndex = 1;
            this.searchButton.Text = "Найти автоматически";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click_1);
            // 
            // cboPort
            // 
            this.cboPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.cboPort.FormattingEnabled = true;
            this.cboPort.Location = new System.Drawing.Point(83, 20);
            this.cboPort.Name = "cboPort";
            this.cboPort.Size = new System.Drawing.Size(69, 23);
            this.cboPort.TabIndex = 0;
            // 
            // meditationLabel
            // 
            this.meditationLabel.AutoSize = true;
            this.meditationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.meditationLabel.Location = new System.Drawing.Point(158, 51);
            this.meditationLabel.Name = "meditationLabel";
            this.meditationLabel.Size = new System.Drawing.Size(77, 15);
            this.meditationLabel.TabIndex = 16;
            this.meditationLabel.Text = "Медитация:";
            // 
            // attentionVal
            // 
            this.attentionVal.AutoSize = true;
            this.attentionVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.attentionVal.ForeColor = System.Drawing.Color.Red;
            this.attentionVal.Location = new System.Drawing.Point(242, 27);
            this.attentionVal.Name = "attentionVal";
            this.attentionVal.Size = new System.Drawing.Size(25, 15);
            this.attentionVal.TabIndex = 18;
            this.attentionVal.Text = "0%";
            // 
            // attentionLabel
            // 
            this.attentionLabel.AutoSize = true;
            this.attentionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.attentionLabel.Location = new System.Drawing.Point(158, 27);
            this.attentionLabel.Name = "attentionLabel";
            this.attentionLabel.Size = new System.Drawing.Size(69, 15);
            this.attentionLabel.TabIndex = 15;
            this.attentionLabel.Text = "Внимание:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.muse2);
            this.groupBox6.Controls.Add(this.mindwave);
            this.groupBox6.Location = new System.Drawing.Point(12, 293);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(198, 106);
            this.groupBox6.TabIndex = 22;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Выбрать устройство ЭЭГ";
            // 
            // muse2
            // 
            this.muse2.AutoSize = true;
            this.muse2.Location = new System.Drawing.Point(26, 60);
            this.muse2.Name = "muse2";
            this.muse2.Size = new System.Drawing.Size(86, 17);
            this.muse2.TabIndex = 24;
            this.muse2.TabStop = true;
            this.muse2.Text = "Muse MU-02";
            this.muse2.UseVisualStyleBackColor = true;
            this.muse2.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // mindwave
            // 
            this.mindwave.AutoSize = true;
            this.mindwave.Location = new System.Drawing.Point(26, 37);
            this.mindwave.Name = "mindwave";
            this.mindwave.Size = new System.Drawing.Size(122, 17);
            this.mindwave.TabIndex = 23;
            this.mindwave.TabStop = true;
            this.mindwave.Text = "Neurosky Mindwave";
            this.mindwave.UseVisualStyleBackColor = true;
            this.mindwave.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.ssdLabel);
            this.groupBox5.Controls.Add(this.selectTempButton);
            this.groupBox5.Controls.Add(this.textBoxTempDir);
            this.groupBox5.Controls.Add(this.defTempButton);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox5.Location = new System.Drawing.Point(12, 95);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(685, 77);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Директория для временных файлов:";
            // 
            // selectTempButton
            // 
            this.selectTempButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.selectTempButton.Location = new System.Drawing.Point(6, 47);
            this.selectTempButton.Name = "selectTempButton";
            this.selectTempButton.Size = new System.Drawing.Size(72, 23);
            this.selectTempButton.TabIndex = 3;
            this.selectTempButton.Text = "Выбрать";
            this.selectTempButton.UseVisualStyleBackColor = true;
            this.selectTempButton.Click += new System.EventHandler(this.selectTempButton_Click);
            // 
            // textBoxTempDir
            // 
            this.textBoxTempDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.textBoxTempDir.Location = new System.Drawing.Point(7, 20);
            this.textBoxTempDir.Name = "textBoxTempDir";
            this.textBoxTempDir.Size = new System.Drawing.Size(672, 21);
            this.textBoxTempDir.TabIndex = 2;
            // 
            // defTempButton
            // 
            this.defTempButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.defTempButton.Location = new System.Drawing.Point(84, 47);
            this.defTempButton.Name = "defTempButton";
            this.defTempButton.Size = new System.Drawing.Size(102, 23);
            this.defTempButton.TabIndex = 0;
            this.defTempButton.Text = "По умолчанию";
            this.defTempButton.UseVisualStyleBackColor = true;
            this.defTempButton.Click += new System.EventHandler(this.defTempButton_Click);
            // 
            // ssdLabel
            // 
            this.ssdLabel.AutoSize = true;
            this.ssdLabel.Location = new System.Drawing.Point(192, 51);
            this.ssdLabel.Name = "ssdLabel";
            this.ssdLabel.Size = new System.Drawing.Size(375, 15);
            this.ssdLabel.TabIndex = 4;
            this.ssdLabel.Text = "Обратите внимание: папка должна быть расположена на SSD!";
            // 
            // FormSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(710, 443);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FormSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройки";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormSetting_FormClosed);
            this.Load += new System.EventHandler(this.FormSetting_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormSetting_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSizeMarkerGaze)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.connectBox)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSetDefaultDirectory;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbPathToDefaultDir;
        private System.Windows.Forms.Button btnSelectDirectory;
        private System.Windows.Forms.TrackBar trackBarSizeMarkerGaze;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label labelGMdiametr;
        private System.Windows.Forms.TextBox tbMaxTimeGaze;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cboPort;
        private System.Windows.Forms.Label infolabel;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Label meditationVal;
        private System.Windows.Forms.Label attentionVal;
        private System.Windows.Forms.Label meditationLabel;
        private System.Windows.Forms.Label attentionLabel;
        private System.Windows.Forms.PictureBox connectBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton muse2;
        private System.Windows.Forms.RadioButton mindwave;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button selectTempButton;
        private System.Windows.Forms.TextBox textBoxTempDir;
        private System.Windows.Forms.Button defTempButton;
        private System.Windows.Forms.Label ssdLabel;
    }
}