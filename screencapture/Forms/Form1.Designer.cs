﻿namespace screencapture
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnRecStop = new System.Windows.Forms.Button();
            this.tbProjectDirectory = new System.Windows.Forms.TextBox();
            this.labelSelectedProject = new System.Windows.Forms.Label();
            this.lbListVideo = new System.Windows.Forms.ListBox();
            this.labelNameProject = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выбратьПроектToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справочникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.респондентыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.целеваяАудиторияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.продуктыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.стимуляторыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.эмоцииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.анализToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cSVВекторToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.калибровкаПриборовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelDirectoryProject = new System.Windows.Forms.Label();
            this.btnOpenAnalytics = new System.Windows.Forms.Button();
            this.tbDescription = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.stimBoxList = new System.Windows.Forms.ListBox();
            this.addStim = new System.Windows.Forms.Button();
            this.rmButton = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.respondentBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateLabel = new System.Windows.Forms.Label();
            this.dataSetButton = new System.Windows.Forms.Button();
            this.loadStimulants = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.respondentLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.statelabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.testButton = new System.Windows.Forms.Button();
            this.resetBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRecStop
            // 
            this.btnRecStop.Enabled = false;
            this.btnRecStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.btnRecStop.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnRecStop.Location = new System.Drawing.Point(233, 543);
            this.btnRecStop.Margin = new System.Windows.Forms.Padding(2);
            this.btnRecStop.Name = "btnRecStop";
            this.btnRecStop.Size = new System.Drawing.Size(326, 23);
            this.btnRecStop.TabIndex = 4;
            this.btnRecStop.Text = "Начать исследование";
            this.btnRecStop.UseVisualStyleBackColor = true;
            this.btnRecStop.Click += new System.EventHandler(this.btnRecStop_Click);
            this.btnRecStop.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnRecStop_KeyDown);
            // 
            // tbProjectDirectory
            // 
            this.tbProjectDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbProjectDirectory.Location = new System.Drawing.Point(166, 68);
            this.tbProjectDirectory.Name = "tbProjectDirectory";
            this.tbProjectDirectory.ReadOnly = true;
            this.tbProjectDirectory.Size = new System.Drawing.Size(777, 22);
            this.tbProjectDirectory.TabIndex = 2;
            this.tbProjectDirectory.TextChanged += new System.EventHandler(this.tbProjectDirectory_TextChanged);
            this.tbProjectDirectory.DoubleClick += new System.EventHandler(this.tbProjectDirectory_DoubleClick);
            // 
            // labelSelectedProject
            // 
            this.labelSelectedProject.AutoSize = true;
            this.labelSelectedProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.labelSelectedProject.Location = new System.Drawing.Point(11, 28);
            this.labelSelectedProject.Name = "labelSelectedProject";
            this.labelSelectedProject.Size = new System.Drawing.Size(53, 15);
            this.labelSelectedProject.TabIndex = 3;
            this.labelSelectedProject.Text = "Проект:";
            // 
            // lbListVideo
            // 
            this.lbListVideo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lbListVideo.FormattingEnabled = true;
            this.lbListVideo.ItemHeight = 16;
            this.lbListVideo.Location = new System.Drawing.Point(232, 119);
            this.lbListVideo.Name = "lbListVideo";
            this.lbListVideo.Size = new System.Drawing.Size(326, 308);
            this.lbListVideo.TabIndex = 5;
            this.lbListVideo.SelectedIndexChanged += new System.EventHandler(this.lbListVideo_SelectedIndexChanged);
            this.lbListVideo.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbListVideo_MouseDoubleClick);
            // 
            // labelNameProject
            // 
            this.labelNameProject.AutoSize = true;
            this.labelNameProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.labelNameProject.Location = new System.Drawing.Point(163, 28);
            this.labelNameProject.Name = "labelNameProject";
            this.labelNameProject.Size = new System.Drawing.Size(130, 15);
            this.labelNameProject.TabIndex = 6;
            this.labelNameProject.Text = "Проект не выбран";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Menu;
            this.menuStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.справочникиToolStripMenuItem,
            this.settingToolStripMenuItem,
            this.HelpToolStripMenuItem,
            this.анализToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(953, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createProjectToolStripMenuItem,
            this.выбратьПроектToolStripMenuItem,
            this.ExitToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.fileToolStripMenuItem.Text = "Проект";
            // 
            // createProjectToolStripMenuItem
            // 
            this.createProjectToolStripMenuItem.Name = "createProjectToolStripMenuItem";
            this.createProjectToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.createProjectToolStripMenuItem.Text = "Создать проект";
            this.createProjectToolStripMenuItem.Click += new System.EventHandler(this.btnCreateProject_Click);
            // 
            // выбратьПроектToolStripMenuItem
            // 
            this.выбратьПроектToolStripMenuItem.Name = "выбратьПроектToolStripMenuItem";
            this.выбратьПроектToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.выбратьПроектToolStripMenuItem.Text = "Открыть проект";
            this.выбратьПроектToolStripMenuItem.Click += new System.EventHandler(this.btnBrowseProject_Click);
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.ExitToolStripMenuItem.Text = "Выход";
            this.ExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // справочникиToolStripMenuItem
            // 
            this.справочникиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.респондентыToolStripMenuItem,
            this.целеваяАудиторияToolStripMenuItem,
            this.продуктыToolStripMenuItem,
            this.стимуляторыToolStripMenuItem,
            this.эмоцииToolStripMenuItem});
            this.справочникиToolStripMenuItem.Name = "справочникиToolStripMenuItem";
            this.справочникиToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.справочникиToolStripMenuItem.Text = "Справочники";
            // 
            // респондентыToolStripMenuItem
            // 
            this.респондентыToolStripMenuItem.Name = "респондентыToolStripMenuItem";
            this.респондентыToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.респондентыToolStripMenuItem.Text = "Респонденты";
            this.респондентыToolStripMenuItem.Click += new System.EventHandler(this.респондентыToolStripMenuItem_Click);
            // 
            // целеваяАудиторияToolStripMenuItem
            // 
            this.целеваяАудиторияToolStripMenuItem.Name = "целеваяАудиторияToolStripMenuItem";
            this.целеваяАудиторияToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.целеваяАудиторияToolStripMenuItem.Text = "Целевая аудитория";
            this.целеваяАудиторияToolStripMenuItem.Click += new System.EventHandler(this.целеваяАудиторияToolStripMenuItem_Click);
            // 
            // продуктыToolStripMenuItem
            // 
            this.продуктыToolStripMenuItem.Name = "продуктыToolStripMenuItem";
            this.продуктыToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.продуктыToolStripMenuItem.Text = "Продукты";
            this.продуктыToolStripMenuItem.Click += new System.EventHandler(this.продуктыToolStripMenuItem_Click);
            // 
            // стимуляторыToolStripMenuItem
            // 
            this.стимуляторыToolStripMenuItem.Name = "стимуляторыToolStripMenuItem";
            this.стимуляторыToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.стимуляторыToolStripMenuItem.Text = "Стимуляторы";
            this.стимуляторыToolStripMenuItem.Click += new System.EventHandler(this.стимуляторыToolStripMenuItem_Click);
            // 
            // эмоцииToolStripMenuItem
            // 
            this.эмоцииToolStripMenuItem.Name = "эмоцииToolStripMenuItem";
            this.эмоцииToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.эмоцииToolStripMenuItem.Text = "Эмоции";
            // 
            // settingToolStripMenuItem
            // 
            this.settingToolStripMenuItem.Name = "settingToolStripMenuItem";
            this.settingToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.settingToolStripMenuItem.Text = "Настройки";
            this.settingToolStripMenuItem.Click += new System.EventHandler(this.settingToolStripMenuItem_Click);
            // 
            // HelpToolStripMenuItem
            // 
            this.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem";
            this.HelpToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.HelpToolStripMenuItem.Text = "Справка";
            this.HelpToolStripMenuItem.Visible = false;
            // 
            // анализToolStripMenuItem
            // 
            this.анализToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cSVВекторToolStripMenuItem,
            this.калибровкаПриборовToolStripMenuItem});
            this.анализToolStripMenuItem.Name = "анализToolStripMenuItem";
            this.анализToolStripMenuItem.Size = new System.Drawing.Size(145, 20);
            this.анализToolStripMenuItem.Text = "Анализ и корректировка";
            // 
            // cSVВекторToolStripMenuItem
            // 
            this.cSVВекторToolStripMenuItem.Name = "cSVВекторToolStripMenuItem";
            this.cSVВекторToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.cSVВекторToolStripMenuItem.Text = "CSV файлы";
            this.cSVВекторToolStripMenuItem.Click += new System.EventHandler(this.cSVВекторToolStripMenuItem_Click);
            // 
            // калибровкаПриборовToolStripMenuItem
            // 
            this.калибровкаПриборовToolStripMenuItem.Name = "калибровкаПриборовToolStripMenuItem";
            this.калибровкаПриборовToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.калибровкаПриборовToolStripMenuItem.Text = "Калибровка устройств";
            this.калибровкаПриборовToolStripMenuItem.Click += new System.EventHandler(this.калибровкаПриборовToolStripMenuItem_Click);
            // 
            // labelDirectoryProject
            // 
            this.labelDirectoryProject.AutoSize = true;
            this.labelDirectoryProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.labelDirectoryProject.Location = new System.Drawing.Point(11, 71);
            this.labelDirectoryProject.Name = "labelDirectoryProject";
            this.labelDirectoryProject.Size = new System.Drawing.Size(149, 15);
            this.labelDirectoryProject.TabIndex = 8;
            this.labelDirectoryProject.Text = "Путь к файлам проекта:";
            // 
            // btnOpenAnalytics
            // 
            this.btnOpenAnalytics.Enabled = false;
            this.btnOpenAnalytics.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnOpenAnalytics.Location = new System.Drawing.Point(705, 543);
            this.btnOpenAnalytics.Name = "btnOpenAnalytics";
            this.btnOpenAnalytics.Size = new System.Drawing.Size(238, 23);
            this.btnOpenAnalytics.TabIndex = 5;
            this.btnOpenAnalytics.Text = "Анализ и обработка исследования";
            this.btnOpenAnalytics.UseVisualStyleBackColor = true;
            this.btnOpenAnalytics.Click += new System.EventHandler(this.btnOpenAnalytics_Click);
            // 
            // tbDescription
            // 
            this.tbDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbDescription.Location = new System.Drawing.Point(565, 204);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.ReadOnly = true;
            this.tbDescription.Size = new System.Drawing.Size(378, 333);
            this.tbDescription.TabIndex = 11;
            this.tbDescription.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.Location = new System.Drawing.Point(235, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 15);
            this.label1.TabIndex = 12;
            this.label1.Text = "Список исследований в проекте:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label2.Location = new System.Drawing.Point(564, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 15);
            this.label2.TabIndex = 13;
            this.label2.Text = "Описание исследования:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label3.Location = new System.Drawing.Point(15, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(188, 15);
            this.label3.TabIndex = 15;
            this.label3.Text = "Список стимуляторов проекта:";
            // 
            // stimBoxList
            // 
            this.stimBoxList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.stimBoxList.FormattingEnabled = true;
            this.stimBoxList.ItemHeight = 16;
            this.stimBoxList.Location = new System.Drawing.Point(12, 119);
            this.stimBoxList.Name = "stimBoxList";
            this.stimBoxList.Size = new System.Drawing.Size(213, 420);
            this.stimBoxList.TabIndex = 14;
            this.stimBoxList.SelectedIndexChanged += new System.EventHandler(this.stimBoxList_SelectedIndexChanged);
            // 
            // addStim
            // 
            this.addStim.Location = new System.Drawing.Point(10, 543);
            this.addStim.Name = "addStim";
            this.addStim.Size = new System.Drawing.Size(65, 23);
            this.addStim.TabIndex = 16;
            this.addStim.Text = "Добавить";
            this.addStim.UseVisualStyleBackColor = true;
            this.addStim.Click += new System.EventHandler(this.button1_Click);
            // 
            // rmButton
            // 
            this.rmButton.Location = new System.Drawing.Point(81, 543);
            this.rmButton.Name = "rmButton";
            this.rmButton.Size = new System.Drawing.Size(65, 23);
            this.rmButton.TabIndex = 17;
            this.rmButton.Text = "Удалить";
            this.rmButton.UseVisualStyleBackColor = true;
            this.rmButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(152, 543);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(73, 23);
            this.button3.TabIndex = 18;
            this.button3.Text = "Настроить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // respondentBox
            // 
            this.respondentBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.respondentBox.Location = new System.Drawing.Point(564, 119);
            this.respondentBox.Name = "respondentBox";
            this.respondentBox.ReadOnly = true;
            this.respondentBox.Size = new System.Drawing.Size(377, 21);
            this.respondentBox.TabIndex = 19;
            this.respondentBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label4.Location = new System.Drawing.Point(561, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 15);
            this.label4.TabIndex = 20;
            this.label4.Text = "Респондент:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label5.Location = new System.Drawing.Point(564, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 15);
            this.label5.TabIndex = 22;
            this.label5.Text = "Дата исследования:";
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.dateLabel.Location = new System.Drawing.Point(692, 166);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(137, 15);
            this.dateLabel.TabIndex = 23;
            this.dateLabel.Text = "дата исследования";
            // 
            // dataSetButton
            // 
            this.dataSetButton.Enabled = false;
            this.dataSetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.dataSetButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dataSetButton.Location = new System.Drawing.Point(232, 516);
            this.dataSetButton.Margin = new System.Windows.Forms.Padding(2);
            this.dataSetButton.Name = "dataSetButton";
            this.dataSetButton.Size = new System.Drawing.Size(326, 23);
            this.dataSetButton.TabIndex = 3;
            this.dataSetButton.Text = "Получить об. данные";
            this.dataSetButton.UseVisualStyleBackColor = true;
            this.dataSetButton.Click += new System.EventHandler(this.dataSetButton_Click);
            // 
            // loadStimulants
            // 
            this.loadStimulants.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.loadStimulants.ForeColor = System.Drawing.SystemColors.ControlText;
            this.loadStimulants.Location = new System.Drawing.Point(232, 462);
            this.loadStimulants.Margin = new System.Windows.Forms.Padding(2);
            this.loadStimulants.Name = "loadStimulants";
            this.loadStimulants.Size = new System.Drawing.Size(326, 23);
            this.loadStimulants.TabIndex = 1;
            this.loadStimulants.Text = "Загрузить об. стимулы";
            this.loadStimulants.UseVisualStyleBackColor = true;
            this.loadStimulants.Click += new System.EventHandler(this.loadStimulants_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.button4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button4.Location = new System.Drawing.Point(232, 435);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(326, 23);
            this.button4.TabIndex = 0;
            this.button4.Text = "Создать исследование";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // respondentLabel
            // 
            this.respondentLabel.AutoSize = true;
            this.respondentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.respondentLabel.Location = new System.Drawing.Point(163, 50);
            this.respondentLabel.Name = "respondentLabel";
            this.respondentLabel.Size = new System.Drawing.Size(161, 15);
            this.respondentLabel.TabIndex = 29;
            this.respondentLabel.Text = "Респондент не выбран";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label7.Location = new System.Drawing.Point(11, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 15);
            this.label7.TabIndex = 28;
            this.label7.Text = "Респондент:";
            // 
            // statelabel
            // 
            this.statelabel.AutoSize = true;
            this.statelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.statelabel.Location = new System.Drawing.Point(702, 147);
            this.statelabel.Name = "statelabel";
            this.statelabel.Size = new System.Drawing.Size(147, 15);
            this.statelabel.TabIndex = 31;
            this.statelabel.Text = "Обучение завершено";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label8.Location = new System.Drawing.Point(564, 147);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(135, 15);
            this.label8.TabIndex = 30;
            this.label8.Text = "Статус исследования:";
            // 
            // testButton
            // 
            this.testButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.testButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.testButton.Location = new System.Drawing.Point(232, 489);
            this.testButton.Margin = new System.Windows.Forms.Padding(2);
            this.testButton.Name = "testButton";
            this.testButton.Size = new System.Drawing.Size(326, 23);
            this.testButton.TabIndex = 2;
            this.testButton.Text = "Проверить об. стимулы";
            this.testButton.UseVisualStyleBackColor = true;
            this.testButton.Click += new System.EventHandler(this.testButton_Click);
            // 
            // resetBtn
            // 
            this.resetBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resetBtn.ForeColor = System.Drawing.Color.DarkRed;
            this.resetBtn.Location = new System.Drawing.Point(564, 543);
            this.resetBtn.Name = "resetBtn";
            this.resetBtn.Size = new System.Drawing.Size(135, 23);
            this.resetBtn.TabIndex = 32;
            this.resetBtn.Text = "Сбросить";
            this.resetBtn.UseVisualStyleBackColor = true;
            this.resetBtn.Click += new System.EventHandler(this.resetBtn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(866, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 33;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(953, 577);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.resetBtn);
            this.Controls.Add(this.testButton);
            this.Controls.Add(this.statelabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.respondentLabel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.loadStimulants);
            this.Controls.Add(this.dataSetButton);
            this.Controls.Add(this.dateLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.respondentBox);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.rmButton);
            this.Controls.Add(this.addStim);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.stimBoxList);
            this.Controls.Add(this.btnOpenAnalytics);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbDescription);
            this.Controls.Add(this.labelDirectoryProject);
            this.Controls.Add(this.labelNameProject);
            this.Controls.Add(this.lbListVideo);
            this.Controls.Add(this.labelSelectedProject);
            this.Controls.Add(this.tbProjectDirectory);
            this.Controls.Add(this.btnRecStop);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(400, 200);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Eye Stream Capture";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRecStop;
        private System.Windows.Forms.TextBox tbProjectDirectory;
        private System.Windows.Forms.Label labelSelectedProject;
        private System.Windows.Forms.ListBox lbListVideo;
        private System.Windows.Forms.Label labelNameProject;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItem;
        private System.Windows.Forms.Label labelDirectoryProject;
        private System.Windows.Forms.RichTextBox tbDescription;
        private System.Windows.Forms.ToolStripMenuItem HelpToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnOpenAnalytics;
        private System.Windows.Forms.ToolStripMenuItem справочникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem целеваяАудиторияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem респондентыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem продуктыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выбратьПроектToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem стимуляторыToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox stimBoxList;
        private System.Windows.Forms.Button addStim;
        private System.Windows.Forms.Button rmButton;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox respondentBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.Button dataSetButton;
        private System.Windows.Forms.Button loadStimulants;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label respondentLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label statelabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStripMenuItem эмоцииToolStripMenuItem;
        private System.Windows.Forms.Button testButton;
        private System.Windows.Forms.ToolStripMenuItem анализToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cSVВекторToolStripMenuItem;
        private System.Windows.Forms.Button resetBtn;
        private System.Windows.Forms.ToolStripMenuItem калибровкаПриборовToolStripMenuItem;
        private System.Windows.Forms.Button button1;
    }
}

