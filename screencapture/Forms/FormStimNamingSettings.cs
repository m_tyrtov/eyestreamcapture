﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class FormStimNamingSettings : Form
    {
        MergeControl projects = new MergeControl();
        FileSystem fileSystem = new FileSystem();

        public FormStimNamingSettings()
        {
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Data.mode == "create")
            {
                DialogResult result = MessageBox.Show("Вы уверены? Изменить название стимулятора в дальнейшем будет не возможно!", "Eye Stream Capture",
                MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (result == DialogResult.Yes)
                {
                    Data.merge.name = nameBox.Text;
                    projects.UpdateMerge(Data.merge.id, nameBox.Text, headerBox.Text, helpBox.Text, taskRichBox.Text, pathBox.Text);
                    this.Close();
                }
            }
            else
            {
                projects.UpdateMerge(Data.merge.id, nameBox.Text, headerBox.Text, helpBox.Text, taskRichBox.Text, pathBox.Text);
                this.Close();
            }
        }

        private void FormStimSettings_Load(object sender, EventArgs e)
        {
            if (Data.mode == "create")
                nameBox.Enabled = true;

            nameBox.Text = Data.merge.name;
            headerBox.Text = Data.merge.header;
            helpBox.Text = Data.merge.help;
            taskRichBox.Text = Data.merge.task;
            pathBox.Text = Data.merge.path;
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
            openFileDialog1.Filter = "Текстовые файлы (*.txt)|*.txt";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pathBox.Text = openFileDialog1.FileName;
            }
        }
    }
}
