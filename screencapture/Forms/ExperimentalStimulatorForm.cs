﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.VFW;
using AForge.Video.DirectShow;
using CsvHelper;
using Tobii.Interaction;

namespace screencapture.Forms
{
    public partial class ExperimentalStimulatorForm : Form
    {
        string FIO;
        string pathToDirectory;
        string[] emotionDirectories;
        int picturesPerEmotion;
        int currentPictureBoxWidth;
        int currentPictureBoxHeight;
        string currentDirectory;
        Image currentImage;
        List<string> currentFiles = new List<string>();
        int currentDirectoryIndex = 0;
        int showPictureTime;
        int delayTime;

        List<Label> headerList = new List<Label>();
        int i;
        List<string> files = new List<string>();
        string currentImagePath;
        Dictionary<int, string> helplines = new Dictionary<int, string>();
        int z = 1;
        static int idx = 1;

        //для записи экрана
        private System.Drawing.Rectangle bounds;
        private System.Drawing.Size screenSize;
        //для записи веб-камеры
        private AVIWriter videoWebWriter;
        private VideoCaptureDevice device;
        //для айтрекинга
        private EyeTracker eyeTracker;

        public ExperimentalStimulatorForm(int picturesPerEmotion, int showPictureTime, int delayTime, string FIO, string pathToDirectory)
        {
            this.picturesPerEmotion = picturesPerEmotion;
            this.FIO = FIO;
            this.showPictureTime = showPictureTime * 1000;
            this.delayTime = delayTime * 1000;
            this.pathToDirectory = pathToDirectory;
            if (delayTime == 0)
            {
                this.delayTime = 1;
            }
            if (showPictureTime == 0)
            { this.showPictureTime = 1; }

            eyeTracker = EyeTracker.GetEyeTracker();
            eyeTracker.StartEyeStream();

            InitializeComponent();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if(startButton.Visible == true)
            {
                if (startButton.Text == "OK")
                {
                    this.Close();
                }
                else
                {
                    startButton.Visible = false;
                    descriptionLabel.Visible = false;
                    timerPictures.Interval = showPictureTime;
                    timerDelay.Interval = delayTime;
                    currentDirectory = emotionDirectories[currentDirectoryIndex];
                    currentFiles = Directory.GetFiles(currentDirectory).ToList();
                    for (int j = 0; j < picturesPerEmotion; j++)
                    {
                        if (currentFiles.Count == 0)
                        { break; }
                        Random rand = new Random();
                        int k = rand.Next(0, currentFiles.Count);
                        files.Add(currentFiles[k]);
                        currentFiles.RemoveAt(k);
                    }

                    pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                    try
                    {
                        showPicture(files[i]);
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        MessageBox.Show("В папке " + currentDirectory + " нет изображений");
                        currentDirectoryIndex++;
                        if (currentDirectoryIndex < emotionDirectories.Length)
                        {
                            pictureBox.Visible = false;
                            descriptionLabel.Location = new Point(this.ClientSize.Width / 2 - descriptionLabel.Size.Width / 2, 5);
                            descriptionLabel.Text = "Исследование приостановлено";
                            startButton.Text = "Продолжить";
                            descriptionLabel.Visible = true;
                            startButton.Visible = true;
                        }
                        else
                        {
                            pictureBox.Visible = false;
                            descriptionLabel.Location = new Point(this.ClientSize.Width / 2 - descriptionLabel.Size.Width / 2, 5);
                            descriptionLabel.Text = "Исследование завершено";
                            startButton.Text = "OK";
                            descriptionLabel.Visible = true;
                            startButton.Visible = true;
                        }
                    }
                }
            }
        }

        private void ExperimentalStimulatorForm_Load(object sender, EventArgs e) // Загружает путь к папке с картинками
        {
            Cursor.Hide();

            currentPictureBoxHeight = Convert.ToInt32(this.Height * 0.8);
            currentPictureBoxWidth = Convert.ToInt32(this.Width * 0.8);
            emotionDirectories = Directory.GetDirectories(pathToDirectory);
            this.WindowState = FormWindowState.Maximized;

            //выбор устройства (веб-камеры)
            var filter = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            try
            {
                device = new VideoCaptureDevice(filter[0].MonikerString);
            }
            catch
            {
                MessageBox.Show("Веб-камера не обнаружена, исследование не может быть начато!");
                this.Close();
                return;
            }

            //Установка разрешения
            device.VideoResolution = device.VideoCapabilities[device.VideoCapabilities.Length - 7]; //-7 для стенда

            //Старт записи
            device.Start();
        }

        private void SetPictureBoxPosition()
        {
            System.Drawing.Size size = new System.Drawing.Size(currentPictureBoxWidth, currentPictureBoxHeight);
            pictureBox.Size = size;
            int y = (this.ClientSize.Height - currentPictureBoxHeight) / 2;
            int x = (this.ClientSize.Width - currentPictureBoxWidth) / 2;
            pictureBox.Location = new Point(x, y);
        }

        private void ExperimentalStimulatorForm_Resize(object sender, EventArgs e) //устанавливает позицию для слов и подсказок при ресайзе формы
        {
            currentPictureBoxHeight = Convert.ToInt32(this.Height * 0.8);
            try
            {
                currentPictureBoxWidth = currentPictureBoxHeight * currentImage.Width / currentImage.Height;
            }
            catch (NullReferenceException)
            {
                currentPictureBoxWidth = Convert.ToInt32(this.Width * 0.8);
            }
            if (currentPictureBoxWidth > this.Width * 0.8)
            {
                currentPictureBoxWidth = Convert.ToInt32(this.Width * 0.8);
                //currentPictureBoxHeight = currentPictureBoxWidth * currentImage.Height / currentImage.Width;
            }
            SetPictureBoxPosition();
            descriptionLabel.Location = new Point(this.ClientSize.Width / 2 - descriptionLabel.Size.Width / 2, 5);
            startButton.Location = new Point(this.ClientSize.Width / 2 - startButton.Width / 2, this.ClientSize.Height - startButton.Height - 5);
        }

        private void timerPictures_Tick(object sender, EventArgs e)
        {
            timerPictures.Stop();
            hidePicture();
        }

        private void timerDelay_Tick(object sender, EventArgs e)
        {
            timerDelay.Stop();
            i++;
            if (i < files.Count)
            {       
                showPicture(files[i]);
            }
            else
            {
                currentDirectoryIndex++;
                if (currentDirectoryIndex < emotionDirectories.Length)
                {
                    files.Clear();
                    i = 0;
                    descriptionLabel.Location = new Point(this.ClientSize.Width / 2 - descriptionLabel.Size.Width / 2, 5);
                    descriptionLabel.Text = "Исследование приостановлено";
                    startButton.Text = "Продолжить";
                    descriptionLabel.Visible = true;
                    startButton.Visible = true;
                }
                else
                {
                    descriptionLabel.Location = new Point(this.ClientSize.Width / 2 - descriptionLabel.Size.Width / 2, 5);
                    descriptionLabel.Text = "Исследование завершено";
                    startButton.Text = "OK";
                    descriptionLabel.Visible = true;
                    startButton.Visible = true;
                }

                //также заканчиваем запись всего
                Data.id_edu = 0;
                device.NewFrame -= device_NewFrame;
                videoWebWriter.Close();
            }
        }

        private void showPicture(string path) //TODO: добавть начало записи данных с шапки и веб-камеры в отдельные файлы
        {
            Data.picPath = Path.GetFileName(path);
            currentImagePath = path;
            currentImage = Image.FromFile(path);
            currentPictureBoxHeight = Convert.ToInt32(this.Height * 0.8);
            currentPictureBoxWidth = currentPictureBoxHeight * currentImage.Width / currentImage.Height;
            if (currentPictureBoxWidth > this.Width * 0.8)
            {
                currentPictureBoxWidth = Convert.ToInt32(this.Width * 0.8);
                currentPictureBoxHeight = currentPictureBoxWidth * currentImage.Height / currentImage.Width;
            }
            SetPictureBoxPosition();
            pictureBox.Image = Image.FromFile(path);
            pictureBox.Visible = true;
            timerPictures.Start();

            Data.startVideoTime = DateTime.Now;

            videoWebWriter = new AVIWriter("IYUV");
            videoWebWriter.FrameRate = 15;
        
            videoWebWriter.Open(Path.Combine(emotionDirectories[currentDirectoryIndex].Replace(" ", "_") + "_" +
                Data.respondent.name.Replace(" ", "_") + "_" + Path.GetFileName(files[i]).Replace(".jpg", "") + "-web-" + z + ".avi"), 1280, 960);
            device.NewFrame += device_NewFrame;
            SaveScreen();
            Data.id_edu = 1;
        }

        private void hidePicture() //TODO: добавть окончание записи данных с шапки и веб-камеры
        {
            pictureBox.Visible = false;
            timerDelay.Start();

            //тормозим запись фрагмена с вебки
            Data.id_edu = 0;
            device.NewFrame -= device_NewFrame;
            videoWebWriter.Close();

            if(Data.eeg_mode != "muse2")
            {
                WriteCSV();
            }
            else
            {
                WriteMuse();
            } 
        }

        private void device_NewFrame(object sender, NewFrameEventArgs e)
        {
            try
            {
                Bitmap pbFrame = (Bitmap)e.Frame.Clone();
                videoWebWriter.AddFrame(pbFrame);
            }
            catch
            {

            }
        }

        private void SaveScreen()
        {
            Thread myThread = new Thread(new ThreadStart(ScrSave));
            myThread.Start(); // запускаем поток
        }

        void ScrSave()
        {
            Thread.Sleep(300);
            bounds = Screen.PrimaryScreen.Bounds;
            screenSize = new System.Drawing.Size(bounds.Width, bounds.Height);

            Bitmap frameImage = new Bitmap(screenSize.Width, screenSize.Height);

            using (Graphics g = Graphics.FromImage(frameImage))
            {
                g.CopyFromScreen(0, 0, 0, 0, screenSize, CopyPixelOperation.SourceCopy);
            }
            
            frameImage.Save(Path.Combine(emotionDirectories[currentDirectoryIndex].Replace(" ", "_") + "_" +
                Data.respondent.name.Replace(" ", "_") + "_" + Path.GetFileName(files[i]).Replace(".jpg", "") + "-img-" + z + ".bmp"));
        }

        private void WriteCSV()
        {
            bool bad = false;

            if (Data.mindList.Count > 0)
            {
                foreach (var item in Data.mindList)
                {
                    if (item.poor_signal != 0)
                    {
                        bad = true;
                        break;
                    }
                }
            }
            else
            {
                bad = true;
            }

            if(Data.mindList.Count > 0)
            {
                if (!bad)
                {
                    string csvpath = Path.Combine(emotionDirectories[currentDirectoryIndex].Replace(" ", "_") + "_" +
                        Data.respondent.name.Replace(" ", "_") + "_" + Path.GetFileName(files[i]).Replace(".jpg", "") + "-raw-" + z + ".csv");

                    using (var writer = new StreamWriter(csvpath))
                    using (var csv = new CsvWriter(writer))
                    {
                        csv.WriteRecords(Data.mindList);
                    }
                }
                else
                {
                    string csvpath = Path.Combine(emotionDirectories[currentDirectoryIndex].Replace(" ", "_") + "_" +
                        Data.respondent.name.Replace(" ", "_") + "_" + Path.GetFileName(files[i]).Replace(".jpg", "") + "-raw-FAIL" + z + ".csv");

                    using (var writer = new StreamWriter(csvpath))
                    using (var csv = new CsvWriter(writer))
                    {
                        csv.WriteRecords(Data.mindList);
                    }
                }
            }
            
            Data.mindList.Clear();
            z++;
        }

        private void ExperimentalStimulatorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                device.Stop();
                videoWebWriter.Close();
            }
            catch
            {

            }
            Cursor.Show();
        }

        private void WriteMuse()
        {
            string csvpath = Path.Combine(emotionDirectories[currentDirectoryIndex].Replace(" ", "_") + "_" +
                       Data.respondent.name.Replace(" ", "_") + "_" + Path.GetFileName(files[i]).Replace(".jpg", "") + "-muse-" + z + ".csv");

            using (var writer = new StreamWriter(csvpath))
            using (var csv = new CsvWriter(writer))
            {
                csv.WriteRecords(Data.museList);
            }
        
            Data.museList.Clear();
            z++;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Data.eeg_mode == "muse2" & Data.id_edu != 0)
            {
                //Data.emotion.id, Data.emotion.anger, Data.emotion.fear, Data.emotion.sadness, Data.emotion.happiness
                Data.museList.Add(new EncefalogramNamed(idx, (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds, (int)eyeTracker.gazeX, (int)eyeTracker.gazeY,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ""));
                idx++;
            }
            else
                idx = 1;
        }
    }
}