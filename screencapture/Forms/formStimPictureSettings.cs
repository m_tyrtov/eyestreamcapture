﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace screencapture.Forms
{
    public partial class FormStimPictureSettings : Form
    {
        MergeControl projects = new MergeControl();
        FileSystem fileSystem = new FileSystem();

        const int pauseKoef = 3;

        public FormStimPictureSettings()
        {
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            if (Data.mode == "create")
            {
                DialogResult result = MessageBox.Show("Вы уверены? Изменить название стимулятора в дальнейшем будет не возможно!", "Eye Stream Capture",
                MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (result == DialogResult.Yes)
                {
                    projects.UpdateMerge(Data.merge.id, nameBox.Text, headerBox.Text, helpBox.Text, taskRichBox.Text, pathBox.Text, Int32.Parse(timeBox.Text),
                        Int32.Parse(pauseTime.Text), Int32.Parse(textTime.Text), (int)numericUpDown4.Value);
                    this.Close();
                }
            }
            else
            {
                projects.UpdateMerge(Data.merge.id, nameBox.Text, headerBox.Text, helpBox.Text, taskRichBox.Text, pathBox.Text, Int32.Parse(timeBox.Text),
                    Int32.Parse(pauseTime.Text), Int32.Parse(textTime.Text), (int)numericUpDown4.Value);
                this.Close();
            }
        }

        private void FormStimPictureSettings_Load(object sender, EventArgs e)
        {
            if (Data.mode == "create")
                nameBox.Enabled = true;

            nameBox.Text = Data.merge.name;
            headerBox.Text = Data.merge.header;
            helpBox.Text = Data.merge.help;
            taskRichBox.Text = Data.merge.task;
            pathBox.Text = Data.merge.path;
            timeBox.Text = Data.merge.interval.ToString();

            pauseTime.Text = Data.merge.pause_time.ToString();
            textTime.Text = Data.merge.text_time.ToString();
            numericUpDown4.Value = Data.merge.part;

            //koefLabel.Text = "Время паузы после картинки: " + Int32.Parse(pauseTime.Text) * 2;
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                pathBox.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void pauseTime_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //koefLabel.Text = "Время паузы после картинки: " + Int32.Parse(pauseTime.Text) * 2;
            }
            catch
            {
                //koefLabel.Text = "Время паузы после картинки: ";
            }
        }
    }
}
