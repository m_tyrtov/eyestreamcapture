﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using System.Data.SQLite.Linq;
using System.Windows.Forms;
using System.Data;
using System.Configuration;

namespace screencapture.Forms
{
    class Stimulants
    {
        SQLiteConnection _SQLiteConnetion;
        SQLiteCommand _SQLiteCommand;
        SQLiteDataReader _SQLiteDataReader;
        private string pathToDB = ConfigurationManager.AppSettings["Default_Project_Directory"] + @"\project.sqlite";
        private string tableName = "stimulants";

        public int id { get; set; }
        public int sort { get; set; }
        public string name { get; set; }
        public string description { get; set; }

        public Stimulants() { }

        private Stimulants(int id, int sort, string name, string description)
        {
            this.id = id;
            this.sort = sort;
            this.name = name;
            this.description = description;
        }

        private void CreateOrUsingDB()
        {
            if (!File.Exists(pathToDB))
                SQLiteConnection.CreateFile(pathToDB);
            try
            {
                _SQLiteCommand = new SQLiteCommand();
                _SQLiteConnetion = new SQLiteConnection("Data Source=" + pathToDB + "; Synchronous=OFF;");
                _SQLiteConnetion.Open();
                _SQLiteCommand.Connection = _SQLiteConnetion;
                _SQLiteCommand.CommandText = "CREATE TABLE IF NOT EXISTS " + tableName + " (id INTEGER PRIMARY KEY AUTOINCREMENT, sort INTEGER, name TEXT, description TEXT)";
                _SQLiteCommand.ExecuteNonQuery();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Disconnected, Ошибка:{0}", ex.Message);
            }
            finally
            {
                _SQLiteConnetion.Close();
            }
        }

        private void ConnectToDB()
        {
            CreateOrUsingDB();

            _SQLiteCommand = new SQLiteCommand();
            _SQLiteConnetion = new SQLiteConnection(@"Data Source=" + pathToDB);
            _SQLiteCommand.Connection = _SQLiteConnetion;
            _SQLiteConnetion.Open();
        }

        private void CommandSQLQuery(string sqlQuery)
        {
            ConnectToDB();
            _SQLiteCommand.CommandText = sqlQuery;
            _SQLiteCommand.ExecuteNonQuery();
            _SQLiteConnetion.Close();
        }

        public List<Stimulants> GetListTargetAudinceItems(string sort)
        {
            ConnectToDB();

            List<Stimulants> list = new List<Stimulants>();
            DataTable dTable = new DataTable();
            String sqlQuery;

            try
            {
                sqlQuery = "SELECT * FROM " + tableName + " ORDER BY sort " + sort;
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, _SQLiteConnetion);
                adapter.Fill(dTable);

                if (dTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dTable.Rows.Count; i++)
                    {
                        list.Add(new Stimulants(Int32.Parse(dTable.Rows[i].ItemArray[0].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[1].ToString()),
                            dTable.Rows[i].ItemArray[2].ToString(), dTable.Rows[i].ItemArray[3].ToString()));
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            _SQLiteConnetion.Close();

            return list;
        }

        public void DeleteStimulantsItem(int id)
        {
            CommandSQLQuery("DELETE FROM " + tableName + " WHERE id=" + id);
        }

        public void AddStimulantsItem(int sort, string name, string description)
        {
            CommandSQLQuery("INSERT INTO " + tableName + " ( sort, name, description ) "
                + "VALUES('" + sort + "', '" + name + "', '" + description + "'); ");
        }

        public void UpdateStimulantsItem(int id, int sort, string name, string description)
        {
            CommandSQLQuery("UPDATE " + tableName + " SET sort = '" + sort
                + "', name = '" + name + "', description = '" + description + "' WHERE id = " + id);
        }

        public Dictionary<int, string> GetStimulantsName(List<int> id_collection)
        {
            Dictionary<int, string> list = new Dictionary<int, string>();

            ConnectToDB();

            foreach (int record in id_collection)
            {
                ConnectToDB();
                _SQLiteCommand.CommandText = "SELECT * FROM " + tableName + " WHERE id = " + record;
                _SQLiteDataReader = _SQLiteCommand.ExecuteReader();

                while (_SQLiteDataReader.Read())
                {
                    list.Add(record, _SQLiteDataReader[2].ToString());
                }

                _SQLiteConnetion.Close();
            }

            return list;
        }
    }
}
