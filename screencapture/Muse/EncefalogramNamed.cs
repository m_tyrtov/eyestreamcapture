﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace screencapture
{
    public class EncefalogramNamed
    {
        public int id { get; set; }
        public double timeStamp { get; set; }

        public int emo_id { get; set; }
        public int anger { get; set; }
        public int fear { get; set; }
        public int sadness { get; set; }
        public int happiness { get; set; }

        public float eeg_1 { get; set; }
        public float eeg_2 { get; set; }
        public float eeg_3 { get; set; }
        public float eeg_4 { get; set; }
        public float eeg_5 { get; set; }
        public float eeg_6 { get; set; }
        public float eeg_7 { get; set; }
        public float eeg_8 { get; set; }
        public float eeg_9 { get; set; }
        public float eeg_10 { get; set; }
        public float eeg_11 { get; set; }
        public float eeg_12 { get; set; }
        public float eeg_13 { get; set; }
        public float eeg_14 { get; set; }
        public float eeg_15 { get; set; }
        public float eeg_16 { get; set; }
        public float eeg_17 { get; set; }
        public float eeg_18 { get; set; }
        public float eeg_19 { get; set; }
        public float eeg_20 { get; set; }

        public float sample_1 { get; set; }
        public float sample_2 { get; set; }
        public float sample_3 { get; set; }
        public float sample_4 { get; set; }
        public float sample_5 { get; set; }
        public float sample_6 { get; set; }
        public float sample_7 { get; set; }
        public float sample_8 { get; set; }
        public float sample_9 { get; set; }
        public float sample_10 { get; set; }
        public float sample_11 { get; set; }
        public float sample_12 { get; set; }

        public string path { get; set; }

        public EncefalogramNamed() { }

        public EncefalogramNamed(int id, double timeStamp, int emo_id, int anger, int fear, int sadness, int happiness, 
            float eeg_1, float eeg_2, float eeg_3, float eeg_4, float eeg_5, float eeg_6, float eeg_7, float eeg_8, float eeg_9, 
                float eeg_10, float eeg_11, float eeg_12, float eeg_13, float eeg_14, float eeg_15, float eeg_16, float eeg_17, float eeg_18, 
                    float eeg_19, float eeg_20, float sample_1, float sample_2, float sample_3, float sample_4, float sample_5,
                        float sample_6, float sample_7, float sample_8, float sample_9, float sample_10, float sample_11, float sample_12,
                            string path)
        {
            this.id = id;
            this.timeStamp = timeStamp;

            this.emo_id = emo_id;
            this.anger = anger;
            this.fear = fear;
            this.sadness = sadness;
            this.happiness = happiness;

            this.eeg_1 = eeg_1;
            this.eeg_2 = eeg_2;
            this.eeg_3 = eeg_3;
            this.eeg_4 = eeg_4;
            this.eeg_5 = eeg_5;
            this.eeg_6 = eeg_6;
            this.eeg_7 = eeg_7;
            this.eeg_8 = eeg_8;
            this.eeg_9 = eeg_9;
            this.eeg_10 = eeg_10;
            this.eeg_11 = eeg_11;
            this.eeg_12 = eeg_12;
            this.eeg_13 = eeg_13;
            this.eeg_14 = eeg_14;
            this.eeg_15 = eeg_15;
            this.eeg_16 = eeg_16;
            this.eeg_17 = eeg_17;
            this.eeg_18 = eeg_18;
            this.eeg_19 = eeg_19;
            this.eeg_20 = eeg_20;

            this.sample_1 = sample_1;
            this.sample_2 = sample_2;
            this.sample_3 = sample_3;
            this.sample_4 = sample_4;
            this.sample_5 = sample_5;
            this.sample_6 = sample_6;
            this.sample_7 = sample_7;
            this.sample_8 = sample_8;
            this.sample_9 = sample_9;
            this.sample_10 = sample_10;
            this.sample_11 = sample_11;
            this.sample_12 = sample_12;

            this.path = path;
        }
    }
}
