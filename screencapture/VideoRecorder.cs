﻿using System;
using System.Collections.Generic;
using System.Text;
using Accord;
using Accord.Video.FFMPEG;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using Tobii.Interaction;
using Tobii.Interaction.Framework;
using System.Collections;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using Timer = System.Timers.Timer;
using System.Text.RegularExpressions;
using screencapture.Properties;
using System.Configuration;
using System.Threading.Tasks;
using System.Threading;
using ThinkGearNET;
using AForge.Video;
using AForge.Video.VFW;
using AForge.Video.DirectShow;

namespace screencapture
{
    public class VideoRecorder
    {
        private VideoFileWriter videoWriter;
        private EyeTracker eyeTracker;
        private FileSystem fileSystem = new FileSystem();
        private PointDataSQLite SQLite = new PointDataSQLite();

        private const int intervalBetweenFrames = 32; // 1000 / fps
        private Timer frameTimer;
        
        private bool isRecording;
        private bool writing = false;

        //string descriptionText;
        public string backupPath;

        double mouseX, mouseY, tempGazeX, tempGazeY, timer;
        int gazeMarkerSize;

        //NeuroSky Values
        int ns_attention;
        int ns_meditation;

        int ns_alpha_1;
        int ns_alpha_2;

        int ns_beta_1;
        int ns_beta_2;

        int ns_gamma_1;
        int ns_gamma_2;

        int ns_blink;
        int poor_signal = 200;

        int raw;

        private int raw_fixed;
        private int i = 1; //counter

        private System.Drawing.Size screenSize;
        Pen gazeMarkerpPen = new Pen(Color.FromArgb(0, 124, 173), 5);
        Image cursor = Resources.cursor_yellow;

        ThinkGearWrapper thinkGear;

        public VideoRecorder()
        {
            eyeTracker = EyeTracker.GetEyeTracker();
            videoWriter = new VideoFileWriter();

            //thinkGear FIX
            thinkGear = new ThinkGearWrapper();

            //Получение границ экрана
            System.Drawing.Rectangle bounds = Screen.PrimaryScreen.Bounds;
            screenSize = new System.Drawing.Size(bounds.Width, bounds.Height);

            //Таймер запускающий запись и редактирование кадра с заданой частотой(FrameRate)
            //frameTimer = new Timer(intervalBetweenFrames);
            //frameTimer.Elapsed += ProcessFrame;
            //frameTimer.AutoReset = true;

            //isRecording = false;
        }

        public void StartRecording()
        {
            gazeMarkerSize = Convert.ToInt32(ConfigurationManager.AppSettings["Size_Gaze_Marker"]);
            eyeTracker.StartEyeStream();

            if (Data.examination.state != "start")
            {
                SQLite.ListPointsData = new List<FramePoint>();
                SQLite.ListPointsRaw = new List<RawPoint>();

                gazeMarkerSize = Convert.ToInt32(ConfigurationManager.AppSettings["Size_Gaze_Marker"]);
                isRecording = true;

                Data.pathtovideo = Path.Combine(fileSystem.CreateOutputDirectory(Data.project.path, Data.merge.name, Data.examination.name),
                    Data.examination.name + ".avi");

                int i = 1;
                while (File.Exists(Data.pathtovideo))
                {
                    Data.pathtovideo = Path.Combine(fileSystem.CreateOutputDirectory(Data.project.path, Data.merge.name, Data.examination.name),
                        Data.examination.name + "_" + i + ".avi");
                    i++;
                }

                //videoWriter.Open(Data.pathtovideo, screenSize.Width, screenSize.Height, 25, VideoCodec.MPEG4);
                //frameTimer.Start();
            }
        }

        void thinkGear_ThinkGearChanged(object sender, ThinkGearChangedEventArgs e)
        {
            if ((Convert.ToInt32(e.ThinkGearState.Raw) != raw_fixed))
            {
                raw = Convert.ToInt32(e.ThinkGearState.Raw);

                try
                {
                    if (Data.id_edu != 0)
                    {
                        tempGazeX = eyeTracker.gazeX;
                        tempGazeY = eyeTracker.gazeY;

                        mouseX = Cursor.Position.X;
                        mouseY = Cursor.Position.Y;
                        
                        Data.mindList.Add(new MindTraining(i, (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds, 0, 0, 0, 0, 0, (int) mouseX, (int) mouseY, (int) tempGazeX, (int) tempGazeY, 
                                Convert.ToInt32(e.ThinkGearState.Meditation), Convert.ToInt32(e.ThinkGearState.Attention), Convert.ToInt32(e.ThinkGearState.Alpha1), 
                                    Convert.ToInt32(e.ThinkGearState.Alpha2), Convert.ToInt32(e.ThinkGearState.Beta1), Convert.ToInt32(e.ThinkGearState.Beta2), 
                                        Convert.ToInt32(e.ThinkGearState.Gamma1), Convert.ToInt32(e.ThinkGearState.Gamma2), Convert.ToInt32(e.ThinkGearState.PoorSignal),
                                            Convert.ToInt32(e.ThinkGearState.BlinkStrength), Convert.ToInt32(e.ThinkGearState.Raw), Data.picPath));
                        i++;
                    }
                    else
                        i = 1;
                }
                catch
                {
                    MessageBox.Show("Исключение в шапке!");
                }

                raw_fixed = raw;
            }
            
            poor_signal = Convert.ToInt32(e.ThinkGearState.PoorSignal);
            ns_attention = Convert.ToInt32(e.ThinkGearState.Attention);
            ns_meditation = Convert.ToInt32(e.ThinkGearState.Meditation);
        }

        public bool ThinkGearStart(){
            SQLite.ListPointsRaw = new List<RawPoint>();
            string defaultPort = ConfigurationManager.AppSettings["COM_Port"];
            thinkGear.ThinkGearChanged += thinkGear_ThinkGearChanged;
            thinkGear.Connect(defaultPort, ThinkGear.BAUD_57600, true); //подключаемся

            thinkGear.EnableBlinkDetection(true); //тест записи моргания

            while (true)
            {
                if ((poor_signal == 0) & (ns_attention != 0) & (ns_meditation != 0))
                    return true;
            }
        }

        public void ThinkGearStop()
        {
            thinkGear.Disconnect();
        }

        public void EndRecording()
        {
            isRecording = false;
            while (writing) { }
            //frameTimer.Stop();
            //videoWriter.Close();
            eyeTracker.StopEyeStream();

            //thinkGear
            thinkGear.ThinkGearChanged -= thinkGear_ThinkGearChanged;
            thinkGear.Disconnect();

            //SQLite.SerializeToBD(); //сериализация в БД

            //if (Data.eeg_mode == "muse2")
            //{
            //    WritePyMuse();
            //}
        }

        //private void stopPyMuse()
        //{
        //    FileStream fs = File.Create(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "stop.txt"));
        //    fs.Close();
        //    Thread.Sleep(700);
        //}

        //private void WritePyMuse()
        //{
        //    stopPyMuse();
        //    Thread.Sleep(700);
        //    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "temp.csv");
        //}

        //private void ProcessFrame(object source, System.Timers.ElapsedEventArgs e)
        //{
        //    tempGazeX = eyeTracker.gazeX;
        //    tempGazeY = eyeTracker.gazeY;

        //    Data.GazeX = (int) eyeTracker.gazeX;
        //    Data.GazeY = (int) eyeTracker.gazeY;

        //    mouseX = Cursor.Position.X;
        //    mouseY = Cursor.Position.Y;
        //    timer = (DateTime.Now - Data.startVideoTime).TotalSeconds;

        //    Bitmap frameImage = new Bitmap(screenSize.Width, screenSize.Height);

        //    using (Graphics g = Graphics.FromImage(frameImage))
        //    {
        //        g.CopyFromScreen(0, 0, 0, 0, screenSize, CopyPixelOperation.SourceCopy);
        //    }

        //    if (isRecording && !writing)
        //    {
        //        writing = true;

        //        //SQLite.LogWriting(tempGazeX, tempGazeY, mouseX, mouseY, timer, 0, 0, ns_meditation, ns_attention,
        //        //    ns_alpha_1, ns_alpha_2, ns_beta_1, ns_beta_2, ns_gamma_1, ns_gamma_2, poor_signal, ns_blink, raw);

        //        videoWriter.WriteVideoFrame(frameImage);
        //        writing = false;
        //    }

        //    frameImage.Dispose();
        //}

    }

}