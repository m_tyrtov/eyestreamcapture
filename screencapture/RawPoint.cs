﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace screencapture
{
    public struct RawPoint
    {        
        public int id { get; set; }
        public double timeStamp { get; set; }
        public int mouseX { get; set; }
        public int mouseY { get; set; }
        public int gazeX { get; set; }
        public int gazeY { get; set; }
        public int cluster { get; set; }
        public double timeGazeFixed { get; set; }

        public int emo_id { get; set; }
        public int anger { get; set; }
        public int fear { get; set; }
        public int sadness { get; set; }
        public int happiness { get; set; }

        public int raw { get; set; }
        
        public RawPoint(int id = 0, double timeStamp = 0, int mouseX = 0, int mouseY = 0, int gazeX = 0, int gazeY = 0, int cluster = 0, double timeGazeFixed = 0, 
            int emo_id = 0, int anger = 0, int fear = 0, int sadness = 0, int happiness = 0, int raw = 0)
        {
            this.id = id;
            this.timeStamp = timeStamp;
            this.mouseX = mouseX;
            this.mouseY = mouseY;
            this.gazeX = gazeX;
            this.gazeY = gazeY;
            this.cluster = cluster;
            this.timeGazeFixed = timeGazeFixed;

            this.emo_id = emo_id;
            this.anger = anger;
            this.fear = fear;
            this.sadness = sadness;
            this.happiness = happiness;

            this.raw = raw;
        }
    }
}
