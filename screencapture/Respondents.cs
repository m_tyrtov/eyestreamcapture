﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using System.Data.SQLite.Linq;
using System.Windows.Forms;
using System.Data;
using System.Configuration;

namespace screencapture
{
    class Respondents
    {
        SQLiteConnection _SQLiteConnetion;
        SQLiteCommand _SQLiteCommand;
        private string pathToDB = ConfigurationManager.AppSettings["Default_Project_Directory"] + @"\project.sqlite";
        private string tableName = "respondents";

        public int id { get; set; }
        public int sort { get; set; }
        public string gender { get; set; }
        public int age { get; set; }
        public int audince_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }

        public Respondents() { }

        private Respondents(int id, int sort, string gender, int age, int audince_id, string name, string description)
        {
            this.id = id;
            this.sort = sort;
            this.gender = gender;
            this.age = age;
            this.audince_id = audince_id;
            this.name = name;
            this.description = description;
        }

        private void CreateOrUsingDB()
        {
            if (!File.Exists(pathToDB))
                SQLiteConnection.CreateFile(pathToDB);
            try
            {
                _SQLiteCommand = new SQLiteCommand();
                _SQLiteConnetion = new SQLiteConnection("Data Source=" + pathToDB + "; Synchronous=OFF;");
                _SQLiteConnetion.Open();
                _SQLiteCommand.Connection = _SQLiteConnetion;
                _SQLiteCommand.CommandText = "CREATE TABLE IF NOT EXISTS " + tableName + " (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "sort INTEGER, gender TEXT, age INTEGER, audince_id INTEGER, name TEXT, description TEXT)";
                _SQLiteCommand.ExecuteNonQuery();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Disconnected, Ошибка:{0}", ex.Message);
            }
            finally
            {
                _SQLiteConnetion.Close();
            }
        }

        private void ConnectToDB()
        {
            CreateOrUsingDB();

            _SQLiteCommand = new SQLiteCommand();
            _SQLiteConnetion = new SQLiteConnection(@"Data Source=" + pathToDB);
            _SQLiteCommand.Connection = _SQLiteConnetion;
            _SQLiteConnetion.Open();
        }

        private void CommandSQLQuery(string sqlQuery)
        {
            ConnectToDB();
            _SQLiteCommand.CommandText = sqlQuery;
            _SQLiteCommand.ExecuteNonQuery();
            _SQLiteConnetion.Close();
        }

        public List<Respondents> GetListRespondentsItems(string sort)
        {
            ConnectToDB();

            List<Respondents> list = new List<Respondents>();
            DataTable dTable = new DataTable();
            String sqlQuery;

            try
            {
                sqlQuery = "SELECT * FROM " + tableName + " ORDER BY sort " + sort;
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, _SQLiteConnetion);
                adapter.Fill(dTable);

                if (dTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dTable.Rows.Count; i++)
                    {
                        list.Add(new Respondents(Int32.Parse(dTable.Rows[i].ItemArray[0].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[1].ToString()),
                            dTable.Rows[i].ItemArray[2].ToString(), Int32.Parse(dTable.Rows[i].ItemArray[3].ToString()),
                                Int32.Parse(dTable.Rows[i].ItemArray[4].ToString()), dTable.Rows[i].ItemArray[5].ToString(), dTable.Rows[i].ItemArray[6].ToString()));
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            _SQLiteConnetion.Close();

            return list;
        }

        public void DeleteRespondentsItem(int id)
        {
            CommandSQLQuery("DELETE FROM " + tableName + " WHERE id=" + id);
        }

        public void AddRespondentsItem(int sort, string gender, int age, int audince_id, string name, string description)
        {
            CommandSQLQuery("INSERT INTO " + tableName + " ( sort, gender, age, audince_id, name, description ) "
                + "VALUES('" + sort + "', '" + gender + "', '" + age + "', '" + audince_id + "', '" + name + "', '" + description + "'); ");
        }

        public void UpdateRespondentsItem(int id, int sort, string gender, int age, int audince_id, string name, string description)
        {
            CommandSQLQuery("UPDATE " + tableName + " SET sort = '" + sort + "', name = '" + name + "', description = '" + description + "', gender = '" + gender +
                "', age = '" + age + "', audince_id = '" + audince_id + "' WHERE id = " + id);
        }

        public Respondents GetInfoRespondentsItem(int id)
        {
            ConnectToDB();

            Respondents record = new Respondents();
            DataTable dTable = new DataTable();
            String sqlQuery;

            try
            {
                sqlQuery = "SELECT * FROM " + tableName + " WHERE id = " + id;
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, _SQLiteConnetion);
                adapter.Fill(dTable);

                if (dTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dTable.Rows.Count; i++)
                    {
                        record = new Respondents(Int32.Parse(dTable.Rows[i].ItemArray[0].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[1].ToString()),
                            dTable.Rows[i].ItemArray[2].ToString(), Int32.Parse(dTable.Rows[i].ItemArray[3].ToString()),
                                Int32.Parse(dTable.Rows[i].ItemArray[4].ToString()), dTable.Rows[i].ItemArray[5].ToString(), dTable.Rows[i].ItemArray[6].ToString());
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            _SQLiteConnetion.Close();

            return record;
        }
    }
}