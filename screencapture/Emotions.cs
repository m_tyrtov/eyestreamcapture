﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using System.Data.SQLite.Linq;
using System.Windows.Forms;
using System.Data;
using System.Configuration;

namespace screencapture
{
    class Emotions
    {
        SQLiteConnection _SQLiteConnetion;
        SQLiteCommand _SQLiteCommand;
        SQLiteDataReader _SQLiteDataReader;

        Result result = new Result();
        List<Result> result_list = new List<Result>();
        
        private string pathToDB = ConfigurationManager.AppSettings["Default_Project_Directory"] + @"\project.sqlite";
        private string tableName = "emotions";

        //default class properties
        public int id { get; set; }
        public int sort { get; set; }
        public string name { get; set; }
        public string name_en { get; set; }

        //goldman default properties
        public int anger { get; set; }
        public int fear { get; set; }
        public int sadness { get; set; }
        public int happiness { get; set; }

        //attitudes to theories
        public bool goldman { get; set; }
        public bool microsoft { get; set; }
        public bool leontyev { get; set; }
        public bool emodetect { get; set; }

        public Emotions() { }

        private Emotions(int id, int sort, string name, string name_en,
                            int anger, int fear, int sadness, int happiness,
                                bool goldman, bool microsoft, bool leontyev, bool emodetect)
        {
            this.id = id;
            this.sort = sort;
            this.name = name;
            this.name_en = name_en;

            this.anger = anger;
            this.fear = fear;
            this.sadness = sadness;
            this.happiness = happiness;

            this.goldman = goldman;
            this.microsoft = microsoft;
            this.leontyev = leontyev;
            this.emodetect = emodetect;
        }

        private void CreateOrUsingDB()
        {
            if (!File.Exists(pathToDB))
                SQLiteConnection.CreateFile(pathToDB);
            try
            {
                _SQLiteCommand = new SQLiteCommand();
                _SQLiteConnetion = new SQLiteConnection("Data Source=" + pathToDB + "; Synchronous=OFF;");
                _SQLiteConnetion.Open();
                _SQLiteCommand.Connection = _SQLiteConnetion;
                _SQLiteCommand.CommandText = "CREATE TABLE IF NOT EXISTS " + tableName +
                    " (id INTEGER PRIMARY KEY AUTOINCREMENT, sort INTEGER, name TEXT, name_en TEXT, anger INTEGER, fear INTEGER, sadness INTEGER, happiness INTEGER, " +
                    "goldman TEXT, microsoft TEXT, leontyev TEXT, emodetect TEXT)";
                _SQLiteCommand.ExecuteNonQuery();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Disconnected, Ошибка:{0}", ex.Message);
            }
            finally
            {
                _SQLiteConnetion.Close();
            }
        }

        private void ConnectToDB()
        {
            CreateOrUsingDB();

            _SQLiteCommand = new SQLiteCommand();
            _SQLiteConnetion = new SQLiteConnection(@"Data Source=" + pathToDB);
            _SQLiteCommand.Connection = _SQLiteConnetion;
            _SQLiteConnetion.Open();
        }

        private void CommandSQLQuery(string sqlQuery)
        {
            ConnectToDB();
            _SQLiteCommand.CommandText = sqlQuery;
            _SQLiteCommand.ExecuteNonQuery();
            _SQLiteConnetion.Close();
        }

        public List<Emotions> GetListEmotionsItems(string sort, string select)
        {
            ConnectToDB();
            List<Emotions> list = new List<Emotions>();

            DataTable dTable = new DataTable();
            string sqlQuery;

            try
            {
                if (select == "Все")
                {
                    sqlQuery = "SELECT * FROM " + tableName + " ORDER BY sort " + sort;

                    SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, _SQLiteConnetion);
                    adapter.Fill(dTable);

                    if (dTable.Rows.Count > 0)
                    {
                        for (int i = 0; i < dTable.Rows.Count; i++)
                        {
                            list.Add(new Emotions(Int32.Parse(dTable.Rows[i].ItemArray[0].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[1].ToString()), dTable.Rows[i].ItemArray[2].ToString(), dTable.Rows[i].ItemArray[3].ToString(),
                                Int32.Parse(dTable.Rows[i].ItemArray[4].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[5].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[6].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[7].ToString()),
                                Checked(dTable.Rows[i].ItemArray[8].ToString()), Checked(dTable.Rows[i].ItemArray[9].ToString()), Checked(dTable.Rows[i].ItemArray[10].ToString()), Checked(dTable.Rows[i].ItemArray[11].ToString())));
                        }
                    }
                }   
                else
                {
                    switch (select)
                    {
                        case "Голдман":
                            select = "goldman";
                            break;
                        case "Microsoft":
                            select = "microsoft";
                            break;
                        case "Леонтьев":
                            select = "leontyev";
                            break;
                        case "Emodetect":
                            select = "emodetect";
                            break;
                        case "Некорректные":
                            select = "flag";
                            break;
                    }

                    if(select != "flag")
                    {
                        sqlQuery = "SELECT * FROM " + tableName + " WHERE " + select + " = 'True' ORDER BY sort " + sort;

                        SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, _SQLiteConnetion);
                        adapter.Fill(dTable);

                        if (dTable.Rows.Count > 0)
                        {
                            for (int i = 0; i < dTable.Rows.Count; i++)
                            {
                                list.Add(new Emotions(Int32.Parse(dTable.Rows[i].ItemArray[0].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[1].ToString()), dTable.Rows[i].ItemArray[2].ToString(), dTable.Rows[i].ItemArray[3].ToString(),
                                    Int32.Parse(dTable.Rows[i].ItemArray[4].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[5].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[6].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[7].ToString()),
                                    Checked(dTable.Rows[i].ItemArray[8].ToString()), Checked(dTable.Rows[i].ItemArray[9].ToString()), Checked(dTable.Rows[i].ItemArray[10].ToString()), Checked(dTable.Rows[i].ItemArray[11].ToString())));
                            }
                        }
                    } 
                    else
                    {
                        result_list = result.GetListExaminationsItems(Data.examination.id, 1);
                        foreach(var record in result_list)
                        {
                            if(record.flag == "R")
                            {
                                list.Add(GiveMeOneEmotionsPlease("SELECT * FROM " + tableName + " WHERE id = '" + record.emotion_id + "' ORDER BY sort " + sort));
                            }
                        }
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            _SQLiteConnetion.Close();

            return list;
        }

        private Emotions GiveMeOneEmotionsPlease(string sqlQuery)
        {
            DataTable dTable = new DataTable();
            SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, _SQLiteConnetion);
            adapter.Fill(dTable);

            if (dTable.Rows.Count > 0)
            {
                int i = 0; //ленивый костыль!
                return new Emotions(Int32.Parse(dTable.Rows[i].ItemArray[0].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[1].ToString()), dTable.Rows[i].ItemArray[2].ToString(), dTable.Rows[i].ItemArray[3].ToString(),
                     Int32.Parse(dTable.Rows[i].ItemArray[4].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[5].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[6].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[7].ToString()),
                         Checked(dTable.Rows[i].ItemArray[8].ToString()), Checked(dTable.Rows[i].ItemArray[9].ToString()), Checked(dTable.Rows[i].ItemArray[10].ToString()), Checked(dTable.Rows[i].ItemArray[11].ToString()));
            }
            else
                return null;
        }

        private bool Checked(string value)
        {
            if (value == "True")
                return true;
            else
                return false;
        }

        public void AddEmotionsItem(int sort, string name, string name_en, int anger, int fear, int sadness, int happiness, bool goldman, bool microsoft, bool leontyev, bool emodetect)
        {
            if (CheckEmotion(name, id))
                CommandSQLQuery("INSERT INTO " + tableName + " ( sort, name, name_en, anger, fear, sadness, happiness, goldman, microsoft, leontyev, emodetect ) " +
                    "VALUES('" + sort + "', '" + name + "', '" + name_en + "', '" + anger + "', '" + fear + "', '" + sadness + "', '" + happiness + "', '" + goldman + "', '" + microsoft + "', '" + leontyev + "', '" + emodetect + "');");
            else
                MessageBox.Show("Не возможно добавить, такая эмоция уже есть!", "Eye Stream Capture");
        }

        public void UpdateEmotionsItem(int id, int sort, string name, string name_en, int anger, int fear, int sadness, int happiness, bool goldman, bool microsoft, bool leontyev, bool emodetect)
        {
            //if(CheckEmotion(name, id))

            //else
            //    MessageBox.Show("Не возможно отредактировать, такая эмоция уже есть!", "Eye Stream Capture");
            CommandSQLQuery("UPDATE emotions SET sort = '" + sort + "', name = '" + name + "', name_en = '" + name_en + "', anger = '" + anger + "', fear = '" + fear +
                "', sadness = '" + sadness + "', happiness = '" + happiness + "', goldman = '" + goldman + "', microsoft = '" + microsoft +
                    "', leontyev = '" + leontyev + "', emodetect = '" + emodetect + "' WHERE id = " + id);
        }

        public bool CheckEmotion(string name, int id)
        {
            if (CommandSQLScalar("SELECT COUNT(*) FROM " + tableName + " WHERE name = '" + name + "' and id != '" + id + "'") > 0)
                return true;
            return false;
        }

        private int CommandSQLScalar(string sqlQuery)
        {
            ConnectToDB();
            _SQLiteCommand.CommandText = sqlQuery;
            _SQLiteCommand.ExecuteScalar();
            int count = Int32.Parse(_SQLiteCommand.ExecuteScalar().ToString());
            _SQLiteConnetion.Close();

            return count;
        } 

        public string GetEmotionsName(int id)
        {
            ConnectToDB();
            _SQLiteCommand.CommandText = "SELECT * FROM " + tableName + " WHERE id = '" + id + "'";
            _SQLiteDataReader = _SQLiteCommand.ExecuteReader();

            string name = "";

            while (_SQLiteDataReader.Read())
            {
                 name = _SQLiteDataReader[2].ToString();
            }

            _SQLiteConnetion.Close();
            return name;
        }

        public Emotions GetEmotionsInfo(int id)
        {
            ConnectToDB();

            Emotions record = new Emotions();
            DataTable dTable = new DataTable();
            String sqlQuery;

            try
            {
                sqlQuery = "SELECT * FROM " + tableName + " WHERE id = " + id;
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, _SQLiteConnetion);
                adapter.Fill(dTable);

                if (dTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dTable.Rows.Count; i++)
                    {
                        record = new Emotions(Int32.Parse(dTable.Rows[i].ItemArray[0].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[1].ToString()), dTable.Rows[i].ItemArray[2].ToString(), dTable.Rows[i].ItemArray[3].ToString(),
                            Int32.Parse(dTable.Rows[i].ItemArray[4].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[5].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[6].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[7].ToString()),
                            Checked(dTable.Rows[i].ItemArray[8].ToString()), Checked(dTable.Rows[i].ItemArray[9].ToString()), Checked(dTable.Rows[i].ItemArray[10].ToString()), Checked(dTable.Rows[i].ItemArray[11].ToString()));
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            _SQLiteConnetion.Close();

            return record;
        }
    }
}
