﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;
using System.Data.SQLite.Linq;
using System.Windows.Forms;
using System.Data;
using System.Configuration;

namespace screencapture
{
    class Result
    {
        SQLiteConnection _SQLiteConnetion;
        SQLiteCommand _SQLiteCommand;

        private string pathToDB = ConfigurationManager.AppSettings["Default_Project_Directory"] + @"\project.sqlite";
        private string tableName = "dataset";

        public int id { get; set; } //ai
        public string flag { get; set; }
        public int exam_id { get; set; } //will be added in the process
        public int product_id { get; set; } //known
        public int stim_id { get; set; } //known
        public int emotion_id { get; set; } //known
        public int respondent_id { get; set; } //known

        public string stim_name { get; set; } //known
        public string stimulant { get; set; } //known

        public int edu_code { get; set; } //known
        public string output_data { get; set; } //will be added in the process
        public string output_video { get; set; } //will be added in the process

        public Result() { }

        public Result(int id, string flag, int exam_id, int product_id, int stim_id, int emotion_id, string stim_name, string stimulant, int respondent_id, int edu_code, string output_data, string output_video)
        {
            this.id = id;
            this.flag = flag;
            this.exam_id = exam_id;
            this.product_id = product_id;
            this.stim_id = stim_id;
            this.emotion_id = emotion_id;

            this.stim_name = stim_name;
            this.stimulant = stimulant;

            this.respondent_id = respondent_id;
            this.edu_code = edu_code;
            this.output_data = output_data;
            this.output_video = output_video;
        }

        private void CreateOrUsingDB()
        {
            if (!File.Exists(pathToDB))
                SQLiteConnection.CreateFile(pathToDB);
            try
            {
                _SQLiteCommand = new SQLiteCommand();
                _SQLiteConnetion = new SQLiteConnection("Data Source=" + pathToDB + "; Synchronous=OFF;");
                _SQLiteConnetion.Open();
                _SQLiteCommand.Connection = _SQLiteConnetion;
                _SQLiteCommand.CommandText = "CREATE TABLE IF NOT EXISTS " + tableName +
                    " (	id INTEGER PRIMARY KEY AUTOINCREMENT, flag TEXT, exam_id INTEGER, product_id INTEGER, stim_id INTEGER, emotion_id INTEGER, input_data TEXT," +
                    " stim_name TEXT, stimulant TEXT, respondent_id INTEGER, edu_code INTEGER, output_data TEXT, output_video TEXT)";
                _SQLiteCommand.ExecuteNonQuery();
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Disconnected, Ошибка:{0}", ex.Message);
            }
            finally
            {
                _SQLiteConnetion.Close();
            }
        }

        public List<Result> GetListExaminationsItems(int exam_id, int edu_code = 0)
        {
            ConnectToDB();

            List<Result> list = new List<Result>();
            DataTable dTable = new DataTable();
            String sqlQuery;

            try
            {
                if(edu_code == 0)
                    sqlQuery = "SELECT * FROM " + tableName + " WHERE exam_id = '" + exam_id + "'";
                else
                    sqlQuery = "SELECT * FROM " + tableName + " WHERE exam_id = '" + exam_id + "' and edu_code = '" + edu_code + "'";

                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sqlQuery, _SQLiteConnetion);
                adapter.Fill(dTable);

                if (dTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dTable.Rows.Count; i++)
                    {
                        list.Add(new Result(Int32.Parse(dTable.Rows[i].ItemArray[0].ToString()), dTable.Rows[i].ItemArray[1].ToString(), Int32.Parse(dTable.Rows[i].ItemArray[2].ToString()),
                            Int32.Parse(dTable.Rows[i].ItemArray[3].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[4].ToString()),
                                 Int32.Parse(dTable.Rows[i].ItemArray[5].ToString()), dTable.Rows[i].ItemArray[6].ToString(), dTable.Rows[i].ItemArray[7].ToString(),
                                    Int32.Parse(dTable.Rows[i].ItemArray[8].ToString()), Int32.Parse(dTable.Rows[i].ItemArray[9].ToString()),
                                        "", ""));

                        string testvalue = dTable.Rows[i].ItemArray[0].ToString();
                    }
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }

            _SQLiteConnetion.Close();

            return list;
        }

        public void SaveTrainingResult(int id, string output_data)
        {
            CommandSQLQuery("UPDATE " + tableName +" SET edu_code = '2', flag = 'S', output_data = '" + output_data +"' WHERE id = '" + id +"'");
        }
        
        public void SetFlag(int id, string flag)
        {
            CommandSQLQuery("UPDATE " + tableName + " SET flag = '" + flag + "' WHERE id = '" + id + "'");
        }

        public void UpdateTrainigImage(int id, string pic_path)
        {
            CommandSQLQuery("UPDATE " + tableName + " SET stimulant = '" + pic_path + "' WHERE id = '" + id + "'");
        }

        public void UpdateTrainingVideo(int id, string video_path)
        {
            CommandSQLQuery("UPDATE " + tableName + " SET output_video = '" + video_path + "' WHERE id = '" + id + "'");
        }

        public void Reset(int exam_id)
        {
            CommandSQLQuery("UPDATE " + tableName + " SET edu_code = '1', output_data = '', output_video = '' WHERE exam_id = '" + exam_id + "'");
        }

        private void ConnectToDB()
        {
            CreateOrUsingDB();

            _SQLiteCommand = new SQLiteCommand();
            _SQLiteConnetion = new SQLiteConnection(@"Data Source=" + pathToDB);
            _SQLiteCommand.Connection = _SQLiteConnetion;
            _SQLiteConnetion.Open();
        }

        private void CommandSQLQuery(string sqlQuery)
        {
            ConnectToDB();
            _SQLiteCommand.CommandText = sqlQuery;
            _SQLiteCommand.ExecuteNonQuery();
            _SQLiteConnetion.Close();
        }

        private int CommandSQLScalar(string sqlQuery)
        {
            ConnectToDB();
            _SQLiteCommand.CommandText = sqlQuery;
            _SQLiteCommand.ExecuteScalar();
            int count = Int32.Parse(_SQLiteCommand.ExecuteScalar().ToString());
            _SQLiteConnetion.Close();

            return count;
        }

        public void AddTrainingItem(int exam_id, int product_id, int stim_id, int emotion_id, int respondent_id, string stim_name, string stimulant)
        {
            if (!CheckTrainingPreset(exam_id, emotion_id))
                CommandSQLQuery("INSERT INTO " + tableName + " ( flag, exam_id, product_id, stim_id, emotion_id, respondent_id, stim_name, stimulant, edu_code ) "
                    + "VALUES('N', '" + exam_id + "', '" + product_id + "', '" + stim_id + "', '" + emotion_id + "', '" + respondent_id + "', '" + stim_name + "', '" + stimulant + "', '1'); ");
            else
                //if (Data.result.flag == "R")
                //    CommandSQLQuery("UPDATE " + tableName + " SET stim_name = '" + stim_name + "', stimulant = '" + stimulant + "', flag = 'N', edu_code = '1' WHERE exam_id = '" + exam_id + "' and emotion_id = '" + emotion_id + "'");
                //else
                    CommandSQLQuery("UPDATE " + tableName + " SET stim_name = '" + stim_name + "', stimulant = '" + stimulant + "' WHERE exam_id = '" + exam_id + "' and emotion_id = '" + emotion_id + "'");
        }

        public void AddExamItem(int exam_id, int product_id, int stim_id, int respondent_id, string stimulant)
        {
            CommandSQLQuery("INSERT INTO " + tableName + " ( flag, exam_id, product_id, stim_id, respondent_id, stimulant, edu_code ) "
                + "VALUES('N', '" + exam_id + "', '" + product_id + "', '" + stim_id + "', '" + respondent_id + "', '" + stimulant + "', '0'); ");
        }

        public bool CheckTraining(int exam_id)
        {
            if(CommandSQLScalar("SELECT COUNT(*) FROM " + tableName + " WHERE exam_id = '" + exam_id + "'") > 0)
                return true;
            return false;
        }

        public bool CheckTrainingPreset(int exam_id, int emotion_id = 0)
        {
            if (emotion_id == 0)
            {
                if (CommandSQLScalar("SELECT COUNT(*) FROM " + tableName + " WHERE exam_id = '" + exam_id + "' and edu_code = 1") > 0)
                    return true;
            }
            else
                if (CommandSQLScalar("SELECT COUNT(*) FROM " + tableName + " WHERE exam_id = '" + exam_id + "' and emotion_id = '" + emotion_id + "' and edu_code = 1") > 0)
                    return true;
            return false;
        }
    }
}
