﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Data.SQLite;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.DirectX.AudioVideoPlayback;
using System.IO;

namespace EyeStreamCapture.Visualizer
{
    public partial class Form1 : Form
    {
        //static PlotModel pm;
        //static LineSeries lsM;
        //static LineSeries lsA;
        //DataPointRepository rep;

        private Video video;
        private string[] videoPaths;
        private string folderPath = @"D:\projects\screencapture\screencapture\bin\Debug\project\justest\";
        private int selectedIndex = 0;
        private Size formSize;
        private Size pnlSize;

        private static SQLiteCommand _SQLiteCommand { get; set; }
        private static SQLiteConnection _SQLiteConnetion { get; set; }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            formSize = new Size(this.Width, this.Height);
            pnlSize = new Size(pnlVideo.Width, pnlVideo.Height);

            videoPaths = Directory.GetFiles(folderPath, "*.avi");

            if (videoPaths != null)
            {
                foreach (string path in videoPaths)
                {
                    string vid = path.Replace(folderPath, string.Empty);
                    vid = vid.Replace(".avi", string.Empty);
                    lstVideos.Items.Add(vid);
                }
            }

            lstVideos.SelectedIndex = selectedIndex;

            //this.WindowState = FormWindowState.Maximized;

        }

        private void lstVideos_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    video.Stop();
            //    video.Dispose();
            //}
            //catch { }

            int index = lstVideos.SelectedIndex;
            selectedIndex = index;
            video = new Video(videoPaths[index], false);
            //video.Owner = pnlVideo;
            //pnlVideo.Size = pnlSize;
            //video.Play();
            //tmrVideo.Enabled = true;

            // video.Ending += Video_Ending;

        }

        private void Video_Ending(object sender, EventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(2000);

                if (InvokeRequired)
                {
                    this.Invoke(new Action(() =>
                    {
                        NextVideo();
                    }));
                }
            });
        }

        private void NextVideo()
        {
            int index = lstVideos.SelectedIndex;
            index++;
            if (index > videoPaths.Length - 1)
                index = 0;
            selectedIndex = index;
            lstVideos.SelectedIndex = index;
        }
        
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
