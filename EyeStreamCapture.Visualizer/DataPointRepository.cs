﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeStreamCapture.Visualizer
{
    class DataPointRepository
    {
        private static SQLiteCommand _SQLiteCommand { get; set; }
        private static SQLiteConnection _SQLiteConnetion { get; set; }

        public DataPointRepository()
        {

        }

        public List<DataPoint> GetList(string pathToDB, string tableName, string column, int frameCount)
        {
            _SQLiteCommand = new SQLiteCommand();
            _SQLiteConnetion = new SQLiteConnection(@"Data Source=" + pathToDB);
            _SQLiteCommand.Connection = _SQLiteConnetion;
            _SQLiteConnetion.Open();
            List<DataPoint> list = new List<DataPoint>();

            for (int frameID = 1; frameID <= frameCount; frameID++)
            {
                _SQLiteCommand.CommandText = "SELECT " + column + " FROM " + tableName + " WHERE id=" + frameID;
                int meditationPerc = Int32.Parse(_SQLiteCommand.ExecuteScalar().ToString());
                list.Add(new DataPoint(frameID, meditationPerc));
            }

            return list;

        }
    }
}
